####################################################################
#
# This a CMake project file that controls which source files must
# be built and which third-party libraries must be linked.
#
# Use the cmake utility to create a Makefile from this project file.
# See http://www.cmake.org/ for details.
#
####################################################################

####################################################################
# List of source files of the reference configuration creation tool.
####################################################################
SET(RefConfToolSourceFiles
	Main.cpp
)

###################################################################
# Build executable.
###################################################################
ADD_EXECUTABLE(RefConfTool ${RefConfToolSourceFiles})
TARGET_LINK_LIBRARIES(RefConfTool CrystalAnalysisLibrary)
SET_TARGET_PROPERTIES(RefConfTool PROPERTIES COMPILE_DEFINITIONS "${ANALYSIS_COMPILE_DEFINITIONS}")
