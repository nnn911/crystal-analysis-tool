///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#ifndef __CA_CATALOG_GENERATOR_H
#define __CA_CATALOG_GENERATOR_H

#include <calib/CALib.h>
#include <calib/context/CAContext.h>
#include <calib/atomic_structure/AtomicStructure.h>
#include <calib/pattern/coordinationpattern/CoordinationPattern.h>
#include <calib/pattern/coordinationpattern/CoordinationPatternAnalysis.h>
#include <calib/pattern/superpattern/SuperPattern.h>
#include <calib/pattern/superpattern/SuperPatternAnalysis.h>
#include <calib/pattern/catalog/PatternCatalog.h>
#include <calib/pattern/coordinationpattern/nda/NDACoordinationPattern.h>
#include <calib/pattern/coordinationpattern/cna/CNACoordinationPattern.h>
#include <calib/pattern/coordinationpattern/acna/AdaptiveCNACoordinationPattern.h>
#include <calib/pattern/coordinationpattern/diamond/DiamondCoordinationPattern.h>
#include "PatternTemplate.h"

using namespace CALib;
using namespace std;

/**
 * Class that takes care of creating and adding a new pattern to the pattern catalog.
 */
class CatalogGenerator : public ContextReference
{
public:

	/// Constructor.
	CatalogGenerator(CAContext& context) : ContextReference(context) {}

	/// Loads an atomic structure file and generates a new pattern for all atoms that do not match to any of the existing patterns.
	void generatePattern(PatternTemplate& templ, PatternCatalog& catalog);

protected:

	/// Creates a new coordination pattern for the given central atom.
	auto_ptr<CoordinationPattern> createCoordinationPatternForAtom(int atomIndex, const PatternTemplate& templ, const std::string& patternName);

	/// Builds a coordination pattern from the neighbor bonds of the given central atom.
	void buildCoordinationPattern(int centerAtomIndex, CoordinationPattern& pattern, const PatternTemplate& templ);

	/// Generates data that is specific for an NDA coordination pattern.
	void buildNDACoordinationPattern(int centerAtomIndex, NDACoordinationPattern& pattern, const PatternTemplate& templ);

	/// Generates data that is specific for a CNA or ACNA coordination pattern.
	void buildCNACoordinationPattern(int centerAtomIndex, CNACoordinationPattern& pattern, const PatternTemplate& templ);

	/// Generates data that is specific for diamond coordination patterns.
	void buildDiamondCoordinationPattern(int centerAtomIndex, DiamondCoordinationPattern& pattern, const PatternTemplate& templ);
};

#endif // __CA_CATALOG_GENERATOR_H

