///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#include "HardcodedSuperPatternAnalysis.h"
#include "../pattern/catalog/PatternCatalog.h"

using namespace std;

namespace CALib {

/******************************************************************************
* Searches the structure for the hardcoded super patterns.
******************************************************************************/
vector<SuperPatternAnalysis::AnalysisResultsRecord> HardcodedSuperPatternAnalysis::searchHardcodedSuperPatterns()
{
	// Put together a list of super patterns.
	vector<SuperPattern const*> patternsToSearchFor;
	patternsToSearchFor.push_back(coordAnalysis().catalog().superPatternByName("fcc"));
	patternsToSearchFor.push_back(coordAnalysis().catalog().superPatternByName("hcp"));
	patternsToSearchFor.push_back(coordAnalysis().catalog().superPatternByName("bcc"));

	// Perform standard super pattern analysis.
	// Note that these patterns are all lattices (and no defect patterns).
	vector<SuperPatternAnalysis::AnalysisResultsRecord> results =
			SuperPatternAnalysis::searchSuperPatterns(patternsToSearchFor);

	return results;
}

/******************************************************************************
* Looks for FCC-HCP coherent interfaces and computes the cluster transformation matrices.
******************************************************************************/
void HardcodedSuperPatternAnalysis::identifyClusterInterfaces()
{
	CALIB_ASSERT(structure().neighborMode() == AtomicStructure::GHOST_ATOMS);

	const SuperPattern* fccPattern = coordAnalysis().catalog().superPatternByName("fcc");
	const SuperPattern* hcpPattern = coordAnalysis().catalog().superPatternByName("hcp");
	CALIB_ASSERT(fccPattern != NULL && hcpPattern != NULL);

	int numAtoms = structure().numLocalAtoms() + structure().numGhostAtoms();
	for(int atomIndex = 0; atomIndex < numAtoms; atomIndex++) {
		const ClusterAtom& hcpAtom = _clusterAtoms[atomIndex];
		Cluster* hcpCluster = hcpAtom.cluster;
		if(hcpCluster == NULL || hcpCluster->pattern != hcpPattern) continue;

		const SuperPatternNode& hcpNode = hcpAtom.patternNode();
		for(int nodeNeighborIndex = 0; nodeNeighborIndex < hcpNode.coordinationPattern->numNeighbors(); nodeNeighborIndex++) {
			int atomNeighborIndex = nodeNeighborToAtomNeighbor(hcpAtom, nodeNeighborIndex);
			int neighborAtomIndex = neighborList().neighbor(atomIndex, atomNeighborIndex);
			const ClusterAtom& fccAtom = _clusterAtoms[neighborAtomIndex];
			Cluster* fccCluster = fccAtom.cluster;
			if(fccCluster == NULL || fccCluster->pattern != fccPattern) continue;

			// Check if the cluster transition is already known.
			if(hcpCluster->findTransition(fccCluster) != NULL)
				continue;

			// Find two common neighbors.
			CALIB_ASSERT(hcpNode.neighbors[nodeNeighborIndex].numCommonNeighbors >= 2);
			int commonNeighbor1 = nodeNeighborToAtomNeighbor(hcpAtom, hcpNode.neighbors[nodeNeighborIndex].commonNeighborNodeIndices[0][0]);
			int commonNeighbor2 = nodeNeighborToAtomNeighbor(hcpAtom, hcpNode.neighbors[nodeNeighborIndex].commonNeighborNodeIndices[1][0]);
			int commonNeighborAtom1 = neighborList().neighbor(atomIndex, commonNeighbor1);
			int commonNeighborAtom2 = neighborList().neighbor(atomIndex, commonNeighbor2);

			const SuperPatternNode& fccNode = fccAtom.patternNode();
			FrameTransformation tm;
			for(int nodeNeighborIndex2 = 0; nodeNeighborIndex2 < fccNode.coordinationPattern->numNeighbors(); nodeNeighborIndex2++) {
				int atomNeighborIndex2 = nodeNeighborToAtomNeighbor(fccAtom, nodeNeighborIndex2);
				int neighborAtomIndex2 = neighborList().neighbor(neighborAtomIndex, atomNeighborIndex2);
				if(neighborAtomIndex2 == atomIndex) {
					tm.addVector(hcpNode.neighbors[nodeNeighborIndex].referenceVector, -fccNode.neighbors[nodeNeighborIndex2].referenceVector);
				}
				else if(neighborAtomIndex2 == commonNeighborAtom1) {
					tm.addVector(
							hcpNode.neighbors[hcpNode.neighbors[nodeNeighborIndex].commonNeighborNodeIndices[0][0]].referenceVector - hcpNode.neighbors[nodeNeighborIndex].referenceVector,
							fccNode.neighbors[nodeNeighborIndex2].referenceVector);
				}
				else if(neighborAtomIndex2 == commonNeighborAtom2) {
					tm.addVector(
							hcpNode.neighbors[hcpNode.neighbors[nodeNeighborIndex].commonNeighborNodeIndices[1][0]].referenceVector - hcpNode.neighbors[nodeNeighborIndex].referenceVector,
							fccNode.neighbors[nodeNeighborIndex2].referenceVector);
				}
			}

			_clusterGraph.createClusterTransition(hcpCluster, fccCluster, tm.computeTransformation());
		}
	}
}

}; // End of namespace
