///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#ifndef __CA_HARDCODED_SUPER_PATTERN_ANALYSIS_H
#define __CA_HARDCODED_SUPER_PATTERN_ANALYSIS_H

#include "../pattern/superpattern/SuperPatternAnalysis.h"
#include "HardcodedCoordinationPatternAnalysis.h"

namespace CALib {

/**
 * Searches for occurrences of super patterns in an AtomicStructure and
 * decomposes the atomic structure into atomic clusters.
 */
class HardcodedSuperPatternAnalysis : public SuperPatternAnalysis
{
public:

	/// Constructor. Takes a reference to the HardcodedCoordinationPatternAnalysis object, which serves as basis for the super pattern analysis.
	HardcodedSuperPatternAnalysis(const HardcodedCoordinationPatternAnalysis& coordAnalysis) : SuperPatternAnalysis(coordAnalysis) {}

	/// Searches the structure for the hardcoded super patterns.
	std::vector<SuperPatternAnalysis::AnalysisResultsRecord> searchHardcodedSuperPatterns();

	/// Looks for FCC-HCP coherent interfaces and computes the cluster transformation matrices.
	void identifyClusterInterfaces();
};

}; // End of namespace

#endif // __CA_HARDCODED_SUPER_PATTERN_ANALYSIS_H
