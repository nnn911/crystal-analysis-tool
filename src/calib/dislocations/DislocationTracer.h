///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#ifndef __CA_DISLOCATION_TRACER_H
#define __CA_DISLOCATION_TRACER_H

#include "../CALib.h"
#include "../context/CAContext.h"
#include "DislocationNetwork.h"

namespace CALib {

class AtomicStructure;
class InterfaceMesh;

/**
 * This is the central class for dislocation line tracing.
 */
class DislocationTracer : public ContextReference
{
public:

	/// Constructor.
	DislocationTracer(InterfaceMesh& mesh) : ContextReference(mesh.context()), _structure(mesh.structure()),
		_mesh(mesh), _network(mesh.clusterGraph()), _unusedCircuit(NULL) {
		setMaximumBurgersCircuitSize(CA_DEFAULT_MAX_BURGERS_CIRCUIT_SIZE);
		setMaximumExtendedBurgersCircuitSize(CA_DEFAULT_MAX_EXTENDED_BURGERS_CIRCUIT_SIZE);
	}

	/// Returns the underlying atomic structure.
	const AtomicStructure& structure() const { return _structure; }

	/// Returns the interface mesh that separates the crystal defects from the perfect regions.
	const InterfaceMesh& mesh() const { return _mesh; }

	/// Returns a reference to the cluster graph.
	ClusterGraph& clusterGraph() const { return _mesh.clusterGraph(); }

	/// Returns the extracted network of dislocation segments.
	DislocationNetwork& network() { return _network; }

	/// Returns a const-reference to the extracted network of dislocation segments.
	const DislocationNetwork& network() const { return _network; }

	/// Sets the maximum number of edges for Burgers circuits.
	void setMaximumBurgersCircuitSize(int maxSize) {
		_maxBurgersCircuitSize = maxSize;
	}

	/// Sets the maximum number of edges for Burgers circuits.
	void setMaximumExtendedBurgersCircuitSize(int maxSize) {
		_maxExtendedBurgersCircuitSize = maxSize;
	}

	/// Performs a dislocation search on the interface mesh by generating
	/// trial Burgers circuits. Identified dislocation segments are converted to
	/// a continuous line representation. Returns the number of segments and junctions.
	std::pair<size_t, size_t> traceDislocationSegments();

	/// After dislocation segments have been extracted, this method trims
	/// dangling lines and finds the optimal cluster to express each segment's
	/// Burgers vector.
	void finishDislocationSegments();

	/// Returns the list of nodes that are not part of a junction.
	const std::vector<DislocationNode*>& danglingNodes() const { return _danglingNodes; }

private:

	BurgersCircuit* allocateCircuit();
	void discardCircuit(BurgersCircuit* circuit);
	void findPrimarySegments(int maxBurgersCircuitSize);
	bool createBurgersCircuit(InterfaceMesh::Edge* edge, int maxBurgersCircuitSize);
	void createAndTraceSegment(BurgersCircuit* forwardCircuit, int maxCircuitLength);
	bool intersectsOtherCircuits(BurgersCircuit* circuit);
	BurgersCircuit* buildReverseCircuit(BurgersCircuit* forwardCircuit);
	void traceSegment(DislocationSegment& segment, DislocationNode& node, int maxCircuitLength, bool isPrimarySegment);
	bool tryRemoveTwoCircuitEdges(InterfaceMesh::Edge*& edge0, InterfaceMesh::Edge*& edge1, InterfaceMesh::Edge*& edge2);
	bool tryRemoveThreeCircuitEdges(InterfaceMesh::Edge*& edge0, InterfaceMesh::Edge*& edge1, InterfaceMesh::Edge*& edge2, bool isPrimarySegment);
	bool tryRemoveOneCircuitEdge(InterfaceMesh::Edge*& edge0, InterfaceMesh::Edge*& edge1, InterfaceMesh::Edge*& edge2, bool isPrimarySegment);
	bool trySweepTwoFacets(InterfaceMesh::Edge*& edge0, InterfaceMesh::Edge*& edge1, InterfaceMesh::Edge*& edge2, bool isPrimarySegment);
	bool tryInsertOneCircuitEdge(InterfaceMesh::Edge*& edge0, InterfaceMesh::Edge*& edge1, bool isPrimarySegment);
	void appendLinePoint(DislocationNode& node);
	void circuitCircuitIntersection(InterfaceMesh::Edge* circuitAEdge1, InterfaceMesh::Edge* circuitAEdge2, InterfaceMesh::Edge* circuitBEdge1, InterfaceMesh::Edge* circuitBEdge2, int& goingOutside, int& goingInside);
	size_t joinSegments(int maxCircuitLength);
	void createSecondarySegment(InterfaceMesh::Edge* firstEdge, BurgersCircuit* outerCircuit, int maxCircuitLength);

private:

	/// The atomic input structure.
	const AtomicStructure& _structure;

	/// The interface mesh that separates the crystal defects from the perfect regions.
	InterfaceMesh& _mesh;

	/// The extracted network of dislocation segments.
	DislocationNetwork _network;

	/// The maximum length (number of edges) for Burgers circuits during the first tracing phase.
	int _maxBurgersCircuitSize;

	/// The maximum length (number of edges) for Burgers circuits during the second tracing phase.
	int _maxExtendedBurgersCircuitSize;

	// Used to allocate memory for BurgersCircuit instances.
	MemoryPool<BurgersCircuit> _circuitPool;

	/// List of nodes that do not form a junction.
	std::vector<DislocationNode*> _danglingNodes;

	/// Stores a pointer to the last allocated circuit which has been discarded.
	/// It can be re-used on the next allocation request.
	BurgersCircuit* _unusedCircuit;
};

}; // End of namespace

#endif // __CA_DISLOCATION_TRACER_H
