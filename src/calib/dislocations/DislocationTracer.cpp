///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#include "DislocationTracer.h"
#include "InterfaceMesh.h"
#include "../context/CAContext.h"
#include "../atomic_structure/AtomicStructure.h"

using namespace std;

namespace CALib {

/******************************************************************************
* Allocates a new BurgersCircuit instance.
******************************************************************************/
BurgersCircuit* DislocationTracer::allocateCircuit()
{
	if(_unusedCircuit == NULL)
		return _circuitPool.construct();
	else {
		BurgersCircuit* circuit = _unusedCircuit;
		_unusedCircuit = NULL;
		return circuit;
	}
}

/******************************************************************************
* Discards a previously allocated BurgersCircuit instance.
******************************************************************************/
void DislocationTracer::discardCircuit(BurgersCircuit* circuit)
{
	CALIB_ASSERT(_unusedCircuit == NULL);
	_unusedCircuit = circuit;
}

/******************************************************************************
* Performs a dislocation search on the interface mesh by generating
* trial Burgers circuits. Identified dislocation segments are converted to
* a continuous line representation.
******************************************************************************/
std::pair<size_t, size_t> DislocationTracer::traceDislocationSegments()
{
	CALIB_ASSERT(_maxBurgersCircuitSize >= 3);
	CALIB_ASSERT(_maxBurgersCircuitSize <= _maxExtendedBurgersCircuitSize);

	// The dislocation search algorithm has not been parallelized yet.
	// Only the master processor performs it.
	if(!parallel().isMaster())
		return make_pair<size_t,size_t>(0,0);

	// Initialize pseudo-random number generator with a fixed seed to make
	// algorithm deterministic.
	srand(1);

	// Then incrementally extend the segments by enlarging the maximum circuit size until
	// segments meet at a junction.
	size_t numJunctions = 0;
	for(int circuitLength = 3; circuitLength <= _maxExtendedBurgersCircuitSize; circuitLength++) {

		// Extend existing segments with dangling ends to the current maximum circuit length.
		BOOST_FOREACH(DislocationNode* node, danglingNodes()) {
			CALIB_ASSERT(node->circuit->isDangling);
			CALIB_ASSERT(node->circuit->countEdges() == node->circuit->edgeCount);

			// Trace segment a bit further.
			traceSegment(*node->segment, *node, circuitLength, false);
		}

		// Find dislocation segments by generating trial Burgers circuits on the interface mesh
		// and then moving them in both directions along the dislocation segment.
		if(circuitLength <=_maxBurgersCircuitSize && (circuitLength & 1))
			findPrimarySegments(circuitLength);

		// Join two or more segments forming a dislocation junction.
		numJunctions += joinSegments(circuitLength);
	}

	return make_pair(network().segments().size(), numJunctions);
}

/******************************************************************************
* After dislocation segments have been extracted, this method trims
* dangling lines and finds the optimal cluster to express each segment's
* Burgers vector.
******************************************************************************/
void DislocationTracer::finishDislocationSegments()
{
	// Remove extra line points from segments that do not merge into a junction.
	// Also assign consecutive IDs to identified segments.
	for(int segmentIndex = 0; segmentIndex < network().segments().size(); segmentIndex++) {
		DislocationSegment* segment = network().segments()[segmentIndex];
		deque<Vector3>& line = segment->line;
		deque<int>& coreSize = segment->coreSize;
		segment->id = segmentIndex;
		CALIB_ASSERT(coreSize.size() == line.size());
		CALIB_ASSERT(segment->backwardNode().circuit->numPreliminaryPoints + segment->forwardNode().circuit->numPreliminaryPoints <= line.size());
		line.erase(line.begin(), line.begin() + segment->backwardNode().circuit->numPreliminaryPoints);
		line.erase(line.end() - segment->forwardNode().circuit->numPreliminaryPoints, line.end());
		coreSize.erase(coreSize.begin(), coreSize.begin() + segment->backwardNode().circuit->numPreliminaryPoints);
		coreSize.erase(coreSize.end() - segment->forwardNode().circuit->numPreliminaryPoints, coreSize.end());
	}

	// Express Burgers vectors of dislocations in a proper lattice frame whenever possible.
	BOOST_FOREACH(DislocationSegment* segment, network().segments()) {
		segment->findOptimalBurgersVectorRepresentation();
	}

	network().alignDislocationDirections();
}

/**
 * This data structure is used for the recursive generation of
 * trial Burgers circuits on the interface mesh.
 */
struct BurgersCircuitSearchStruct
{
	/// The current mesh node.
	InterfaceMesh::Node* node;

	/// The coordinates of this node in the unstrained reference crystal it was mapped to.
	Vector3 latticeCoord;

	/// The matrix that transforms local lattice vectors to the reference frame of the start node.
	Matrix3 tm;

	/// Number of steps between this node and the start node of the recursive walk.
	int recursiveDepth;

	/// The previous edge in the path to this node.
	InterfaceMesh::Edge* predecessorEdge;

	/// Linked list pointer.
	BurgersCircuitSearchStruct* nextToProcess;
};

/******************************************************************************
* Generates all possible trial circuits on the interface mesh until it finds
* one with a non-zero Burgers vector.
* Then moves the Burgers circuit in both directions along the dislocation
* segment until the maximum circuit size has been reached.
******************************************************************************/
void DislocationTracer::findPrimarySegments(int maxBurgersCircuitSize)
{
	int searchDepth =  (maxBurgersCircuitSize - 1) / 2;
	CALIB_ASSERT(searchDepth >= 1);

	MemoryPool<BurgersCircuitSearchStruct> structPool;

	// Find an appropriate start node for the recursive search.
	BOOST_FOREACH(InterfaceMesh::Node* startNode, mesh().nodes()) {
		CALIB_ASSERT(startNode->burgersSearchStruct == NULL);
		CALIB_ASSERT(startNode->edges != NULL);
		if(startNode->flags.test(InterfaceMesh::Node::INVALID)) continue;

		// The first node is the seed of our recursive walk.
		// It is mapped to the origin of the perfect reference lattice.
		BurgersCircuitSearchStruct* start = structPool.construct();
		start->latticeCoord.setZero();
		start->predecessorEdge = NULL;
		start->recursiveDepth = 0;
		start->nextToProcess = NULL;
		start->tm.setIdentity();
		start->node = startNode;
		startNode->burgersSearchStruct = start;
		Cluster* cluster = startNode->edges->clusterTransition->cluster1;

		bool foundBurgersCircuit = false;
		BurgersCircuitSearchStruct* end_of_queue = start;

		// Process nodes from the queue until it becomes empty or until a valid Burgers circuit has been found.
		for(BurgersCircuitSearchStruct* current = start; current != NULL && foundBurgersCircuit == false; current = current->nextToProcess) {
			InterfaceMesh::Node* currentNode = current->node;
			CALIB_ASSERT(currentNode->flags.test(InterfaceMesh::Node::INVALID) == false);
			for(InterfaceMesh::Edge* edge = currentNode->edges; edge != NULL; edge = edge->nextNodeEdge) {

				CALIB_ASSERT((edge->circuit == NULL) == (edge->nextCircuitEdge == NULL));
				CALIB_ASSERT((edge->oppositeEdge->circuit == NULL) == (edge->oppositeEdge->nextCircuitEdge == NULL));
				CALIB_ASSERT(edge->facet != NULL);

				// Skip edges which are, or have already been part of a Burgers circuit.
				if(edge->nextCircuitEdge != NULL || edge->oppositeEdge->nextCircuitEdge != NULL)
					continue;

				// Skip border edges.
				if(edge->facet->circuit != NULL)
					continue;

				// Get the neighbor node.
				InterfaceMesh::Node* neighbor = edge->node2();

				// Skip invalid mesh nodes without proper lattice vectors.
				if(neighbor->flags.test(InterfaceMesh::Node::INVALID))
					continue;

				// Calculate lattice coordinates of the neighbor node.
				Vector3 neighborCoord = current->tm * edge->clusterVector;
				neighborCoord += current->latticeCoord;

				// If this neighbor has been assigned lattice coordinates already
				// then perform Burgers circuit test by comparing previous and current coordinates.
				if(neighbor->burgersSearchStruct != NULL) {
					BurgersCircuitSearchStruct* neighborStruct = neighbor->burgersSearchStruct;

					// Compute Burgers vector of the current circuit.
					Vector3 burgersVector = neighborStruct->latticeCoord - neighborCoord;
					if(burgersVector.isZero(CA_LATTICE_VECTOR_EPSILON) == false) { // Found circuit with non-zero Burgers vector.

						// Check if circuit encloses disclination.
						Matrix3 frankRotation = neighborStruct->tm.inverse() * current->tm * edge->clusterTransition->reverse->tm;
						if((frankRotation - Matrix3::Identity()).isZero(CA_TRANSITION_MATRIX_EPSILON)) {
							// Stop as soon as a valid Burgers circuit has been found.
							if(createBurgersCircuit(edge, maxBurgersCircuitSize)) {
								foundBurgersCircuit = true;
								break;
							}
						}
					}
				}
				else if(current->recursiveDepth < searchDepth) {
					// This neighbor has not been visited before. Put it at the end of the queue.
					BurgersCircuitSearchStruct* neighborStruct = structPool.construct();
					neighborStruct->node = neighbor;
					neighborStruct->latticeCoord = neighborCoord;
					neighborStruct->predecessorEdge = edge;
					neighborStruct->recursiveDepth = current->recursiveDepth + 1;
					if(edge->clusterTransition->isSelfTransition())
						neighborStruct->tm = current->tm;
					else
						neighborStruct->tm = current->tm * edge->clusterTransition->reverse->tm;
					neighborStruct->nextToProcess = NULL;
					neighbor->burgersSearchStruct = neighborStruct;
					CALIB_ASSERT(end_of_queue->nextToProcess == NULL);
					end_of_queue->nextToProcess = neighborStruct;
					end_of_queue = neighborStruct;
				}
			}
		}

		// Clear the pointers of the nodes that have been visited during the last pass.
		for(BurgersCircuitSearchStruct* s = start; s != NULL; ) {
			s->node->burgersSearchStruct = NULL;
			s->node->flags.reset(InterfaceMesh::Node::VISITED);
			s = s->nextToProcess;
		}
		structPool.clear(true);
	}
}

/******************************************************************************
* Creates a dislocation segment and a pair of Burgers circuits.
******************************************************************************/
bool DislocationTracer::createBurgersCircuit(InterfaceMesh::Edge* edge, int maxBurgersCircuitSize)
{
	InterfaceMesh::Node* currentNode = edge->node1();
	InterfaceMesh::Node* neighborNode = edge->node2();
	BurgersCircuitSearchStruct* currentStruct = currentNode->burgersSearchStruct;
	BurgersCircuitSearchStruct* neighborStruct = neighborNode->burgersSearchStruct;

	// Reconstruct the Burgers circuit from the path we took along the mesh edges.
	BurgersCircuit* forwardCircuit = allocateCircuit();
	forwardCircuit->edgeCount = 1;
	forwardCircuit->firstEdge = forwardCircuit->lastEdge = edge->oppositeEdge;
	edge->oppositeEdge->circuit = forwardCircuit;

	// Clear flags of nodes on the second branch of the recursive walk.
	for(BurgersCircuitSearchStruct* a = neighborStruct; ; ) {
		a->node->flags.reset(InterfaceMesh::Node::VISITED);
		if(a->predecessorEdge == NULL) break;
		a = a->predecessorEdge->node1()->burgersSearchStruct;
	}

	// Mark all nodes on the first branch of the recursive walk.
	for(BurgersCircuitSearchStruct* a = currentStruct; ; ) {
		a->node->flags.set(InterfaceMesh::Node::VISITED);
		if(a->predecessorEdge == NULL) break;
		a = a->predecessorEdge->node1()->burgersSearchStruct;
	}

	// Then walk on the second branch until the first branch is reached.
	for(BurgersCircuitSearchStruct* a = neighborStruct; ; ) {
		if(a->node->flags.test(InterfaceMesh::Node::VISITED)) {
			a->node->flags.reset(InterfaceMesh::Node::VISITED);
			break;
		}
		CALIB_ASSERT(a->predecessorEdge != NULL);
		CALIB_ASSERT(a->predecessorEdge->circuit == NULL);
		CALIB_ASSERT(a->predecessorEdge->oppositeEdge->circuit == NULL);
		// Insert edge into the circuit.
		a->predecessorEdge->nextCircuitEdge = forwardCircuit->firstEdge;
		forwardCircuit->firstEdge = a->predecessorEdge;
		forwardCircuit->edgeCount++;
		forwardCircuit->firstEdge->circuit = forwardCircuit;
		a = a->predecessorEdge->node1()->burgersSearchStruct;
	}

	// Walk along the first branch again until the second branch is hit.
	for(BurgersCircuitSearchStruct* a = currentStruct; a->node->flags.test(InterfaceMesh::Node::VISITED); a = a->predecessorEdge->node1()->burgersSearchStruct) {
		CALIB_ASSERT(a->predecessorEdge != NULL);
		CALIB_ASSERT(a->predecessorEdge->circuit == NULL);
		CALIB_ASSERT(a->predecessorEdge->oppositeEdge->circuit == NULL);
		// Insert edge into the circuit.
		forwardCircuit->lastEdge->nextCircuitEdge = a->predecessorEdge->oppositeEdge;
		forwardCircuit->lastEdge = forwardCircuit->lastEdge->nextCircuitEdge;
		forwardCircuit->edgeCount++;
		forwardCircuit->lastEdge->circuit = forwardCircuit;
		a->node->flags.reset(InterfaceMesh::Node::VISITED);
	}

	// Close circuit.
	forwardCircuit->lastEdge->nextCircuitEdge = forwardCircuit->firstEdge;
	CALIB_ASSERT(forwardCircuit->firstEdge != forwardCircuit->firstEdge->nextCircuitEdge);
	CALIB_ASSERT(forwardCircuit->countEdges() == forwardCircuit->edgeCount);

	// Make sure the circuit does not span periodic boundaries.
	// This can be checked by summing up the vectors of the circuit's edges.
	// The sum should be zero for valid closed circuits.
	InterfaceMesh::Edge* e = forwardCircuit->firstEdge;
	Vector3 edgeSum(Vector3::Zero());
	Matrix3 frankRotation(Matrix3::Identity());
	Vector3 b(Vector3::Zero());
	do {
		edgeSum += e->physicalVector;
		b += frankRotation * e->clusterVector;
		frankRotation *= e->clusterTransition->reverse->tm;
		e = e->nextCircuitEdge;
	}
	while(e != forwardCircuit->firstEdge);
	CALIB_ASSERT((frankRotation - Matrix3::Identity()).isZero(CA_TRANSITION_MATRIX_EPSILON));

	// Make sure new circuit does not intersect other circuits.
	bool intersects = intersectsOtherCircuits(forwardCircuit);
	// Also make sure circuit does not span the entire periodic simulation box.
	if(intersects || edgeSum.isZero(CA_ATOM_VECTOR_EPSILON) == false) {
		// Clear edges.
		InterfaceMesh::Edge* e = forwardCircuit->firstEdge;
		do {
			InterfaceMesh::Edge* nextEdge = e->nextCircuitEdge;
			e->nextCircuitEdge = NULL;
			e->circuit = NULL;
			e = nextEdge;
		}
		while(e != forwardCircuit->firstEdge);
		discardCircuit(forwardCircuit);
		return intersects;
	}

	createAndTraceSegment(forwardCircuit, maxBurgersCircuitSize);

	return true;
}

/******************************************************************************
* Creates a reverse Burgers circuit, allocates a new DislocationSegment,
* and traces it in both directions.
******************************************************************************/
void DislocationTracer::createAndTraceSegment(BurgersCircuit* forwardCircuit, int maxCircuitLength)
{
	// Generate the reverse circuit.
	BurgersCircuit* backwardCircuit = buildReverseCircuit(forwardCircuit);

	// Create new dislocation segment.
	DislocationSegment* segment = network().createSegment(Vector3::Zero());
	segment->forwardNode().circuit = forwardCircuit;
	segment->backwardNode().circuit = backwardCircuit;
	forwardCircuit->dislocationNode = &segment->forwardNode();
	backwardCircuit->dislocationNode = &segment->backwardNode();
	_danglingNodes.push_back(&segment->forwardNode());
	_danglingNodes.push_back(&segment->backwardNode());

	// Add the first point to the line.
	segment->line.push_back(backwardCircuit->calculateCenter());
	segment->coreSize.push_back(backwardCircuit->countEdges());
	// Add a second point to the line.
	appendLinePoint(segment->forwardNode());

	// Trace the segment in the forward direction.
	traceSegment(*segment, segment->forwardNode(), maxCircuitLength, true);

	// Trace the segment in the backward direction.
	traceSegment(*segment, segment->backwardNode(), maxCircuitLength, true);

	// Store circuit state.
	forwardCircuit->storeCircuit();
	backwardCircuit->storeCircuit();
	forwardCircuit->numPreliminaryPoints = 0;
	backwardCircuit->numPreliminaryPoints = 0;
}

/******************************************************************************
* Tests whether the given circuit intersection any other existing circuit.
******************************************************************************/
bool DislocationTracer::intersectsOtherCircuits(BurgersCircuit* circuit)
{
	InterfaceMesh::Edge* edge1 = circuit->firstEdge;
	do {
		InterfaceMesh::Edge* edge2 = edge1->nextCircuitEdge;
		if(edge1 != edge2->oppositeEdge) {
			InterfaceMesh::Edge* currentEdge = edge1->oppositeEdge;
			do {
				CALIB_ASSERT(currentEdge->facet != NULL);
				InterfaceMesh::Edge* nextEdge = currentEdge->facet->previousEdge(currentEdge);
				if(nextEdge != edge2 && nextEdge->circuit != NULL) {
					CALIB_ASSERT(nextEdge->circuit == nextEdge->nextCircuitEdge->circuit);
					int goingOutside = 0, goingInside = 0;
					circuitCircuitIntersection(edge2->oppositeEdge, edge1->oppositeEdge, nextEdge, nextEdge->nextCircuitEdge, goingOutside, goingInside);
					CALIB_ASSERT(goingInside == 0);
					if(goingOutside)
						return true;
				}
				currentEdge = nextEdge->oppositeEdge;
			}
			while(currentEdge != edge2);
		}
		edge1 = edge2;
	}
	while(edge1 != circuit->firstEdge);
	return false;
}

/******************************************************************************
* Given some Burgers circuit, this function generates a reverse circuit.
******************************************************************************/
BurgersCircuit* DislocationTracer::buildReverseCircuit(BurgersCircuit* forwardCircuit)
{
	BurgersCircuit* backwardCircuit = allocateCircuit();

	// Build the backward circuit along inner outline.
	backwardCircuit->edgeCount = 0;
	backwardCircuit->firstEdge = NULL;
	backwardCircuit->lastEdge = NULL;
	InterfaceMesh::Edge* edge1 = forwardCircuit->firstEdge;
	do {
		InterfaceMesh::Edge* edge2 = edge1->nextCircuitEdge;
		InterfaceMesh::Edge* oppositeEdge1 = edge1->oppositeEdge;
		InterfaceMesh::Edge* oppositeEdge2 = edge2->oppositeEdge;
		InterfaceMesh::Facet* facet1 = oppositeEdge1->facet;
		InterfaceMesh::Facet* facet2 = oppositeEdge2->facet;
		CALIB_ASSERT(facet1 != NULL && facet2 != NULL);
		CALIB_ASSERT(facet1->circuit == NULL || facet1->circuit == backwardCircuit);
		CALIB_ASSERT(facet2->circuit == NULL || facet2->circuit == backwardCircuit);
		CALIB_ASSERT(edge1->node2() == edge2->node1());
		CALIB_ASSERT((edge1->clusterVector + oppositeEdge1->clusterTransition->tm * oppositeEdge1->clusterVector).isZero(CA_LATTICE_VECTOR_EPSILON));
		CALIB_ASSERT((edge2->clusterVector + oppositeEdge2->clusterTransition->tm * oppositeEdge2->clusterVector).isZero(CA_LATTICE_VECTOR_EPSILON));

		if(facet1 != facet2) {
			InterfaceMesh::Edge* outerEdge1 = NULL;
			InterfaceMesh::Edge* innerEdge1 = NULL;
			InterfaceMesh::Edge* outerEdge2 = NULL;
			InterfaceMesh::Edge* innerEdge2 = NULL;
			for(int e = 0; e < 3; e++) {
				if(facet1->edges[e] == oppositeEdge1) {
					innerEdge1 = facet1->edges[(e+2)%3]->oppositeEdge;
					outerEdge1 = facet1->edges[(e+1)%3]->oppositeEdge;
				}
				if(facet2->edges[e] == oppositeEdge2) {
					innerEdge2 = facet2->edges[(e+1)%3]->oppositeEdge;
					outerEdge2 = facet2->edges[(e+2)%3]->oppositeEdge;
				}
			}
			CALIB_ASSERT(innerEdge1 != NULL && innerEdge2 != NULL);
			CALIB_ASSERT(innerEdge1->node1() == edge1->node2());
			CALIB_ASSERT(innerEdge2->node2() == edge1->node2());
			CALIB_ASSERT(innerEdge1->node1() == innerEdge2->node2());
			CALIB_ASSERT(innerEdge1->circuit == NULL || innerEdge1->circuit == backwardCircuit);
			CALIB_ASSERT(innerEdge2->circuit == NULL || innerEdge2->circuit == backwardCircuit);
			facet1->flags.set(InterfaceMesh::Facet::IS_PRIMARY_SEGMENT);
			facet1->circuit = backwardCircuit;
			facet2->flags.set(InterfaceMesh::Facet::IS_PRIMARY_SEGMENT);
			facet2->circuit = backwardCircuit;
			innerEdge1->circuit = backwardCircuit;
			innerEdge2->circuit = backwardCircuit;
			innerEdge2->nextCircuitEdge = innerEdge1;
			if(backwardCircuit->lastEdge == NULL) {
				CALIB_ASSERT(backwardCircuit->firstEdge == NULL);
				CALIB_ASSERT(innerEdge1->nextCircuitEdge == NULL);
				backwardCircuit->lastEdge = innerEdge1;
				backwardCircuit->firstEdge = innerEdge2;
				backwardCircuit->edgeCount += 2;
			}
			else if(backwardCircuit->lastEdge != innerEdge2) {
				if(innerEdge1 != backwardCircuit->firstEdge) {
					innerEdge1->nextCircuitEdge = backwardCircuit->firstEdge;
					backwardCircuit->edgeCount += 2;
				}
				else {
					backwardCircuit->edgeCount += 1;
				}
				backwardCircuit->firstEdge = innerEdge2;
			}
			else if(backwardCircuit->firstEdge != innerEdge1) {
				innerEdge1->nextCircuitEdge = backwardCircuit->firstEdge;
				backwardCircuit->firstEdge = innerEdge1;
				backwardCircuit->edgeCount += 1;
			}
			CALIB_ASSERT(innerEdge1->node1() != innerEdge1->node2());
			CALIB_ASSERT(innerEdge2->node1() != innerEdge2->node2());
		}
		else {
#ifdef DEBUG_CRYSTAL_ANALYSIS
			InterfaceMesh::Edge* shortEdge = NULL;
			for(int e = 0; e < 3; e++) {
				if(facet1->edges[e] == oppositeEdge1) {
					shortEdge = facet1->edges[(e+1)%3]->oppositeEdge;
					break;
				}
			}
			CALIB_ASSERT(shortEdge != NULL && shortEdge != edge1 && shortEdge != edge2);
			CALIB_ASSERT(shortEdge->node1() == edge2->node2());
			CALIB_ASSERT(shortEdge->node2() == edge1->node1());
			CALIB_ASSERT(shortEdge->circuit == NULL || shortEdge->circuit == backwardCircuit);
#endif
		}

		edge1 = edge2;
	}
	while(edge1 != forwardCircuit->firstEdge);
	CALIB_ASSERT(backwardCircuit->edgeCount > 0);
	CALIB_ASSERT(backwardCircuit->lastEdge->node2() == backwardCircuit->firstEdge->node1());
	CALIB_ASSERT(backwardCircuit->lastEdge->nextCircuitEdge == NULL || backwardCircuit->lastEdge->nextCircuitEdge == backwardCircuit->firstEdge);

	// Close circuit.
	backwardCircuit->lastEdge->nextCircuitEdge = backwardCircuit->firstEdge;

	CALIB_ASSERT(backwardCircuit->firstEdge != backwardCircuit->firstEdge->nextCircuitEdge);
	CALIB_ASSERT(backwardCircuit->countEdges() == backwardCircuit->edgeCount);

	return backwardCircuit;
}

/******************************************************************************
* Traces a dislocation segment in one direction by advancing the Burgers circuit.
******************************************************************************/
void DislocationTracer::traceSegment(DislocationSegment& segment, DislocationNode& node, int maxCircuitLength, bool isPrimarySegment)
{
	BurgersCircuit& circuit = *node.circuit;
	CALIB_ASSERT(circuit.countEdges() == circuit.edgeCount);
	CALIB_ASSERT(circuit.isDangling);

	// Advance circuit as far as possible.
	for(;;) {

		// During each iteration, first shorten circuit as much as possible.
		// Pick a random start edge to distribute the removal of edges
		// over the whole circuit.
		InterfaceMesh::Edge* firstEdge = circuit.getEdge(rand() % circuit.edgeCount);

		InterfaceMesh::Edge* edge0 = firstEdge;
		InterfaceMesh::Edge* edge1 = edge0->nextCircuitEdge;
		InterfaceMesh::Edge* edge2 = edge1->nextCircuitEdge;
		CALIB_ASSERT(edge1->circuit == &circuit);
		int counter = 0;
		do {
			// Check Burgers circuit.
			CALIB_ASSERT(circuit.edgeCount >= 2);
			CALIB_ASSERT(circuit.countEdges() == circuit.edgeCount);
			CALIB_ASSERT(edge0->circuit == &circuit && edge1->circuit == &circuit && edge2->circuit == &circuit);

			bool wasShortened = false;
			if(tryRemoveTwoCircuitEdges(edge0, edge1, edge2))
				wasShortened = true;
			else if(tryRemoveThreeCircuitEdges(edge0, edge1, edge2, isPrimarySegment))
				wasShortened = true;
			else if(tryRemoveOneCircuitEdge(edge0, edge1, edge2, isPrimarySegment))
				wasShortened = true;
			else if(trySweepTwoFacets(edge0, edge1, edge2, isPrimarySegment))
				wasShortened = true;

			if(wasShortened) {
				appendLinePoint(node);
				counter = -1;
			}

			edge0 = edge1;
			edge1 = edge2;
			edge2 = edge2->nextCircuitEdge;
			counter++;
		}
		while(counter <= circuit.edgeCount);
		CALIB_ASSERT(circuit.edgeCount >= 2);
		CALIB_ASSERT(circuit.countEdges() == circuit.edgeCount);

		if(circuit.edgeCount >= maxCircuitLength)
			break;

		// In the second step, extend circuit by inserting an edge if possible.
		bool wasExtended = false;

		// Pick a random start edge to distribute the insertion of new edges
		// over the whole circuit.
		firstEdge = circuit.getEdge(rand() % circuit.edgeCount);

		edge0 = firstEdge;
		edge1 = firstEdge->nextCircuitEdge;
		do {
			if(tryInsertOneCircuitEdge(edge0, edge1, isPrimarySegment)) {
				wasExtended = true;
				appendLinePoint(node);
				break;
			}

			edge0 = edge1;
			edge1 = edge1->nextCircuitEdge;
		}
		while(edge0 != firstEdge);
		if(!wasExtended) break;
	}
}

/******************************************************************************
* Eliminates two edges from a Burgers circuit if they are opposite halfedges.
******************************************************************************/
bool DislocationTracer::tryRemoveTwoCircuitEdges(InterfaceMesh::Edge*& edge0, InterfaceMesh::Edge*& edge1, InterfaceMesh::Edge*& edge2)
{
	if(edge1 != edge2->oppositeEdge)
		return false;

	BurgersCircuit* circuit = edge0->circuit;
	CALIB_ASSERT(circuit->edgeCount >= 4);
	edge0->nextCircuitEdge = edge2->nextCircuitEdge;
	if(edge0 == circuit->lastEdge) {
		circuit->firstEdge = circuit->lastEdge->nextCircuitEdge;
	}
	else if(edge1 == circuit->lastEdge) {
		circuit->lastEdge = edge0;
		circuit->firstEdge = edge0->nextCircuitEdge;
	}
	else if(edge2 == circuit->lastEdge) {
		circuit->lastEdge = edge0;
	}
	circuit->edgeCount -= 2;

	edge1 = edge0->nextCircuitEdge;
	edge2 = edge1->nextCircuitEdge;
	return true;
}

/******************************************************************************
* Eliminates three edges from a Burgers circuit if they border a triangle.
******************************************************************************/
bool DislocationTracer::tryRemoveThreeCircuitEdges(InterfaceMesh::Edge*& edge0, InterfaceMesh::Edge*& edge1, InterfaceMesh::Edge*& edge2, bool isPrimarySegment)
{
	InterfaceMesh::Facet* facet1 = edge1->facet;
	InterfaceMesh::Facet* facet2 = edge2->facet;
	CALIB_ASSERT(facet1 != NULL && facet2 != NULL);

	if(facet2 != facet1 || facet1->circuit != NULL)
		return false;

	BurgersCircuit* circuit = edge0->circuit;
	CALIB_ASSERT(circuit->edgeCount > 2);
	InterfaceMesh::Edge* edge3 = edge2->nextCircuitEdge;

	if(edge3->facet != facet1) return false;
	CALIB_ASSERT(circuit->edgeCount > 4);

	edge0->nextCircuitEdge = edge3->nextCircuitEdge;

	if(edge2 == circuit->firstEdge || edge3 == circuit->firstEdge) {
		circuit->firstEdge = edge3->nextCircuitEdge;
		circuit->lastEdge = edge0;
	}
	else if(edge1 == circuit->firstEdge) {
		circuit->firstEdge = edge3->nextCircuitEdge;
		CALIB_ASSERT(circuit->lastEdge == edge0);
	}
	else if(edge3 == circuit->lastEdge) {
		circuit->lastEdge = edge0;
	}
	circuit->edgeCount -= 3;
	edge1 = edge3->nextCircuitEdge;
	edge2 = edge1->nextCircuitEdge;

	facet1->circuit = circuit;
	if(isPrimarySegment)
		facet1->flags.set(InterfaceMesh::Facet::IS_PRIMARY_SEGMENT);

	return true;
}

/******************************************************************************
* Eliminates one edge from a Burgers circuit by replacing two edges with one.
******************************************************************************/
bool DislocationTracer::tryRemoveOneCircuitEdge(InterfaceMesh::Edge*& edge0, InterfaceMesh::Edge*& edge1, InterfaceMesh::Edge*& edge2, bool isPrimarySegment)
{
	InterfaceMesh::Facet* facet1 = edge1->facet;
	InterfaceMesh::Facet* facet2 = edge2->facet;
	CALIB_ASSERT(facet1 != NULL && facet2 != NULL);
	if(facet2 != facet1 || facet1->circuit != NULL) return false;

	BurgersCircuit* circuit = edge0->circuit;
	CALIB_ASSERT(circuit->edgeCount > 2);

	if(edge0->facet == facet1) return false;

	InterfaceMesh::Edge* shortEdge = NULL;
	for(int e = 0; e < 3; e++) {
		if(facet1->edges[e] != edge1 && facet1->edges[e] != edge2) {
			shortEdge = facet1->edges[e]->oppositeEdge;
			break;
		}
	}
	CALIB_ASSERT(shortEdge != NULL);

	if(shortEdge->circuit != NULL) return false;

	CALIB_ASSERT(shortEdge->nextCircuitEdge == NULL);
	shortEdge->nextCircuitEdge = edge2->nextCircuitEdge;
	CALIB_ASSERT(shortEdge != edge2->nextCircuitEdge->oppositeEdge);
	CALIB_ASSERT(shortEdge != edge0->oppositeEdge);
	edge0->nextCircuitEdge = shortEdge;
	if(edge0 == circuit->lastEdge) {
		CALIB_ASSERT(circuit->lastEdge != edge2);
		CALIB_ASSERT(circuit->firstEdge == edge1);
		CALIB_ASSERT(shortEdge != circuit->lastEdge->oppositeEdge);
		circuit->firstEdge = shortEdge;
	}

	if(edge2 == circuit->lastEdge) {
		circuit->lastEdge = shortEdge;
	}
	else if(edge2 == circuit->firstEdge) {
		circuit->firstEdge = shortEdge->nextCircuitEdge;
		circuit->lastEdge = shortEdge;
	}
	circuit->edgeCount -= 1;
	edge1 = shortEdge;
	edge2 = shortEdge->nextCircuitEdge;
	shortEdge->circuit = circuit;

	facet1->circuit = circuit;
	if(isPrimarySegment)
		facet1->flags.set(InterfaceMesh::Facet::IS_PRIMARY_SEGMENT);

	return true;
}

/******************************************************************************
* Advances a Burgers circuit by skipping two facets.
******************************************************************************/
bool DislocationTracer::trySweepTwoFacets(InterfaceMesh::Edge*& edge0, InterfaceMesh::Edge*& edge1, InterfaceMesh::Edge*& edge2, bool isPrimarySegment)
{
	InterfaceMesh::Facet* facet1 = edge1->facet;
	InterfaceMesh::Facet* facet2 = edge2->facet;
	CALIB_ASSERT(facet1 != NULL && facet2 != NULL);

	if(facet1->circuit != NULL || facet2->circuit != NULL) return false;

	BurgersCircuit* circuit = edge0->circuit;
	if(facet1 == facet2 || circuit->edgeCount <= 2) return false;

	InterfaceMesh::Edge* outerEdge1 = NULL;
	InterfaceMesh::Edge* innerEdge1 = NULL;
	InterfaceMesh::Edge* outerEdge2 = NULL;
	InterfaceMesh::Edge* innerEdge2 = NULL;
	for(int e = 0; e < 3; e++) {
		if(facet1->edges[e] == edge1) {
			outerEdge1 = facet1->edges[(e+2)%3]->oppositeEdge;
			innerEdge1 = facet1->edges[(e+1)%3];
		}
		if(facet2->edges[e] == edge2) {
			outerEdge2 = facet2->edges[(e+1)%3]->oppositeEdge;
			innerEdge2 = facet2->edges[(e+2)%3];
		}
	}
	CALIB_ASSERT(outerEdge1 != NULL && outerEdge2 != NULL);

	if(innerEdge1 != innerEdge2->oppositeEdge || outerEdge1->circuit != NULL || outerEdge2->circuit != NULL)
		return false;

	CALIB_ASSERT(outerEdge1->nextCircuitEdge == NULL);
	CALIB_ASSERT(outerEdge2->nextCircuitEdge == NULL);
	outerEdge1->nextCircuitEdge = outerEdge2;
	outerEdge2->nextCircuitEdge = edge2->nextCircuitEdge;
	edge0->nextCircuitEdge = outerEdge1;
	if(edge0 == circuit->lastEdge) {
		circuit->firstEdge = outerEdge1;
	}
	else if(edge1 == circuit->lastEdge) {
		circuit->lastEdge = outerEdge1;
		circuit->firstEdge = outerEdge2;
	}
	else if(edge2 == circuit->lastEdge) {
		circuit->lastEdge = outerEdge2;
	}
	outerEdge1->circuit = circuit;
	outerEdge2->circuit = circuit;

	facet1->circuit = circuit;
	facet2->circuit = circuit;
	if(isPrimarySegment) {
		facet1->flags.set(InterfaceMesh::Facet::IS_PRIMARY_SEGMENT);
		facet2->flags.set(InterfaceMesh::Facet::IS_PRIMARY_SEGMENT);
	}

	edge0 = outerEdge1;
	edge1 = outerEdge2;
	edge2 = edge1->nextCircuitEdge;

	return true;
}

/******************************************************************************
* Advances a Burgers circuit by skipping one facet and inserting an
* additional edge.
******************************************************************************/
bool DislocationTracer::tryInsertOneCircuitEdge(InterfaceMesh::Edge*& edge0, InterfaceMesh::Edge*& edge1, bool isPrimarySegment)
{
	CALIB_ASSERT(edge0 != edge1->oppositeEdge);

	InterfaceMesh::Facet* facet = edge1->facet;
	CALIB_ASSERT(facet != NULL);
	if(facet->circuit != NULL)
		return false;

	InterfaceMesh::Edge* insertEdge1 = facet->previousEdge(edge1)->oppositeEdge;
	if(insertEdge1->circuit != NULL)
		return false;

	InterfaceMesh::Edge* insertEdge2 = facet->nextEdge(edge1)->oppositeEdge;
	if(insertEdge2->circuit != NULL)
		return false;

	CALIB_ASSERT(insertEdge1->nextCircuitEdge == NULL);
	CALIB_ASSERT(insertEdge2->nextCircuitEdge == NULL);
	BurgersCircuit* circuit = edge0->circuit;
	insertEdge1->nextCircuitEdge = insertEdge2;
	insertEdge2->nextCircuitEdge = edge1->nextCircuitEdge;
	edge0->nextCircuitEdge = insertEdge1;
	if(edge0 == circuit->lastEdge) {
		circuit->firstEdge = insertEdge1;
	}
	else if(edge1 == circuit->lastEdge) {
		circuit->lastEdge = insertEdge2;
	}
	insertEdge1->circuit = circuit;
	insertEdge2->circuit = circuit;
	circuit->edgeCount++;

	// Check Burgers circuit.
	CALIB_ASSERT(circuit->countEdges() == circuit->edgeCount);

	facet->circuit = circuit;
	if(isPrimarySegment)
		facet->flags.set(InterfaceMesh::Facet::IS_PRIMARY_SEGMENT);

	return true;
}

/******************************************************************************
* Appends another point to the curve at one end of a dislocation segment.
******************************************************************************/
void DislocationTracer::appendLinePoint(DislocationNode& node)
{
	DislocationSegment& segment = *node.segment;
	CALIB_ASSERT(!segment.line.empty());

	// Get size of dislocation core.
	int coreSize = node.circuit->edgeCount;

	// Make sure the line is not wrapped at periodic boundaries.
	const Vector3& lastPoint = node.isForwardNode() ? segment.line.back() : segment.line.front();
	Vector3 newPoint = lastPoint + structure().wrapVector(node.circuit->calculateCenter() - lastPoint);

	if(node.isForwardNode()) {
		// Add a new point to end the line.
		segment.line.push_back(newPoint);
		segment.coreSize.push_back(coreSize);
	}
	else {
		// Add a new point to start the line.
		segment.line.push_front(newPoint);
		segment.coreSize.push_front(coreSize);
	}
	node.circuit->numPreliminaryPoints++;
}

/******************************************************************************
* Determines whether two Burgers circuits intersect.
******************************************************************************/
void DislocationTracer::circuitCircuitIntersection(InterfaceMesh::Edge* circuitAEdge1, InterfaceMesh::Edge* circuitAEdge2, InterfaceMesh::Edge* circuitBEdge1, InterfaceMesh::Edge* circuitBEdge2, int& goingOutside, int& goingInside)
{
	CALIB_ASSERT(circuitAEdge2->node1() == circuitBEdge2->node1());
	CALIB_ASSERT(circuitAEdge1->node2() == circuitBEdge2->node1());
	CALIB_ASSERT(circuitBEdge1->node2() == circuitBEdge2->node1());

	// Iterate over interior facet edges.
	InterfaceMesh::Edge* edge = circuitBEdge2;
	bool contour1inside = false;
	bool contour2inside = false;
	for(;;) {
		InterfaceMesh::Edge* oppositeEdge = edge->oppositeEdge;
		if(oppositeEdge == circuitBEdge1) break;
		if(edge != circuitBEdge2) {
			if(oppositeEdge == circuitAEdge1) contour1inside = true;
			if(edge == circuitAEdge2) contour2inside = true;
		}
		InterfaceMesh::Facet* facet = oppositeEdge->facet;
		CALIB_ASSERT(facet != NULL);
		edge = facet->nextEdge(oppositeEdge);
		CALIB_ASSERT(edge->node1() == circuitBEdge2->node1());
		CALIB_ASSERT(edge != circuitBEdge2);
	}
	CALIB_ASSERT(circuitAEdge2 != circuitBEdge2 || contour2inside == false);

	// Iterate over exterior facet edges.
	bool contour1outside = false;
	bool contour2outside = false;
	edge = circuitBEdge1;
	for(;;) {
		InterfaceMesh::Facet* facet = edge->facet;
		CALIB_ASSERT(facet != NULL);
		InterfaceMesh::Edge* nextEdge = facet->nextEdge(edge);
		if(nextEdge == circuitBEdge2) break;
		InterfaceMesh::Edge* oppositeEdge = nextEdge->oppositeEdge;
		CALIB_ASSERT(oppositeEdge->node2() == circuitBEdge2->node1());
		edge = oppositeEdge;
		if(edge == circuitAEdge1) contour1outside = true;
		if(nextEdge == circuitAEdge2) contour2outside = true;
	}

	CALIB_ASSERT(contour1outside == false || contour1inside == false);
	CALIB_ASSERT(contour2outside == false || contour2inside == false);

	if(contour2outside == true && contour1outside == false) {
		goingOutside += 1;
	}
	else if(contour2inside == true && contour1inside == false) {
		goingInside += 1;
	}
}

/******************************************************************************
* Look for dislocation segments whose circuits touch each other.
******************************************************************************/
size_t DislocationTracer::joinSegments(int maxCircuitLength)
{
	// First iteration over all dangling circuits.
	// Try to create secondary dislocation segments in the adjacent regions of the
	// interface mesh.
	for(size_t nodeIndex = 0; nodeIndex < danglingNodes().size(); nodeIndex++) {
		DislocationNode* node = danglingNodes()[nodeIndex];
		BurgersCircuit* circuit = node->circuit;
		CALIB_ASSERT(circuit->isDangling);

		// Go around the circuit to find an unvisited region on the interface mesh.
		InterfaceMesh::Edge* edge = circuit->firstEdge;
		do {
			CALIB_ASSERT(edge->circuit == circuit);
			BurgersCircuit* oppositeCircuit = edge->oppositeEdge->circuit;
			if(oppositeCircuit == NULL) {
				CALIB_ASSERT(edge->oppositeEdge->nextCircuitEdge == NULL);

				// Try to create a new circuit inside the unvisited region.
				createSecondarySegment(edge, circuit, maxCircuitLength);

				// Skip edges to the end of the unvisited interval.
				while(edge->oppositeEdge->circuit == NULL && edge != circuit->firstEdge)
					edge = edge->nextCircuitEdge;
			}
			else edge = edge->nextCircuitEdge;
		}
		while(edge != circuit->firstEdge);
	}

	// Second pass over all dangling nodes.
	// Mark circuits that are completely blocked by other circuits.
	// They are candidates for the formation of junctions.
	BOOST_FOREACH(DislocationNode* node, danglingNodes()) {
		BurgersCircuit* circuit = node->circuit;
		CALIB_ASSERT(circuit->isDangling);

		// Go around the circuit to see whether it is completely surrounded by other circuits.
		// Put it into one ring with the adjacent circuits.
		circuit->isCompletelyBlocked = true;
		InterfaceMesh::Edge* edge = circuit->firstEdge;
		do {
			CALIB_ASSERT(edge->circuit == circuit);
			BurgersCircuit* adjacentCircuit = edge->oppositeEdge->circuit;
			if(adjacentCircuit == NULL) {
				// Found a section of the circuit, which is not blocked
				// by some other circuit.
				circuit->isCompletelyBlocked = false;
				break;
			}
			else if(adjacentCircuit != circuit) {
				CALIB_ASSERT(adjacentCircuit->isDangling);
				DislocationNode* adjacentNode = adjacentCircuit->dislocationNode;
				if(node->formsJunctionWith(adjacentNode) == false) {
					node->connectNodes(adjacentNode);
				}
			}
			edge = edge->nextCircuitEdge;
		}
		while(edge != circuit->firstEdge);
	}

	// Count number of created dislocation junctions.
	size_t numJunctions = 0;

	// Create actual junctions for completely blocked circuits.
	BOOST_FOREACH(DislocationNode* node, danglingNodes()) {
		BurgersCircuit* circuit = node->circuit;

		// Skip circuits which have already become part of a junction.
		if(circuit->isDangling == false) continue;
		// Skip dangling circuits, which are not completely blocked by other circuits;
		if(!circuit->isCompletelyBlocked) {
			node->dissolveJunction();
			continue;
		}
		// Junctions must consist of at least two dislocation segments.
		if(node->junctionRing == node) continue;

		CALIB_ASSERT(node->segment->replacedWith == NULL);

		// Compute center of mass of junction node.
		Vector3 centerOfMassVector(Vector3::Zero());
		Vector3 basePoint = node->position();
		int armCount = 1;
		bool allCircuitsCompletelyBlocked = true;
		DislocationNode* armNode = node->junctionRing;
		while(armNode != node) {
			CALIB_ASSERT(armNode->segment->replacedWith == NULL);
			CALIB_ASSERT(armNode->circuit->isDangling);
			if(armNode->circuit->isCompletelyBlocked == false) {
				allCircuitsCompletelyBlocked = false;
				break;
			}
			armCount++;
			centerOfMassVector += structure().wrapVector(armNode->position() - basePoint);
			armNode = armNode->junctionRing;
		}

		// All circuits of the junction must be fully blocked by other circuits.
		if(!allCircuitsCompletelyBlocked) {
			node->dissolveJunction();
			continue;
		}

		// Junctions must consist of at least two dislocation segments.
		CALIB_ASSERT(armCount >= 2);

		// Only create a real junction for three or more segments.
		if(armCount >= 3) {
			Vector3 centerOfMass = basePoint + centerOfMassVector / armCount;

			// Iterate over all arms of the new junction.
			armNode = node;
			do {
				// Mark this node as no longer dangling.
				armNode->circuit->isDangling = false;

				// Extend arm to junction's exact center point.
				deque<Vector3>& line = armNode->segment->line;
				if(armNode->isForwardNode()) {
					line.push_back(line.back() + structure().wrapVector(centerOfMass - line.back()));
					armNode->segment->coreSize.push_back(armNode->segment->coreSize.back());
				}
				else {
					line.push_front(line.front() + structure().wrapVector(centerOfMass - line.front()));
					armNode->segment->coreSize.push_front(armNode->segment->coreSize.front());
				}
				armNode->circuit->numPreliminaryPoints = 0;
				armNode = armNode->junctionRing;
			}
			while(armNode != node);
			numJunctions++;
		}
		else {
			// For a two-armed junction, just merge the two segments into one.
			DislocationNode* node1 = node;
			DislocationNode* node2 = node->junctionRing;
			CALIB_ASSERT(node2->junctionRing == node1);
			CALIB_ASSERT(node1->junctionRing == node2);

			BurgersCircuit* circuit1 = node1->circuit;
			BurgersCircuit* circuit2 = node2->circuit;
			circuit1->isDangling = false;
			circuit2->isDangling = false;
			circuit1->numPreliminaryPoints = 0;
			circuit2->numPreliminaryPoints = 0;

			// Check if this is a closed dislocation loop.
			if(node1->oppositeNode == node2) {
				CALIB_ASSERT(node1->segment == node2->segment);
				DislocationSegment* loop = node1->segment;
				CALIB_ASSERT(loop->isClosedLoop());

				// Make both ends of the segment coincide by adding an extra point if necessary.
				if(!structure().wrapVector(node1->position() - node2->position()).isZero(CA_ATOM_VECTOR_EPSILON)) {
					loop->line.push_back(loop->line.back() + structure().wrapVector(loop->line.front() - loop->line.back()));
					CALIB_ASSERT(structure().wrapVector(node1->position() - node2->position()).isZero(CA_ATOM_VECTOR_EPSILON));
					loop->coreSize.push_back(loop->coreSize.back());
				}

				// Loop segment should not be degenerate.
				CALIB_ASSERT(loop->line.size() >= 3);
			}
			else {
				// If not a closed loop, merge the two segments into a single line.

				CALIB_ASSERT(node1->segment != node2->segment);

				DislocationNode* farEnd1 = node1->oppositeNode;
				DislocationNode* farEnd2 = node2->oppositeNode;
				DislocationSegment* segment1 = node1->segment;
				DislocationSegment* segment2 = node2->segment;
				if(node1->isBackwardNode()) {
					segment1->nodes[1] = farEnd2;
					Vector3 shiftVector;
					if(node2->isBackwardNode()) {
						shiftVector = structure().calculateShiftVector(segment1->line.front(), segment2->line.front());
						segment1->line.insert(segment1->line.begin(), segment2->line.rbegin(), segment2->line.rend() - 1);
						segment1->coreSize.insert(segment1->coreSize.begin(), segment2->coreSize.rbegin(), segment2->coreSize.rend() - 1);
					}
					else {
						shiftVector = structure().calculateShiftVector(segment1->line.front(), segment2->line.back());
						segment1->line.insert(segment1->line.begin(), segment2->line.begin(), segment2->line.end() - 1);
						segment1->coreSize.insert(segment1->coreSize.begin(), segment2->coreSize.begin(), segment2->coreSize.end() - 1);
					}
					if(shiftVector != Vector3::Zero()) {
						for(deque<Vector3>::iterator p = segment1->line.begin(); p != segment1->line.begin() + segment2->line.size() - 1; ++p)
							*p -= shiftVector;
					}
				}
				else {
					segment1->nodes[0] = farEnd2;
					Vector3 shiftVector;
					if(node2->isBackwardNode()) {
						shiftVector = structure().calculateShiftVector(segment1->line.back(), segment2->line.front());
						segment1->line.insert(segment1->line.end(), segment2->line.begin() + 1, segment2->line.end());
						segment1->coreSize.insert(segment1->coreSize.end(), segment2->coreSize.begin() + 1, segment2->coreSize.end());
					}
					else {
						shiftVector = structure().calculateShiftVector(segment1->line.back(), segment2->line.back());
						segment1->line.insert(segment1->line.end(), segment2->line.rbegin() + 1, segment2->line.rend());
						segment1->coreSize.insert(segment1->coreSize.end(), segment2->coreSize.rbegin() + 1, segment2->coreSize.rend());
					}
					if(shiftVector != Vector3::Zero()) {
						for(deque<Vector3>::iterator p = segment1->line.end() - segment2->line.size() + 1; p != segment1->line.end(); ++p)
							*p -= shiftVector;
					}
				}
				farEnd2->segment = segment1;
				farEnd2->oppositeNode = farEnd1;
				farEnd1->oppositeNode = farEnd2;
				node1->oppositeNode = node2;
				node2->oppositeNode = node1;
				segment2->replacedWith = segment1;
				network().discardSegment(segment2);
			}
		}
	}

	// Clean up list of dangling nodes. Remove joined nodes.
	_danglingNodes.erase(std::remove_if(_danglingNodes.begin(), _danglingNodes.end(),
			not1(mem_fun(&DislocationNode::isDangling))), _danglingNodes.end());

	return numJunctions;
}

/******************************************************************************
* Creates a new dislocation segment at an incomplete junction.
******************************************************************************/
void DislocationTracer::createSecondarySegment(InterfaceMesh::Edge* firstEdge, BurgersCircuit* outerCircuit, int maxCircuitLength)
{
	CALIB_ASSERT(firstEdge->circuit == outerCircuit);

	// Create circuit along the border of the hole.
	int edgeCount = 1;
	Vector3 burgersVector(Vector3::Zero());
	Vector3 edgeSum(Vector3::Zero());
	Matrix3 frankRotation(Matrix3::Identity());
	int numCircuits = 1;
	InterfaceMesh::Edge* circuitStart = firstEdge->oppositeEdge;
	InterfaceMesh::Edge* circuitEnd = circuitStart;
	InterfaceMesh::Edge* edge = circuitStart;
	for(;;) {
		for(;;) {
			CALIB_ASSERT(edge->facet != NULL);
			CALIB_ASSERT(edge->circuit == NULL);
			InterfaceMesh::Edge* oppositeEdge = edge->oppositeEdge;
			InterfaceMesh::Facet* oppositeFacet = oppositeEdge->facet;
			InterfaceMesh::Edge* nextEdge = oppositeFacet->previousEdge(oppositeEdge);
			CALIB_ASSERT(nextEdge->node2() == oppositeEdge->node1());
			CALIB_ASSERT(nextEdge->node2() == edge->node2());
			if(nextEdge->circuit != NULL) {
				if(nextEdge->circuit != outerCircuit) {
					outerCircuit = nextEdge->circuit;
					numCircuits++;
				}
				edge = nextEdge->oppositeEdge;
				break;
			}
			edge = nextEdge;
		}

		circuitEnd->nextCircuitEdge = edge;
		edgeSum += edge->physicalVector;
		burgersVector += frankRotation * edge->clusterVector;
		frankRotation *= edge->clusterTransition->reverse->tm;
		if(edge == circuitStart)
			break;
		circuitEnd = edge;
		edgeCount++;

		if(edgeCount > maxCircuitLength)
			break;
	}

	// Create secondary segment only for dislocations (b != 0) and small enough dislocation cores.
	if(numCircuits == 1
			|| edgeCount > maxCircuitLength
			|| burgersVector.isZero(CA_LATTICE_VECTOR_EPSILON)
			|| edgeSum.isZero(CA_ATOM_VECTOR_EPSILON) == false
			|| !(frankRotation - Matrix3::Identity()).isZero(CA_TRANSITION_MATRIX_EPSILON)) {

		// Discard unused circuit.
		edge = circuitStart;
		for(;;) {
			CALIB_ASSERT(edge->circuit == NULL);
			InterfaceMesh::Edge* nextEdge = edge->nextCircuitEdge;
			edge->nextCircuitEdge = NULL;
			if(edge == circuitEnd) break;
			edge = nextEdge;
		}
		return;
	}
	CALIB_ASSERT(circuitStart != circuitEnd);

	// Create forward circuit.
	BurgersCircuit* forwardCircuit = allocateCircuit();
	forwardCircuit->firstEdge = circuitStart;
	forwardCircuit->lastEdge = circuitEnd;
	forwardCircuit->edgeCount = edgeCount;
	edge = circuitStart;
	do {
		CALIB_ASSERT(edge->circuit == NULL);
		edge->circuit = forwardCircuit;
		edge = edge->nextCircuitEdge;
	}
	while(edge != circuitStart);
	CALIB_ASSERT(forwardCircuit->countEdges() == forwardCircuit->edgeCount);

	// Do all the rest.
	createAndTraceSegment(forwardCircuit, maxCircuitLength);
}


}; // End of namespace
