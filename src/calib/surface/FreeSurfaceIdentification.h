///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#ifndef __CA_FREE_SURFACE_IDENTIFICATION_H
#define __CA_FREE_SURFACE_IDENTIFICATION_H

#include "../CALib.h"
#include "../atomic_structure/AtomicStructure.h"
#include "../trianglemesh/TriangleMesh.h"
#include "../context/CAContext.h"
#include "../tessellation/DelaunayTessellation.h"

namespace CALib {

/**
 * This class generates triangle mesh that represents the free surfaces of a solid.
 */
class FreeSurfaceIdentification : public ContextReference
{
public:

	/// Constructor,
	FreeSurfaceIdentification(const AtomicStructure& structure);

	/// Returns the alpha parameter value.
	CAFloat alpha() const { return _alpha; }

	/// Sets the alpha parameter value.
	void setAlpha(CAFloat alpha) { _alpha = alpha; }

	/// Returns the atomic structure.
	const AtomicStructure& structure() const { return _structure; }

	/// Returns the underlying tessellation of the atomistic system.
	const DelaunayTessellation& tessellation() const { return _tessellation; }

	/// Generates a geometric representation of the free surfaces of the solid.
	void buildSurfaceMesh(TriangleMesh& mesh);

	/// Returns the volume of the solid region.
	CAFloat solidVolume() const { return _solidVolume; }

private:

	/// Stores the mesh facets generated for a tetrahedron of the Delaunay tessellation.
	struct Tetrahedron {
		/// Pointers to the mesh facets associated with the four faces of the tetrahedron.
		boost::array<TriangleMeshFacet*, 4> meshFacets;
	};

	/// Determines if the given Delaunay tetrahedron is part of the solid region.
	bool isSolidTetrahedron(DelaunayTessellation::CellHandle cell) {
		if(!_tessellation.isValidCell(cell)) return false;
	    return _tessellation.dt().geom_traits().compare_squared_radius_3_object()(
	          cell->vertex(0)->point(),
	          cell->vertex(1)->point(),
	          cell->vertex(2)->point(),
	          cell->vertex(3)->point(),
	          alpha()) != CGAL::POSITIVE;
	}

	/// Creates the triangular mesh facets separating solid and open tetrahedra.
	void createSeparatingFacets(TriangleMesh& mesh);

	/// Links half-edges to opposite half-edges.
	void linkHalfEdges(TriangleMesh& mesh);

	/// The atomic structure.
	const AtomicStructure& _structure;

	/// The Delaunay tessellation.
	DelaunayTessellation _tessellation;

	/// Internal list of tetrahedra of the Delaunay tessellation.
	std::vector<Tetrahedron> _tetrahedra;

	/// The alpha value.
	CAFloat _alpha;

	/// Reverse the normal orientation of the surface mesh.
	bool _flipOrientation;

	/// The volume of the solid region.
	CAFloat _solidVolume;
};

}; // End of namespace

#endif // __CA_FREE_SURFACE_IDENTIFICATION_H

