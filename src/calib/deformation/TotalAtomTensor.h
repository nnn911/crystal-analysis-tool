///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#ifndef __CA_TOTAL_ATOM_TENSOR_H
#define __CA_TOTAL_ATOM_TENSOR_H

#include "../CALib.h"
#include "../context/CAContext.h"
#include "../atomic_structure/AtomicStructure.h"
#include "../atomic_structure/neighbor_list/NeighborList.h"
#include "DisplacementVectors.h"

namespace CALib {

/**
 * Calculates the total deformation gradient tensor for an atom.
 */
class TotalAtomTensor : public ContextReference
{
public:

	/// Constructor.
	TotalAtomTensor(CAContext& context, const AtomicStructure& structure1, const AtomicStructure& structure2, const NeighborList& neighborList) :
		ContextReference(context), _structure1(structure1), _structure2(structure2),
		_neighborList(neighborList), _displacements(context, structure1, structure2) {

		// Build two-way map between atoms in the initial configuration and the final configuration.
		_displacements.prepare();
	}

	/// Calculates the total deformation tensor for the given atom.
	/// Returns true if the tensor was calculated. Returns false if it could not be calculated for this atom.
	bool calculateDeformationTensor(int atomIndex, Matrix3& F) const {
		int atomIndex2 = _displacements.mapInitialToFinal(atomIndex);
		if(atomIndex2 < 0) return false;
		FrameTransformation tm;
		BOOST_FOREACH(const NeighborList::neighbor_info& n, _neighborList.neighbors(atomIndex)) {
			int neighborIndex2 = _displacements.mapInitialToFinal(n.index);
			if(neighborIndex2 < 0) continue;
			Vector3 delta_final = _structure2.atomPosition(neighborIndex2) - _structure2.atomPosition(atomIndex2);
			if(_structure2.isWrappedVector(delta_final)) {
				context().raiseErrorOne("Initially close-by atoms %1 and %2 have moved apart very far in the deformed configuration. "
						"Their final distance exceeds half the periodic box size. "
						"Under these circumstances, we cannot compute the deformation tensor, "
						"because the calculation of displacement vectors is ambiguous.", _structure2.atomTag(atomIndex2), _structure2.atomTag(neighborIndex2));
			}
			tm.addVector(n.delta, delta_final);
		}
		if(tm.isSingular())
			return false;
		F = tm.computeAverageTransformation();
		return true;
	}

private:

	/// The initial configuration of the atomic structure.
	const AtomicStructure& _structure1;

	/// The final configuration of the atomic structure.
	const AtomicStructure& _structure2;

	/// The neighbor list for the initial configuration.
	const NeighborList& _neighborList;

	/// Used to compute the displacement vectors.
	DisplacementVectors _displacements;
};

}; // End of namespace

#endif // __CA_TOTAL_ATOM_TENSOR_H

