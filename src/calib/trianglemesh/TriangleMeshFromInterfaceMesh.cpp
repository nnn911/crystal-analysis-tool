///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2010, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#include "TriangleMesh.h"
#include "../atomic_structure/AtomicStructure.h"
#include "../dislocations/InterfaceMesh.h"
#include "../dislocations/DislocationTracer.h"
#include "../context/CAContext.h"

using namespace std;

namespace CALib {

/******************************************************************************
* Generates the nodes and facets of the output mesh based on the interface mesh.
******************************************************************************/
void TriangleMesh::generateFromInterfaceMesh(const InterfaceMesh& interfaceMesh, const DislocationTracer& tracer)
{
	// Gather defect surface facets.
	BOOST_FOREACH(InterfaceMesh::Facet* facet, interfaceMesh.facets()) {

		// Skip parts of the interface mesh that have been swept by a Burgers circuit and are
		// now part of a dislocation line.
		if(facet->circuit != NULL) {
			if(facet->flags.test(InterfaceMesh::Facet::IS_PRIMARY_SEGMENT) == true || facet->circuit->isDangling == false)
				continue;
		}

		TriangleMeshEdge* outputEdges[3];

		// Transfer vertices.
		for(int v = 0; v < 3; v++) {
			InterfaceMesh::Edge* meshEdge = facet->edges[2-v];
			InterfaceMesh::Node* node1 = meshEdge->node1();
			InterfaceMesh::Node* node2 = meshEdge->node2();

			if(node1->outputVertex == NULL)
				node1->outputVertex = createVertex(node1->pos);
			if(node2->outputVertex == NULL)
				node2->outputVertex = createVertex(node2->pos);
			if(meshEdge->outputEdge == NULL) {
				CALIB_ASSERT(meshEdge->oppositeEdge->outputEdge == NULL);
				meshEdge->outputEdge = createEdge(node1->outputVertex, node2->outputVertex);
				meshEdge->oppositeEdge->outputEdge = meshEdge->outputEdge->oppositeEdge;
			}
			outputEdges[v] = meshEdge->oppositeEdge->outputEdge;
		}

		// Create new output facet.
		TriangleMeshFacet* outputFacet = createFacet(outputEdges);
	}

	// Generate cap vertices and facets to close holes left by
	// dangling Burgers circuits.
	BOOST_FOREACH(DislocationNode* dislocationNode, tracer.danglingNodes()) {
		BurgersCircuit* circuit = dislocationNode->circuit;
		CALIB_ASSERT(dislocationNode->isDangling());
		CALIB_ASSERT(circuit != NULL);
		CALIB_ASSERT(!circuit->segmentMeshCap.empty());

		TriangleMeshVertex* capVertex = createVertex(dislocationNode->position());
		capVertex->flags.set(TriangleMeshVertex::VERTEX_IS_FIXED);

		InterfaceMesh::Node* firstNode = circuit->segmentMeshCap.front()->node1();
		if(firstNode->outputVertex == NULL)
			firstNode->outputVertex = createVertex(firstNode->pos);
		TriangleMeshEdge* firstEdge = createEdge(capVertex, firstNode->outputVertex);
		TriangleMeshEdge* facetEdges[3];
		facetEdges[0] = firstEdge;

		BOOST_FOREACH(InterfaceMesh::Edge* meshEdge, circuit->segmentMeshCap) {
			InterfaceMesh::Node* node1 = meshEdge->node1();
			InterfaceMesh::Node* node2 = meshEdge->node2();

			CALIB_ASSERT(node1->outputVertex != NULL);
			if(node2->outputVertex == NULL)
				node2->outputVertex = createVertex(node2->pos);
			if(meshEdge->outputEdge == NULL) {
				CALIB_ASSERT(meshEdge->oppositeEdge->outputEdge == NULL);
				meshEdge->outputEdge = createEdge(node1->outputVertex, node2->outputVertex);
				meshEdge->oppositeEdge->outputEdge = meshEdge->outputEdge->oppositeEdge;
			}
			CALIB_ASSERT(meshEdge->outputEdge->facet == NULL);
			CALIB_ASSERT(facetEdges[0]->facet == NULL);

			// Create new output facet.
			facetEdges[1] = meshEdge->outputEdge;
			if(meshEdge != circuit->segmentMeshCap.back())
				facetEdges[2] = createEdge(node2->outputVertex, capVertex);
			else
				facetEdges[2] = firstEdge->oppositeEdge;
			TriangleMeshFacet* facet = createFacet(facetEdges);
			facetEdges[0] = facetEdges[2]->oppositeEdge;
		}
	}

	duplicateSharedVertices();

#ifdef DEBUG_CRYSTAL_ANALYSIS
	context().msgLogger() << "Validating triangle mesh." << endl;
	validate(true);
#endif
}


}; // End of namespace
