///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#ifndef __CA_TRIANGLE_MESH_H
#define __CA_TRIANGLE_MESH_H

#include "../CALib.h"
#include "../util/MemoryPool.h"
#include "../context/CAContext.h"

namespace CALib {

struct TriangleMeshVertex;
struct TriangleMeshEdge;
struct TriangleMeshFacet;
class InterfaceMesh;
class SimulationCell;
class DislocationTracer;

/**
 * A half-edge of a triangle mesh.
 */
struct TriangleMeshEdge
{
	/// The opposite half-edge.
	TriangleMeshEdge* oppositeEdge;

	/// The vertex this halfedge is pointing to.
	TriangleMeshVertex* vertex2;

	/// The adjacent triangle facet.
	TriangleMeshFacet* facet;

	/// The next edge in the linked-list of halfedge of the source vertex.
	TriangleMeshEdge* nextEdge;

	/// Returns the vertex this halfedge is coming from.
	TriangleMeshVertex* vertex1() const { return oppositeEdge->vertex2; }
};

/**
 * A vertex of a triangle mesh.
 */
struct TriangleMeshVertex
{
	enum BitFlags {
		VERTEX_CLIPX,
		VERTEX_CLIPY,
		VERTEX_CLIPZ,
		VERTEX_IS_FIXED,
		VERTEX_VISITED,
		VERTEX_IS_CORNER,
		IS_GHOST_VERTEX,
		NUM_FLAGS
	};

	/// The world-space position of the vertex.
	Vector3 pos;

	/// The surface normal at this vertex.
	Vector3 normal;

	/// The local Laplacian used for surface smooting.
	Vector3 laplacian;

	/// The index in the mesh's list of vertices.
	int index;

	/// The unique identifier of the vertex. This is copied from
	/// the atom from which this vertex was derived.
	AtomInteger tag;

	/// The number of adjacent triangle facets.
	int numFacets;

	/// The head of the linked-list of outgoing edges.
	TriangleMeshEdge* edges;

	/// The flags array.
	std::bitset<NUM_FLAGS> flags;

	bool isClipVertex(int dim) const { return flags.test(VERTEX_CLIPX + dim); }
	void setClipVertex(int dim) { flags.set(VERTEX_CLIPX + dim); }

	bool isCornerVertex() const { return flags.test(VERTEX_IS_CORNER); }
	void setCornerVertex() { flags.set(VERTEX_IS_CORNER); }

	bool wasVisited() const { return flags.test(VERTEX_VISITED); }
	void setVisited() { flags.set(VERTEX_VISITED); }
	void clearVisited() { flags.reset(VERTEX_VISITED); }
};

/**
 * A triangular facet of a mesh.
 */
struct TriangleMeshFacet
{
	enum BitFlags {
		FACET_VISITED,
		IS_GHOST_FACET,
		NUM_FLAGS
	};

	/// The three half-edges that border the facet.
	TriangleMeshEdge* edges[3];

	/// Temporary field used for processing.
	int index;

	/// The flags array.
	std::bitset<NUM_FLAGS> flags;

	bool wasVisited() const { return flags.test(FACET_VISITED); }
	void setVisited() { flags.set(FACET_VISITED); }
	void clearVisited() { flags.reset(FACET_VISITED); }

	/// Given an edge of the triangle, returns its position
	/// in the list of edges of this triangle.
	int edgeIndex(TriangleMeshEdge* edge) const {
		if(edges[0] == edge) return 0;
		if(edges[1] == edge) return 1;
		if(edges[2] == edge) return 2;
		CALIB_ASSERT(false);
		return -1;
	}

	/// Given an edge of the triangle, returns the following edge
	/// in this triangles's cyclic list of edges.
	TriangleMeshEdge* nextEdge(TriangleMeshEdge* edge) const {
		if(edges[0] == edge) return edges[1];
		if(edges[1] == edge) return edges[2];
		if(edges[2] == edge) return edges[0];
		CALIB_ASSERT(false);
		return NULL;
	}

	/// Given an edge of the triangle, returns the preceding edge
	/// in this triangles's cyclic list of edges.
	TriangleMeshEdge* previousEdge(TriangleMeshEdge* edge) const {
		if(edges[0] == edge) return edges[2];
		if(edges[1] == edge) return edges[0];
		if(edges[2] == edge) return edges[1];
		CALIB_ASSERT(false);
		return NULL;
	}

	/// Returns the index of the given vertex of the triangular facet.
	int vertexIndex(TriangleMeshVertex* v) const {
		if(edges[0]->vertex2 == v) return 1;
		else if(edges[1]->vertex2 == v) return 2;
		else {
			CALIB_ASSERT(edges[2]->vertex2 == v);
			return 0;
		}
	}

	/// Returns the a vertex node of the triangular facet.
	TriangleMeshVertex* vertex(int index) const {
		CALIB_ASSERT(index >= 0 && index < 3);
		if(index == 0) return edges[2]->vertex2;
		else if(index == 1) return edges[0]->vertex2;
		else return edges[1]->vertex2;
	}

	/// Returns true if the given node is a vertex of this triangle facet.
	bool hasVertex(TriangleMeshVertex* v) const {
		return edges[0]->vertex2 == v || edges[1]->vertex2 == v || edges[2]->vertex2 == v;
	}
};

/**
 * Stores a mesh of triangles using a half-edge data structure.
 */
class TriangleMesh : public ContextReference
{
public:

	/// Constructor.
	TriangleMesh(CAContext& _context) : ContextReference(_context), _numLocalVertices(0), _numLocalFacets(0) {}

	/// Generates a triangle mesh from the given interface mesh.
	void generateFromInterfaceMesh(const InterfaceMesh& interfaceMesh, const DislocationTracer& tracer);

	/// Clears the mesh and releases all memory.
	void clear();

	/// Creates a new vertex in the mesh.
	TriangleMeshVertex* createVertex(const Vector3& pos, bool isGhostVertex = false, AtomInteger tag = 0);

	/// Creates a new vertex in the mesh with the given normal vector.
	TriangleMeshVertex* createVertex(const Vector3& pos, const Vector3& normal) {
		TriangleMeshVertex* v = createVertex(pos);
		v->normal = normal;
		return v;
	}

	/// Create a new edge (consisting of a pair of halfedges).
	TriangleMeshEdge* createEdge(TriangleMeshVertex* vertex1, TriangleMeshVertex* vertex2);

	/// Create a new half edge.
	TriangleMeshEdge* createHalfEdge(TriangleMeshVertex* vertex1, TriangleMeshVertex* vertex2);

	/// Create a new triangle facet.
	TriangleMeshFacet* createFacet(TriangleMeshEdge* edges[3], bool isGhostFacet = false);

	/// Create a new triangle facet and creates its edges if necessary.
	TriangleMeshFacet* createFacetAndEdges(TriangleMeshVertex* vertices[3]);

	/// Smoothes the output mesh for better visualization results.
	void smoothMesh(int smoothingLevel, const SimulationCell& cell);
	void smoothMesh(CAFloat prefactor, const SimulationCell& cell, bool projectToNormals);

	/// Splits up facets which are wrapped at the periodic boundaries.
	void wrapMesh(const SimulationCell& cell, TriangleMesh* capMesh = NULL);

	/// Calculates the facet and vertex normals.
	void calculateNormals(const SimulationCell& cell);

	/// Tests whether the given point is inside the closed polyhedron described by this mesh.
	bool pointInPolyhedron(const Vector3& p, const SimulationCell& cell) const;

	/// Duplicates mesh vertices which are part of multiple manifolds.
	size_t duplicateSharedVertices();

	/// Returns a reference to the internal list of facets.
	const std::vector<TriangleMeshFacet*>& facets() const { return _facets; }

	/// Returns a reference to the internal list of vertices.
	const std::vector<TriangleMeshVertex*>& vertices() const { return _vertices; }

	/// Merges partial meshes from all processors into a single mesh.
	void mergeParallel(TriangleMesh& outputMesh);

	/// Creates a copy of this mesh.
	TriangleMesh* clone(bool reverseOrientation = false);

	/// Creates a copy of this mesh.
	void clone(TriangleMesh& output, bool reverseOrientation = false);

	/// Performs a thorough check of the topology of the mesh.
	/// Raises an error if something wrong is found. Note that this should never happen.
	void validate(bool performManifoldCheck = true) const;

	/// Computes the total surface area of the mesh.
	CAFloat computeTotalArea(const SimulationCell& cell) const;

private:

	/// Subdivides a facet edge to clip it at the periodic boundaries of the simulation cell.
	void splitEdge(TriangleMeshEdge* edge, const SimulationCell& cell, int dim);
	void splitFacet(TriangleMeshFacet* facet1, TriangleMeshEdge* edge1, TriangleMeshEdge* edge2, TriangleMeshVertex* intersectionPoint1, TriangleMeshVertex* intersectionPoint2, int dim);

	/// Creates the cap facets to close holes at periodic boundaries.
	void createCaps(const SimulationCell& cell, TriangleMesh& capMesh, TriangleMeshVertex* cornerVertices[8]);

	/// The vertices of the mesh.
	std::vector<TriangleMeshVertex*> _vertices;
	MemoryPool<TriangleMeshVertex> _vertexPool;

	/// The edges of the mesh.
	MemoryPool<TriangleMeshEdge> _edgePool;

	/// The facets of the mesh.
	std::vector<TriangleMeshFacet*> _facets;
	MemoryPool<TriangleMeshFacet> _facetPool;

	/// The number of local vertices owned by this processor (which are not ghost vertices).
	int _numLocalVertices;

	/// The number of local facets owned by this processor.
	int _numLocalFacets;
};

}; // End of namespace

#endif // __CA_TRIANGLE_MESH_H
