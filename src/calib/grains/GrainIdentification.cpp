///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#include "GrainIdentification.h"

using namespace std;

namespace CALib {

/******************************************************************************
* Constructor.
******************************************************************************/
GrainIdentification::GrainIdentification(SuperPatternAnalysis& patternAnalysis) :
	ContextReference(patternAnalysis.context()), _structure(patternAnalysis.structure()),
	_patternAnalysis(patternAnalysis)
{

}

/**
 * This structure is an edge in the graph of grains connecting
 * two adjacent grains.
 */
struct GrainGraphEdge
{
	/// Identifier of grain 1.
	AtomInteger a;

	/// Identifier of grain 2.
	AtomInteger b;

	/// Misorientation angle between the two grains.
	CAFloat misorientation;

	/// This comaprison operator is used to sort edges.
	bool operator<(const GrainGraphEdge& other) const { return misorientation < other.misorientation; }
};

/******************************************************************************
* Builds a list of grains and assigns atoms to the grains.
******************************************************************************/
AtomInteger GrainIdentification::findGrains()
{
	// This algorithm does not support distributed data.
	if(parallel().processorCount() != 1)
		context().raiseErrorAll("The grain identification algorithm is not parallelized. Please run the program in serial mode.");

	// Build reverse mapping from ghost atoms to local atoms.
	// This mapping will be required to resolve references to ghost atoms in the neighbor list
	// with the corresponding local atom index.
	generateGhostToLocalAtomMap();

	// Initialize the working list of grains.
	_grains.resize(structure().numLocalAtoms());
	BOOST_FOREACH(Grain& grain, _grains)
		grain.init();

	// Computes the local orientation tensor for all atoms whose structure has been identified.
	computeAtomicOrientationTensors();

	// Build grain graph.
	vector<GrainGraphEdge> bulkEdges;
	vector<Grain>::iterator grainA = _grains.begin();
	for(int atomA = 0; atomA < structure().numLocalAtoms(); atomA++, ++grainA) {
		// If the current atom is a crystal atom recognized by the atomic structure identification algorithm,
		// then we connect it with the neighbors provided by the structure pattern.
		// Skip disordered atoms.
		if(grainA->cluster == NULL) continue;

		// Iterate over all neighbors of the atom.
		int numNeighbors = patternAnalysis().atomCoordinationPattern(atomA).numNeighbors();
		for(int ni = 0; ni < numNeighbors; ni++) {
			// Lookup neighbor atom from neighbor list.
			int atomB = mapToLocalAtomIndex(
					neighborList().neighbor(atomA,
							patternAnalysis().nodeNeighborToAtomNeighbor(atomA, ni)));

			Grain& grainB = _grains[atomB];
			if(grainB.cluster != NULL) {
				// This test ensures that we will create only one edge per pair of neighbor atoms.
				if(atomB <= atomA)
					continue;

				// Connect the two atoms with an edge.
				GrainGraphEdge edge = { atomA, atomB, calculateMisorientation(*grainA, grainB) };
				bulkEdges.push_back(edge);
			}
			else {
				// Add isolated GB atoms to the adjacent lattice grain.
				if(grainB.parent->cluster > grainA->cluster) {
					grainB.parent->atomCount--;
					grainB.parent = &grainB;
				}
				if(grainB.isRoot())
					grainA->join(grainB);
			}
		}
	}

	// Sort edges in order of ascending misorientation.
	boost::sort(bulkEdges);

	// Merge grains.
	for(int pass = 1; pass <= 2; pass++) {
		BOOST_FOREACH(const GrainGraphEdge& edge, bulkEdges) {
			Grain& grainA = parentGrain(edge.a);
			Grain& grainB = parentGrain(edge.b);
			bool allowForFluctuations = (pass == 2);
			mergeTest(grainA, grainB, allowForFluctuations);
		}
	}

	// Dissolve crystal grains that are too small (i.e. number of atoms below threshold).
	BOOST_FOREACH(Grain& atomicGrain, _grains) {
		Grain& rootGrain = parentGrain(atomicGrain);
		if(rootGrain.latticeAtomCount < _minGrainAtomCount || rootGrain.cluster->pattern->isLattice() == false)
			atomicGrain.init();					// Dissolve grain.
		else
			atomicGrain.parent = &rootGrain;	// Path compression
	}

	// Merge grain boundary atoms into grains.
	for(;;) {
		bool done = true;
		vector<bool> mergedAtoms(structure().numLocalAtoms(), false);
		for(int atomA = 0; atomA < structure().numLocalAtoms(); atomA++) {
			Grain& grainA = parentGrain(atomA);
			int numNeighbors = min(12, neighborList().neighborCount(atomA));
			for(int ni = 0; ni < numNeighbors; ni++) {
				int atomB = mapToLocalAtomIndex(neighborList().neighbor(atomA, ni));

				if(mergedAtoms[atomB])
					continue;

				Grain& grainB = parentGrain(atomB);
				if(mergeTest(grainA, grainB)) {
					mergedAtoms[atomA] = true;
					done = false;
				}
			}
		}
		if(done) break;
	}

	// Now assign final contiguous IDs to parent grains.
	_grainCount = assignIdsToGrains();

	return _grainCount;
}

/******************************************************************************
* Determines the reverse mapping from ghost atoms to local atoms (assuming serial mode).
******************************************************************************/
void GrainIdentification::generateGhostToLocalAtomMap()
{
	_ghostToLocalMap.clear();
	for(int dim = 0; dim < 3; dim++) {
		if(structure().pbc(dim) == false) continue;
		for(int dir = 0; dir <= 1; dir++) {
			const vector<int>& localAtomList = structure().ghostCommunicationList(dim, dir);
			_ghostToLocalMap.insert(_ghostToLocalMap.end(), localAtomList.begin(), localAtomList.end());
		}
	}
	CALIB_ASSERT(_ghostToLocalMap.size() == structure().numGhostAtoms());
	BOOST_FOREACH(int& atomIndex, _ghostToLocalMap) {
		while(structure().isGhostAtom(atomIndex)) {
			CALIB_ASSERT(atomIndex < _ghostToLocalMap.size() + structure().numLocalAtoms());
			atomIndex = _ghostToLocalMap[atomIndex - structure().numLocalAtoms()];
		}
		CALIB_ASSERT(structure().isLocalAtom(atomIndex));
	}
}

/******************************************************************************
* Computes the local orientation tensor for all atoms whose structure has been identified.
******************************************************************************/
void GrainIdentification::computeAtomicOrientationTensors()
{
	// Compute the local orientation tensors for all lattice atoms.
	ElasticAtomTensor elasticAtomTensor(patternAnalysis());
	vector<Grain>::iterator grain = _grains.begin();
	for(int atomIndex = 0; atomIndex < structure().numLocalAtoms(); atomIndex++, ++grain) {
		if(patternAnalysis().atomCluster(atomIndex) != NULL) {
			if(elasticAtomTensor.calculateElasticTensor(atomIndex, grain->orientation)) {
				grain->cluster = patternAnalysis().atomCluster(atomIndex);
				grain->latticeAtomCount = 1;
			}
		}
	}
}

/******************************************************************************
* Tests if two grain should be merged and merges them if deemed necessary.
******************************************************************************/
bool GrainIdentification::mergeTest(Grain& grainA, Grain& grainB, bool allowForFluctuations)
{
	if(&grainA == &grainB)
		return false;
	if(grainA.cluster == NULL && grainB.cluster == NULL)
		return false;

	if(grainA.cluster != NULL && grainB.cluster != NULL) {
		Matrix3 alignmentTM;
		CAFloat misorientation = calculateMisorientation(grainA, grainB, &alignmentTM);

		if(allowForFluctuations)
			misorientation -= _fluctuationTolerance * sqrt(1.0 / CAFloat(grainA.latticeAtomCount) + 1.0 / CAFloat(grainB.latticeAtomCount));

		if(misorientation >= _misorientationThreshold &&
				grainA.latticeAtomCount >= _minGrainAtomCount && grainB.latticeAtomCount >= _minGrainAtomCount)
			return false;

		// Join the two grains.
		if(grainA.rank > grainB.rank ||
				(grainA.cluster->pattern->isLattice() && !grainB.cluster->pattern->isLattice())) {
			grainA.join(grainB, alignmentTM);
		}
		else {
			grainB.join(grainA, alignmentTM.inverse());
			if(grainA.rank == grainB.rank)
				grainB.rank++;
		}
	}
	else {
		// Join the two grains.
		if(grainA.cluster != NULL)
			grainA.join(grainB);
		else
			grainB.join(grainA);
	}

	return true;
}

/******************************************************************************
* Calculates the misorientation angle between two lattice orientations.
******************************************************************************/
CAFloat GrainIdentification::calculateMisorientation(const Grain& grainA, const Grain& grainB, Matrix3* alignmentTM)
{
	Cluster* clusterA = grainA.cluster;
	Cluster* clusterB = grainB.cluster;
	CALIB_ASSERT(clusterA != NULL && clusterB != NULL);
	Matrix3 inverseOrientationA = grainA.orientation.inverse();

	if(clusterB == clusterA) {
		if(alignmentTM) alignmentTM->setIdentity();
		return angleFromMatrix(grainB.orientation * inverseOrientationA);
	}
	else if(clusterA->pattern == clusterB->pattern) {
		const SuperPattern& pattern = *clusterA->pattern;
		CAFloat smallestAngle = CAFLOAT_MAX;
		for(int nodeIndex = 0; nodeIndex < pattern.numCoreNodes; nodeIndex++) {
			BOOST_FOREACH(const SpaceGroupElement& sge, pattern.nodes[nodeIndex].spaceGroupEntries) {
				CAFloat angle = angleFromMatrix(grainB.orientation * sge.tm * inverseOrientationA);
				if(angle < smallestAngle) {
					smallestAngle = angle;
					if(alignmentTM)
						*alignmentTM = sge.tm;
				}
			}
		}
		return smallestAngle;
	}
	else {
		ClusterTransition* t = patternAnalysis().clusterGraph().determineClusterTransition(clusterA, clusterB);
		if(t != NULL) {
			if(alignmentTM) *alignmentTM = t->tm;
			return angleFromMatrix(grainB.orientation * t->tm * inverseOrientationA);
		}
		return CAFLOAT_MAX;
	}
}

/******************************************************************************
* Computes the angle of rotation from a rotation matrix.
******************************************************************************/
CAFloat GrainIdentification::angleFromMatrix(const Matrix3& tm)
{
	CAFloat trace = tm.trace() - 1.0;
	Vector3 axis(tm(2,1) - tm(1,2), tm(0,2) - tm(2,0), tm(1,0) - tm(0,1));
	CAFloat angle = atan2(axis.norm(), trace);
	if(angle > CAFLOAT_PI)
		return (2.0 * CAFLOAT_PI) - angle;
	else
		return angle;
}

/******************************************************************************
* Assigns contiguous IDs to all parent grains.
******************************************************************************/
AtomInteger GrainIdentification::assignIdsToGrains()
{
	AtomInteger numGrains = 0;
	for(int atomIndex = 0; atomIndex < structure().numLocalAtoms(); atomIndex++) {
		Grain& atomicGrain = _grains[atomIndex];
		Grain& rootGrain = parentGrain(atomIndex);
		if(rootGrain.cluster != NULL) {
			CALIB_ASSERT(rootGrain.atomCount >= _minGrainAtomCount);
			if(atomicGrain.isRoot()) {
				rootGrain.id = ++numGrains;
			}
		}
		else rootGrain.id = 0;
	}
	return numGrains;
}

}; // End of namespace
