///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#ifndef __CRYSTAL_ANALYSIS_FIXED_CAPACITY_VECTOR_H
#define __CRYSTAL_ANALYSIS_FIXED_CAPACITY_VECTOR_H

#include "../CALib.h"

namespace CALib {

/**
 * An STL vector with fixed maximum capacity but variable length.
 */
template<typename T, std::size_t C>
class FixedCapacityVector
{
public:

	// type definitions
	typedef T              value_type;
	typedef T*             iterator;
	typedef const T*       const_iterator;
	typedef T&             reference;
	typedef const T&       const_reference;
	typedef int            size_type;
	typedef std::ptrdiff_t difference_type;

	// Default constructor
	FixedCapacityVector() : _size(0) {}

	// Copy constructor
	FixedCapacityVector(const FixedCapacityVector& other) : _size(other._size) {
		std::copy(other.begin(), other.end(), begin());
	}

	// Size constructor
	FixedCapacityVector(size_type size) : _size(size) {
		CALIB_ASSERT(size <= size_type(C));
	}

	// Iterator support
	iterator        begin()       { return _elems; }
	const_iterator  begin() const { return _elems; }
	const_iterator cbegin() const { return _elems; }

	iterator        end()       { return _elems+size(); }
	const_iterator  end() const { return _elems+size(); }
	const_iterator cend() const { return _elems+size(); }

	// operator[]
	reference operator[](size_type i) {
		CALIB_ASSERT(i < size());
		return _elems[i];
	}

	const_reference operator[](size_type i) const {
		CALIB_ASSERT(i < size());
		return _elems[i];
	}

	// at() with range check
	reference at(size_type i) { rangecheck(i); return _elems[i]; }
	const_reference at(size_type i) const { rangecheck(i); return _elems[i]; }

	// front() and back()
	reference front() { return _elems[0]; }

    const_reference front() const { return _elems[0]; }

    reference back() {
    	CALIB_ASSERT(!empty());
    	return _elems[size()-1];
    }

    const_reference back() const {
    	CALIB_ASSERT(!empty());
        return _elems[size()-1];
    }

	size_type size() const { return _size; }
	bool empty() const { return _size == 0; }
	size_type max_size() const { return size_type(C); }

	// direct access to data (read-only)
	const T* data() const { return _elems; }
	T* data() { return _elems; }

	// assignment with type conversion
    template<typename T2>
    FixedCapacityVector<T,C>& operator=(const FixedCapacityVector<T2,C>& rhs) {
    	_size = rhs._size;
    	std::copy(rhs.begin(), rhs.end(), begin());
    	return *this;
	}

	// Sets the number of elements.
	void resize(size_type n) {
		CALIB_ASSERT(n <= size_type(C));
    	_size = n;
	}

	// Assign one value to all elements
	void assign(size_type n, const T& value) {
		CALIB_ASSERT(n <= size_type(C));
    	_size = n;
		std::fill_n(begin(), n, value);
	}

	// Assign one value to all elements
	void fill(const T& value) {
		std::fill_n(begin(), size(), value);
	}

	/// Appends one element to the vector.
	void push_back(const T& value) {
		CALIB_ASSERT(_size < size_type(C));
		_elems[_size++] = value;
	}

	/// Clears the vector.
	void clear() {
		_size = 0;
	}

private:

    // Check range
    void rangecheck(size_type i) const {
    	if(i >= size())
    		throw std::out_of_range("FixedCapacityVector: index out of range");
    }

    /// Stores the current size of the vector.
	size_type _size;

    /// Fixed-size array of elements of type T
	T _elems[C];
};

}; // End of namespace

#endif // __CRYSTAL_ANALYSIS_FIXED_CAPACITY_VECTOR_H

