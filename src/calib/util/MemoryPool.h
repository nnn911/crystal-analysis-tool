///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#ifndef __CRYSTAL_ANALYSIS_MEMORY_POOL_H
#define __CRYSTAL_ANALYSIS_MEMORY_POOL_H

#include "../CALib.h"

namespace CALib {

/**
 * This helper class provides an object pool.
 * Instances of a certain class/struct can be allocated by this class very efficiently.
 * A restriction is that all instances can only be destroyed at once.
 */
template<typename T>
class MemoryPool
{
public:

	MemoryPool(size_t _pageSize = 1024) : lastPageNumber(_pageSize), pageSize(_pageSize) {}
	~MemoryPool() { clear(); }

	T* construct() {
		T* p = malloc();
		new (p) T();
		return p;
	}

	template <typename T0>
	T* construct(T0& a0) {
		T* p = malloc();
		new (p) T(a0);
		return p;
	}

	template <typename T0>
	T* construct(const T0& a0) {
		T* p = malloc();
		new (p) T(a0);
		return p;
	}

	template <typename T0, typename T1>
	T* construct(T0& a0, T1& a1) {
		T* p = malloc();
		new (p) T(a0, a1);
		return p;
	}

	template <typename T0, typename T1>
	T* construct(const T0& a0, const T1& a1) {
		T* p = malloc();
		new (p) T(a0, a1);
		return p;
	}

	template <typename T0, typename T1, typename T2>
	T* construct(T0& a0, T1& a1, T2& a2) {
		T* p = malloc();
		new (p) T(a0, a1, a2);
		return p;
	}

	template <typename T0, typename T1, typename T2>
	T* construct(const T0& a0, const T1& a1, const T2& a2) {
		T* p = malloc();
		new (p) T(a0, a1, a2);
		return p;
	}

	template <typename T0, typename T1, typename T2, typename T3>
	T* construct(T0& a0, T1& a1, T2& a2, T3& a3) {
		T* p = malloc();
		new (p) T(a0, a1, a2, a3);
		return p;
	}

	template <typename T0, typename T1, typename T2, typename T3>
	T* construct(const T0& a0, const T1& a1, const T2& a2, const T3& a3) {
		T* p = malloc();
		new (p) T(a0, a1, a2, a3);
		return p;
	}

	template <typename T0, typename T1, typename T2, typename T3, typename T4>
	T* construct(T0& a0, T1& a1, T2& a2, T3& a3, T4& a4) {
		T* p = malloc();
		new (p) T(a0, a1, a2, a3, a4);
		return p;
	}

	inline void clear(bool keepPageReserved = false) {
		for(typename std::vector<T*>::const_iterator i = pages.begin(); i != pages.end(); ++i) {
			T* p = *i;
			T* pend = p + pageSize;
			if(i+1 == pages.end())
				pend = p + lastPageNumber;
			for(; p != pend; ++p)
				p->~T();
			if(!keepPageReserved || i != pages.begin())
				delete[] (unsigned char*)*i;
		}
		if(!keepPageReserved) {
			pages.clear();
			lastPageNumber = pageSize;
		}
		else if(!pages.empty()) {
			pages.resize(1);
			lastPageNumber = 0;
		}
	}

	size_t memoryUsage() const {
		return pages.size() * pageSize * sizeof(T);
	}

private:

	T* malloc() {
		T* p;
		if(lastPageNumber == pageSize) {
			pages.push_back(p = (T*)new unsigned char[pageSize * sizeof(T)]);
			lastPageNumber = 1;
			CALIB_ASSERT_MSG(p != NULL, "MemoryPool::malloc()", "Out of memory.");
		}
		else {
			p = pages.back() + lastPageNumber;
			lastPageNumber++;
		}
		return p;
	}

	std::vector<T*> pages;
	size_t lastPageNumber;
	size_t pageSize;
};

}; // End of namespace

#endif // __CRYSTAL_ANALYSIS_MEMORY_POOL_H

