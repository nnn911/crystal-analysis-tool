///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#ifndef __CRYSTAL_ANALYSIS_MPI_GATHER_HELPER_H
#define __CRYSTAL_ANALYSIS_MPI_GATHER_HELPER_H

#include "../CALib.h"
#include "../context/CAContext.h"

namespace CALib {

/******************************************************************************
* Helper class that can be used to transmit a variable number of data items
* from all processors to the master processor.
******************************************************************************/
template<typename T>
class MPIGatherHelper : public ContextReference
{
public:

	/// Constructor.
	MPIGatherHelper(CAContext& _context) : ContextReference(_context), numReceivedItems(0) {}

	/// Sets up the communication.
	void initialize(int numLocalItems) {
		this->numLocalItems = numLocalItems;
		maxItemsPerProc = parallel().reduce_max(numLocalItems);
		buffer.resize(numLocalItems);
		currentProc = 0;
	}

	/// Performs a transmission between one processor and the master.
	/// This function is called by both the master and the other processors. The master calls
	/// it multiple times until it returns false.
	bool communicate() {
		if(parallel().isMaster() == false) {

			// Send buffer to master processor.
			parallel().send(buffer, 0);

			receiveEndIterator = buffer.begin();
			return false;
		}
		else {
			if(currentProc == parallel().processorCount())
				return false;

			if(currentProc != 0) {
				// Receive atoms from other processor.
				buffer.resize(maxItemsPerProc);
				numReceivedItems = parallel().receive(buffer, currentProc);
				currentProc++;
				receiveEndIterator = buffer.begin() + numReceivedItems;
			}
			else {
				// Use our own atoms.
				numReceivedItems = numLocalItems;
				currentProc = 1;
				receiveEndIterator = buffer.end();
			}

			return true;
		}
	}

	/// Returns iterator used to fill the send buffer.
	typename std::vector<T>::iterator beginSend() { return buffer.begin(); }

	/// Returns end iterator used to fill the send buffer.
	typename std::vector<T>::iterator endSend() { return buffer.end(); }

	/// Returns iterator used to read data from the receive buffer.
	typename std::vector<T>::const_iterator beginReceive() const { return buffer.begin(); }

	/// Returns end iterator used to read data from the receive buffer.
	typename std::vector<T>::const_iterator endReceive() const { return receiveEndIterator; }

private:

	/// Send/receive buffer.
	std::vector<T> buffer;

	/// The number of items local to this processor.
	int numLocalItems;

	/// The maximum number of items a single processor transmits.
	int maxItemsPerProc;

	/// The remote processor we are receiving data from.
	int currentProc;

	/// The number of items received form the remote processor.
	int numReceivedItems;

	/// The end of the data array received from the remote processor.
	typename std::vector<T>::const_iterator receiveEndIterator;
};

}; // End of namespace

#endif // __CRYSTAL_ANALYSIS_MPI_GATHER_HELPER_H

