///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#ifndef __CA_COLOR_H
#define __CA_COLOR_H

#include "../CALib.h"

namespace CALib {

/// \brief Converts a color from hue, saturation, value representation to RGB representation.
/// \param hue The hue value between zero and one.
/// \param saturation The saturation value between zero and one.
/// \param value The value of the color between zero and one.
/// \return The color in RGB representation.
inline Vector3 HSV2RGB(CAFloat hue, CAFloat saturation, CAFloat value)
{
	if(saturation == 0.0) {
		return Vector3(value, value, value);
	}
	else {
		CAFloat f, p, q, t;
		int i;
		if(hue >= 1.0 || hue < 0.0) hue = 0.0;
		hue *= 6.0;
		i = (int)floor(hue);
		f = hue - (CAFloat)i;
		p = value * (1.0 - saturation);
		q = value * (1.0 - (saturation * f));
		t = value * (1.0 - (saturation * (1.0 - f)));
		switch(i) {
			case 0: return Vector3(value, t, p);
			case 1: return Vector3(q, value, p);
			case 2: return Vector3(p, value, t);
			case 3: return Vector3(p, q, value);
			case 4: return Vector3(t, p, value);
			case 5: return Vector3(value, p, q);
			default:
				return Vector3(value, value, value);
		}
	}
}

}; // End of namespace

#endif // __CA_COLOR_H

