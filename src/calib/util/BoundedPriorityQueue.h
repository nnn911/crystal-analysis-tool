///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#ifndef __CA_BOUNDED_PRIORITY_QUEUE_H
#define __CA_BOUNDED_PRIORITY_QUEUE_H

#include "../CALib.h"

namespace CALib {

/**
 * A priority queue with fixed maximum capacity.
 * While the queue has not reached its maximum capacity, elements are
 * inserted as they will be in a heap, the root (top()) being such that
 * Compare(top(),x)=false for any x in the queue.
 * Once the queue is full, trying to insert x in the queue will have no effect if
 * Compare(x,top())=false. Otherwise, the element at the root of the heap is removed
 * and x is inserted so as to keep the heap property.
 */
template<typename T, typename Compare = std::less<T> >
class BoundedPriorityQueue
{
public:
	typedef T value_type;
	typedef typename std::vector<value_type>::const_iterator const_iterator;

	/// Constructor.
	BoundedPriorityQueue(int size, const Compare& comp = Compare()) : _count(0), _data(size), _comp(comp) {}

	/// Returns the number of elements in the queue.
	unsigned int size() const { return _count; }

	/// Removes all elements of the queue. The max size remains unchanged.
	void clear() { _count = 0; }

	/// Returns whether the maximum queue size has been reached.
	bool full() const { return _count == _data.size(); }

	/// Returns whether the current queue size is zero.
	bool empty() const { return _count == 0; }

	/// Returns greatest element.
	const value_type& top() const { return _data[0]; }

	void insert(const value_type& x)
	{
		value_type* data1 = (&_data[0]-1);
		if(full()) {
			if(_comp(x, top())) {
				unsigned int j(1), k(2);
				while (k <= _count) {
					value_type* z = &(data1[k]);
					if((k < _count) && _comp(*z, data1[k+1]))
						z = &(data1[++k]);

					if(_comp(*z, x)) break;
					data1[j] = *z;
					j = k;
					k = j << 1;
				}
				data1[j] = x;
			}
		}
		else {
			int i(++_count), j;
			while(i >= 2) {
				j = i >> 1;
				value_type& y = data1[j];
				if(_comp(x, y)) break;
				data1[i] = y;
				i = j;
			}
			data1[i] = x;
		}
	}

	/// Returns an iterator pointing to the first element in the queue.
	const_iterator begin() const { return _data.begin(); }

	/// Returns an iterator pointing to the element after the last element in the queue.
	const_iterator end() const { return _data.begin() + _count; }

	/// Sort the entries of the queue.
	void sort() { std::sort(_data.begin(), _data.begin() + _count, _comp); }

protected:

	unsigned int _count;
	std::vector<value_type> _data;
	Compare _comp;
};

}; // End of namespace

#endif // __CA_BOUNDED_PRIORITY_QUEUE_H

