///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#ifndef __CRYSTAL_ANALYSIS_LINALG_H
#define __CRYSTAL_ANALYSIS_LINALG_H

#include "../CALib.h"

namespace CALib {

#ifndef CALIB_USE_DOUBLE_PRECISION_FP
	typedef Eigen::Vector3f Vector3;
	typedef Eigen::Matrix3f Matrix3;
#else
	typedef Eigen::Vector3d Vector3;
	typedef Eigen::Matrix3d Matrix3;
#endif

typedef Eigen::Vector3i Vector3I;

typedef Eigen::Transform<CAFloat, 3, Eigen::AffineCompact> AffineTransformation;
typedef Eigen::Hyperplane<CAFloat, 3> Plane3;

/// Writes a vector an output text stream.
inline std::ostream& operator<<(std::ostream& s, const Vector3& v) {
	return s << "(" << v.x() << " " << v.y() << " " << v.z() << ")";
}

/// Writes an integer vector an output text stream.
inline std::ostream& operator<<(std::ostream& s, const Vector3I& v) {
	return s << "(" << v.x() << " " << v.y() << " " << v.z() << ")";
}

/// Writes a matrix an output text stream.
inline std::ostream& operator<<(std::ostream& s, const Matrix3& m) {
	return Eigen::operator<<(s,m) << std::endl;
}

/// Writes an affine transformation matrix an output text stream.
inline std::ostream& operator<<(std::ostream& s, const AffineTransformation& m) {
	return Eigen::operator<<(s,m.matrix()) << std::endl;
}

};	// End of namespace

#include "Matrix3.h"
#include "FrameTransformation.h"

#endif // __CRYSTAL_ANALYSIS_LINALG_H

