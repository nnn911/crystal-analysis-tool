///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#include "AtomicStructureParser.h"
#include "../../atomic_structure/AtomicStructure.h"
#include "../../context/CAContext.h"

using namespace std;

namespace CALib {

/******************************************************************************
* Reads the atomic coordinates from the input file in IMD format.
******************************************************************************/
void AtomicStructureParser::readIMDAtomsFile(TextParserStream& stream)
{
	context().msgLogger() << "Parsing IMD file." << endl;

	Vector3 cellVectors[3];
	int columnPosx = -1;
	int columnPosy = -1;
	int columnPosz = -1;
	int columnId = -1;
	int columnType = -1;
	int numberOfColumns = 0;
	boost::array<bool,3> pbc;
	pbc.assign(false);
	AffineTransformation simulationCell(AffineTransformation::Identity());

	if(parallel().isMaster()) {
		// Read header lines
		while(true) {
			stream.readline();
			if(stream.line().empty() || stream.line().at(0) != '#')
				context().raiseErrorOne("Invalid header in IMD atom file (line %i).", stream.lineNumber());
			if(stream.line().at(1) == '#') continue;
			else if(stream.line().at(1) == 'E') break;
			else if(stream.line().at(1) == 'C') {

				// Parse column names.
				string::const_iterator tokenBegin = stream.line().begin() + 3;
				while(tokenBegin != stream.line().end()) {
					while(tokenBegin != stream.line().end() && (*tokenBegin == ' ' || *tokenBegin == '\t')) ++tokenBegin;
					if(tokenBegin == stream.line().end()) break;
					string::const_iterator tokenEnd = tokenBegin + 1;
					while(tokenEnd != stream.line().end() && *tokenEnd != ' ' && *tokenEnd != '\t') ++tokenEnd;
					string columnName(tokenBegin, tokenEnd);
					if(columnName == "x") columnPosx = numberOfColumns;
					else if(columnName == "y") columnPosy = numberOfColumns;
					else if(columnName == "z") columnPosz = numberOfColumns;
					else if(columnName == "number") columnId = numberOfColumns;
					else if(columnName == "type") columnType = numberOfColumns;
					tokenBegin = tokenEnd;
					numberOfColumns++;
				}
				if(columnPosx == -1) context().raiseErrorOne("File parsing error. Input file dos not contain X coordinate column.");
				if(columnPosy == -1) context().raiseErrorOne("File parsing error. Input file dos not contain Y coordinate column.");
				if(columnPosz == -1) context().raiseErrorOne("File parsing error. Input file dos not contain Z coordinate column.");

			}
			else if(stream.line().at(1) == 'X') {
				if(sscanf(stream.line().c_str()+2, CAFLOAT_SCANF_STRING_3, &cellVectors[0].x(), &cellVectors[0].y(), &cellVectors[0].z()) != 3)
					context().raiseErrorOne("Invalid simulation cell bounds in line %i of IMD file: %s", stream.lineNumber(), stream.line().c_str());
			}
			else if(stream.line().at(1) == 'Y') {
				if(sscanf(stream.line().c_str()+2, CAFLOAT_SCANF_STRING_3, &cellVectors[1].x(), &cellVectors[1].y(), &cellVectors[1].z()) != 3)
					context().raiseErrorOne("Invalid simulation cell bounds in line %i of IMD file: %s", stream.lineNumber(), stream.line().c_str());
			}
			else if(stream.line().at(1) == 'Z') {
				if(sscanf(stream.line().c_str()+2, CAFLOAT_SCANF_STRING_3, &cellVectors[2].x(), &cellVectors[2].y(), &cellVectors[2].z()) != 3)
					context().raiseErrorOne("Invalid simulation cell bounds in line %i of IMD file: %s", stream.lineNumber(), stream.line().c_str());
			}
			else context().raiseErrorOne("Invalid header line key in IMD atom file (line %i).", stream.lineNumber());
		}
		simulationCell.linear().col(0) = cellVectors[0];
		simulationCell.linear().col(1) = cellVectors[1];
		simulationCell.linear().col(2) = cellVectors[2];
	}

	// Setup simulation cell geometry.
	setupSimulationCell(simulationCell, pbc);

	beginDistributeAtoms();
	_numInputAtoms = 0;
	for(;;) {
		AtomInteger nchunk = 0;
		if(parallel().isMaster()) {
			vector<AtomInfo>::iterator atomInfo = _atomDistributionBuffer.begin();
			while(!stream.eof() && atomInfo != _atomDistributionBuffer.end()) {

				stream.readline();
				if(stream.line().empty()) break;

				atomInfo->tag = _numAtomsRead + (AtomInteger)nchunk + 1;
				atomInfo->species = 1;
				string::const_iterator tokenBegin = stream.line().begin();
				int columnIndex = 0;
				while(tokenBegin != stream.line().end()) {
					while(tokenBegin != stream.line().end() && (*tokenBegin == ' ' || *tokenBegin == '\t')) ++tokenBegin;
					if(tokenBegin == stream.line().end()) break;
					string::const_iterator tokenEnd = tokenBegin + 1;
					while(tokenEnd != stream.line().end() && *tokenEnd != ' ' && *tokenEnd != '\t') ++tokenEnd;
					string token(tokenBegin, tokenEnd);
					if(columnPosx == columnIndex) {
						if(sscanf(token.c_str(), CAFLOAT_SCANF_STRING_1, &atomInfo->pos.x()) != 1)
							context().raiseErrorOne("File parsing error. Invalid X coordinate (line %d, column %d): %s", stream.lineNumber(), columnIndex, stream.line().c_str());
					}
					else if(columnPosy == columnIndex) {
						if(sscanf(token.c_str(), CAFLOAT_SCANF_STRING_1, &atomInfo->pos.y()) != 1)
							context().raiseErrorOne("File parsing error. Invalid Y coordinate (line %d, column %d): %s", stream.lineNumber(), columnIndex, stream.line().c_str());
					}
					else if(columnPosz == columnIndex) {
						if(sscanf(token.c_str(), CAFLOAT_SCANF_STRING_1, &atomInfo->pos.z()) != 1)
							context().raiseErrorOne("File parsing error. Invalid Z coordinate (line %d, column %d): %s", stream.lineNumber(), columnIndex, stream.line().c_str());
					}
					else if(columnId == columnIndex) {
						if(sscanf(token.c_str(), "%u", &atomInfo->tag) != 1)
							context().raiseErrorOne("File parsing error. Invalid atom ID (line %d, column %d): %s", stream.lineNumber(), columnIndex, stream.line().c_str());
					}
					else if(columnType == columnIndex) {
						if(sscanf(token.c_str(), "%u", &atomInfo->species) != 1)
							context().raiseErrorOne("File parsing error. Invalid atom type (line %d, column %d): %s", stream.lineNumber(), columnIndex, stream.line().c_str());
					}
					tokenBegin = tokenEnd;
					columnIndex++;
				}
				if(columnIndex != numberOfColumns)
					context().raiseErrorOne("File parsing error. Unexpected end of line (line %i).", stream.lineNumber());

				++atomInfo;
				++nchunk;
			}
		}
		parallel().broadcast(nchunk);
		if(nchunk == 0) break;

		_numInputAtoms += nchunk;
		vector<AtomInfo>::const_iterator atomInfo = _atomDistributionBuffer.begin();
		for(int i = 0; i < nchunk; i++, ++atomInfo)
			distributeAtom(*atomInfo);
	}

	endDistributeAtoms();

	context().msgLogger() << "Read " << _numInputAtoms << " atoms from input file." << endl;
}

}; // End of namespace
