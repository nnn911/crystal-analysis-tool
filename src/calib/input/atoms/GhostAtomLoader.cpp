///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#include "GhostAtomLoader.h"
#include "../../atomic_structure/AtomicStructure.h"
#include "../../context/CAContext.h"

using namespace std;

namespace CALib {

/******************************************************************************
* Loads all required ghost atoms from the decomposed input file.
******************************************************************************/
void GhostAtomLoader::loadGhostAtoms(const string& baseDir)
{
	/// Calculate world-space origin point of the local processor domain.
	Point3 domainOrigin = _structure.processorDomainOrigin();

	for(int dim = 0; dim < 3; dim++) {
		// Calculate normal vector of the current simulation cell side.
		Vector3 normal = Normalize(CrossProduct(_structure.simulationCell().column((dim+1)%3), _structure.simulationCell().column((dim+2)%3)));
		// Flip normal if necessary.
		if(DotProduct(normal, _structure.simulationCell().column(dim)) < 0.0)
			normal = -normal;
		if(_structure.ghostCutoff() * parallel().processorGrid(dim) * 2.0 >= DotProduct(normal, _structure.simulationCell().column(dim)))
			context().raiseErrorAll("Spatial decomposition domains are too small for ghost cutoff (in dimension %i). Please reduce number of spatial subdivisions or ghost cutoff.", dim+1);

		cutNormals[dim][0] = normal;
		cutDistances[dim][0] = -_structure.ghostCutoff() + DotProduct(normal, domainOrigin - ORIGIN);
		cutNormals[dim][1] = -normal;
		cutDistances[dim][1] = -_structure.ghostCutoff() + DotProduct(-normal, domainOrigin + _structure.simulationCell().column(dim) * (CAFloat(1) / parallel().processorGrid(dim)) - ORIGIN);
	}

	Vector3I imageDelta;
	for(imageDelta.X = -1; imageDelta.X <= +1; imageDelta.X++) {
		if(!_structure.pbc(0) && imageDelta.X == -1 && parallel().processorLocation(0) == 0) continue;
		if(!_structure.pbc(0) && imageDelta.X == +1 && parallel().processorLocation(0) == parallel().processorGrid(0) - 1) continue;
		for(imageDelta.Y = -1; imageDelta.Y <= +1; imageDelta.Y++) {
			if(!_structure.pbc(1) && imageDelta.Y == -1 && parallel().processorLocation(1) == 0) continue;
			if(!_structure.pbc(1) && imageDelta.Y == +1 && parallel().processorLocation(1) == parallel().processorGrid(1) - 1) continue;
			for(imageDelta.Z = -1; imageDelta.Z <= +1; imageDelta.Z++) {
				if(!_structure.pbc(2) && imageDelta.Z == -1 && parallel().processorLocation(2) == 0) continue;
				if(!_structure.pbc(2) && imageDelta.Z == +1 && parallel().processorLocation(2) == parallel().processorGrid(2) - 1) continue;
				if(imageDelta == NULL_VECTOR) continue;
				shiftVector = NULL_VECTOR;
				Vector3I procCoordShiftVector = NULL_VECTOR;
				for(unsigned int dim = 0; dim < 3; dim++) {
					if(imageDelta[dim] == -1 && parallel().processorLocation(dim) == 0) {
						CALIB_ASSERT(_structure.pbc(dim));
						shiftVector = -_structure.simulationCell().column(dim);
						procCoordShiftVector[dim] = +parallel().processorGrid(dim);
					}
					else if(imageDelta[dim] == +1 && parallel().processorLocation(dim) == parallel().processorGrid(dim) - 1) {
						CALIB_ASSERT(_structure.pbc(dim));
						shiftVector = _structure.simulationCell().column(dim);
						procCoordShiftVector[dim] = -parallel().processorGrid(dim);
					}
				}
				procCoord = parallel().processorLocationPoint() + imageDelta;
				Point3I imageLocation = procCoord + procCoordShiftVector;
				stringstream ghostFilename;
				ghostFilename << baseDir << "/z" << imageLocation.Z << "/y" << imageLocation.Y <<
						"/split_z" << imageLocation.Z << "_y" << imageLocation.Y << "_x" << imageLocation.X << ".ss";
				readAtomsFile(ghostFilename.str().c_str());
			}
		}
	}

	context().msgLogger() << "Number of ghost atoms: " << _structure.numGhostAtoms() << endl;
}


/******************************************************************************
* Sets up the simulation cell of the atomic structure.
******************************************************************************/
void GhostAtomLoader::setupSimulationCell(Matrix3 simulationCell, Point3 simulationCellOrigin, boost::array<bool,3> pbc)
{
	// Cell geometry has already been specified when loading the real atoms.
}

/******************************************************************************
* Is called before atoms are read from the input file.
******************************************************************************/
void GhostAtomLoader::beginDistributeAtoms()
{
	CALIB_ASSERT(_preprocessing.replicate[0] >= 1);
	CALIB_ASSERT(_preprocessing.replicate[1] >= 1);
	CALIB_ASSERT(_preprocessing.replicate[2] >= 1);

	_structure.beginAddingAtoms(max(0, _numInputAtoms / 4));
	_numAtomsRead = 0;

	// Flag that indicates whether the atomic coordinates are being transformed during parsing.
	_applyCellTransformation = (_preprocessing.cellDeformation != IDENTITY);
}

/******************************************************************************
* Is called for every atom read from the input file.
******************************************************************************/
void GhostAtomLoader::distributeAtom(const AtomInfo& atom)
{
	CALIB_ASSERT(parallel().isMaster());
	++_numAtomsRead;

	// Apply affine transformation.
	if(_applyCellTransformation)
		_currentDistributionAtom->pos = _structure.simulationCellOrigin() + _preprocessing.cellDeformation * (_currentDistributionAtom->pos - _structure.simulationCellOrigin());

	// Wrap at periodic boundaries.
	Point3 wrappedPos = _structure.wrapPoint(atom.pos);

	// Shift by PBC vector.
	wrappedPos += shiftVector;

	// Clip at ghost cut planes.
	for(int dim = 0; dim < 3; dim++) {
		for(int dir = 0; dir < 2; dir++) {
			if(DotProduct(wrappedPos - ORIGIN, cutNormals[dim][dir]) < cutDistances[dim][dir])
				return;
		}
	}

	_structure.addAtom(wrappedPos, atom.tag, atom.species, procCoord, true);
}

/******************************************************************************
* Is called after atoms have been read from the input file.
*****************************************************************************/
void GhostAtomLoader::endDistributeAtoms()
{
	CALIB_ASSERT(_numAtomsRead == _numInputAtoms);
	_structure.endAddingAtoms();
}

}; // End of namespace
