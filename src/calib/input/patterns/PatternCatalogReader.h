///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#ifndef __CA_PATTERN_CATALOG_READER_H
#define __CA_PATTERN_CATALOG_READER_H

#include "../../CALib.h"
#include "../../context/CAContext.h"
#include "../../pattern/catalog/PatternCatalog.h"

namespace CALib {

/**
 * Reads a PatternCatalog object from a file.
 */
class PatternCatalogReader : public ContextReference
{
public:

	/// Constructor.
	PatternCatalogReader(CAContext& context) : ContextReference(context) {}

	/// Deserializes a PatternCatalog object from a file.
	void read(std::istream& stream, PatternCatalog& catalog);

private:

	CoordinationPattern* readCoordinationPattern(std::istream& stream, PatternCatalog& catalog);
	SuperPattern* readSuperPattern(std::istream& stream, PatternCatalog& catalog);
	void readVector(std::istream& stream, Vector3& v) { stream >> v.x() >> v.y() >> v.z(); }
	void readMatrix(std::istream& stream, Matrix3& m) {
		for(int i = 0; i < 3; i++)
			stream >> m(i,0) >> m(i,1) >> m(i,2);
	}

	int _fileFormatVersion;
};

}; // End of namespace

#endif // __CA_PATTERN_CATALOG_READER_H
