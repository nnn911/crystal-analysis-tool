///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#include "CrystalPathFinder.h"

using namespace std;

namespace CALib {

/******************************************************************************
* Finds an atom-to-atom path from atom 1 to atom 2 that lies entirely in the
* good crystal region. Returns true if a path could be found and stores the
* corresponding ideal vector and the cluster transition in the provided
* pass-by-reference variables.
******************************************************************************/
boost::optional<ClusterVector> CrystalPathFinder::findPath(int atomIndex1, int atomIndex2)
{
	// Test if atom 2 is a direct neighbor of atom 1.
	if(patternAnalysis().atomCluster(atomIndex1) != NULL) {
		const SuperPatternNode& patternNode = patternAnalysis().atomPatternNode(atomIndex1);
		for(int nodeNeighborIndex = 0; nodeNeighborIndex < patternNode.coordinationPattern->numNeighbors(); nodeNeighborIndex++) {
			// Resolve pattern node neighbor to actual neighbor atom.
			int atomNeighborIndex = patternAnalysis().nodeNeighborToAtomNeighbor(atomIndex1, nodeNeighborIndex);
			int neighborIndex = patternAnalysis().neighborList().neighbor(atomIndex1, atomNeighborIndex);
			if(neighborIndex == atomIndex2) {
				const Vector3& refv = patternNode.neighbors[nodeNeighborIndex].referenceVector;
				Cluster* cluster = patternAnalysis().atomCluster(atomIndex1);
				return boost::optional<ClusterVector>(ClusterVector(refv, cluster));
			}
		}
	}

	// Test if atom 1 is a direct neighbor of atom 2.
	else if(patternAnalysis().atomCluster(atomIndex2) != NULL) {
		const SuperPatternNode& patternNode = patternAnalysis().atomPatternNode(atomIndex2);
		for(int nodeNeighborIndex = 0; nodeNeighborIndex < patternNode.coordinationPattern->numNeighbors(); nodeNeighborIndex++) {
			// Resolve pattern node neighbor to actual neighbor atom.
			int atomNeighborIndex = patternAnalysis().nodeNeighborToAtomNeighbor(atomIndex2, nodeNeighborIndex);
			int neighborIndex = patternAnalysis().neighborList().neighbor(atomIndex2, atomNeighborIndex);
			if(neighborIndex == atomIndex1) {
				const Vector3& refv = patternNode.neighbors[nodeNeighborIndex].referenceVector;
				Cluster* cluster = patternAnalysis().atomCluster(atomIndex2);
				return boost::optional<ClusterVector>(ClusterVector(-refv, cluster));
			}
		}
	}

	if(_maxPathLength == 1)
		return boost::optional<ClusterVector>();

	_nodePool.clear(true);

	PathNode start(atomIndex1, Vector3::Zero());
	start.distance = 0;

	// Mark the head atom as visited.
	_visitedAtoms.set(atomIndex1);

	// Process items from queue until it becomes empty or the destination atom has been reached.
	PathNode* end_of_queue = &start;
	boost::optional<ClusterVector> result;
	for(PathNode* current = &start; current != NULL && !result; current = current->nextToProcess) {
		int currentAtom = current->atomIndex;
		CALIB_ASSERT(currentAtom != atomIndex2);
		CALIB_ASSERT(_visitedAtoms.test(currentAtom) == true);

		if(patternAnalysis().atomCluster(currentAtom) != NULL) {
			const SuperPatternNode& patternNode = patternAnalysis().atomPatternNode(currentAtom);
			for(int nodeNeighborIndex = 0; nodeNeighborIndex < patternNode.coordinationPattern->numNeighbors(); nodeNeighborIndex++) {
				// Resolve pattern node neighbor to actual neighbor atom.
				int atomNeighborIndex = patternAnalysis().nodeNeighborToAtomNeighbor(currentAtom, nodeNeighborIndex);
				int neighborIndex = patternAnalysis().neighborList().neighbor(currentAtom, atomNeighborIndex);

				// Skip neighbor atom if it has been visited before.
				if(_visitedAtoms.test(neighborIndex)) continue;

				// Check if maximum path length has been reached.
				if(current->distance >= _maxPathLength - 1 && neighborIndex != atomIndex2)
					continue;

				// Compute the new ideal vector from the start atom to the current neighbor atom.
				ClusterVector pathVector(current->idealVector);
				ClusterVector step(patternNode.neighbors[nodeNeighborIndex].referenceVector, patternAnalysis().atomCluster(currentAtom));
				CALIB_ASSERT(step.cluster() != NULL);

				// Concatenate the two cluster vectors.
				if(pathVector.cluster() == step.cluster())
					pathVector.localVec() += step.localVec();
				else if(pathVector.cluster() != NULL) {
					CALIB_ASSERT(step.cluster() != NULL);
					CALIB_ASSERT(pathVector.cluster() != NULL);
					ClusterTransition* transition = clusterGraph().determineClusterTransition(step.cluster(), pathVector.cluster());
					if(!transition)
						continue;	// Failed to concatenate cluster vectors.
					pathVector.localVec() += transition->transform(step.localVec());
				}
				else pathVector = step;

				// Did we reach the destination atom already?
				if(neighborIndex == atomIndex2) {
					result = pathVector;
					break;
				}

				// Put current neighbor atom on recursive stack.
				if(current->distance < _maxPathLength - 1) {
					// Put neighbor at end of the queue.
					PathNode* neighborNode = _nodePool.construct(neighborIndex, pathVector);
					neighborNode->distance = current->distance + 1;
					CALIB_ASSERT(end_of_queue->nextToProcess == NULL);
					end_of_queue->nextToProcess = neighborNode;
					end_of_queue = neighborNode;

					// Mark as visited.
					_visitedAtoms.set(neighborIndex);
				}
			}
		}
		else {
			bool foundNeighbor = false;
			BOOST_FOREACH(const NeighborList::neighbor_info& neighbor, patternAnalysis().neighborList().neighbors(currentAtom)) {
				if(foundNeighbor) break;
				if(patternAnalysis().atomCluster(neighbor.index) == NULL) continue;
				if(_visitedAtoms.test(neighbor.index)) continue;
				if(current->distance >= _maxPathLength - 1 && neighbor.index != atomIndex2) continue;
				const SuperPatternNode& patternNode = patternAnalysis().atomPatternNode(neighbor.index);
				for(int nodeNeighborIndex = 0; nodeNeighborIndex < patternNode.coordinationPattern->numNeighbors(); nodeNeighborIndex++) {
					// Resolve pattern node neighbor to actual neighbor atom.
					int atomNeighborIndex = patternAnalysis().nodeNeighborToAtomNeighbor(neighbor.index, nodeNeighborIndex);
					int neighborIndex = patternAnalysis().neighborList().neighbor(neighbor.index, atomNeighborIndex);
					if(neighborIndex == currentAtom) {
						foundNeighbor = true;

						// Compute the new ideal vector from the start atom to the current neighbor atom.
						ClusterVector pathVector(current->idealVector);
						ClusterVector step(-patternNode.neighbors[nodeNeighborIndex].referenceVector, patternAnalysis().atomCluster(neighbor.index));

						// Concatenate the two cluster vectors.
						if(pathVector.cluster() == step.cluster())
							pathVector.localVec() += step.localVec();
						else if(pathVector.cluster() != NULL) {
							CALIB_ASSERT(step.cluster() != NULL);
							CALIB_ASSERT(pathVector.cluster() != NULL);
							ClusterTransition* transition = clusterGraph().determineClusterTransition(step.cluster(), pathVector.cluster());
							if(!transition)
								break;	// Failed to concatenate cluster vectors.
							pathVector.localVec() += transition->transform(step.localVec());
						}
						else pathVector = step;

						// Did we reach the destination atom already?
						if(neighbor.index == atomIndex2) {
							result = pathVector;
							break;
						}

						// Put current neighbor atom on recursive stack.
						if(current->distance < _maxPathLength - 1) {
							// Put neighbor at end of the queue.
							PathNode* neighborNode = _nodePool.construct(neighbor.index, pathVector);
							neighborNode->distance = current->distance + 1;
							CALIB_ASSERT(end_of_queue->nextToProcess == NULL);
							end_of_queue->nextToProcess = neighborNode;
							end_of_queue = neighborNode;

							// Mark as visited.
							_visitedAtoms.set(neighbor.index);
						}

						break;
					}
				}
			}
		}
	}

	// Cleanup visit flags.
	for(PathNode* current = &start; current != NULL; current = current->nextToProcess)
		_visitedAtoms.reset(current->atomIndex);

	return result;
}

}; // End of namespace
