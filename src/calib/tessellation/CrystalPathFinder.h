///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#ifndef __CA_CRYSTAL_PATH_FINDER_H
#define __CA_CRYSTAL_PATH_FINDER_H

#include "../CALib.h"
#include "../context/CAContext.h"
#include "../pattern/superpattern/SuperPatternAnalysis.h"
#include "../cluster_graph/ClusterVector.h"
#include "../cluster_graph/ClusterGraph.h"

namespace CALib {

/**
 * Utility class that can find the shortest connecting path between two atoms
 * (which may or may not be nearest neighbors) in the good crystal region.
 *
 * If a connecting path can be found, the routine returns the vector connecting the
 * two atoms in the ideal stress-free configuration.
 */
class CrystalPathFinder : public ContextReference
{
public:

	/// Constructor.
	CrystalPathFinder(SuperPatternAnalysis& patternAnalysis, int maxPathLength) : ContextReference(patternAnalysis.context()),
		_patternAnalysis(patternAnalysis), _nodePool(1024),
		_visitedAtoms(patternAnalysis.structure().numLocalAtoms() + patternAnalysis.structure().numGhostAtoms()),
		_maxPathLength(maxPathLength) {
		CALIB_ASSERT(maxPathLength >= 1);
	}

	/// Returns a reference to the underlying results of the pattern analysis.
	const SuperPatternAnalysis& patternAnalysis() const { return _patternAnalysis; }

	/// Returns a reference to the cluster graph.
	ClusterGraph& clusterGraph() { return _patternAnalysis.clusterGraph(); }

	/// Returns a const-reference to the cluster graph.
	const ClusterGraph& clusterGraph() const { return _patternAnalysis.clusterGraph(); }

	/// Finds an atom-to-atom path from atom 1 to atom 2 that lies entirely in the good crystal region.
	/// If a path could be found, returns the corresponding ideal vector connecting the two
	/// atoms in the ideal stress-free reference configuration.
	boost::optional<ClusterVector> findPath(int atomIndex1, int atomIndex2);

private:

	/**
	 * This data structure is used for the shortest path search.
	 */
	struct PathNode
	{
		/// Constructor
		PathNode(int _atomIndex, const ClusterVector& _idealVector) :
			atomIndex(_atomIndex), idealVector(_idealVector), nextToProcess(NULL) {}

		/// The atom index.
		int atomIndex;

		/// The vector from the start atom of the path to this atom.
		ClusterVector idealVector;

		/// Number of steps between this atom and the start atom of the recursive walk.
		int distance;

		/// Linked list.
		PathNode* nextToProcess;
	};

	/// The underlying results of the pattern analysis.
	SuperPatternAnalysis& _patternAnalysis;

	/// A memory pool to create PathNode instances.
	MemoryPool<PathNode> _nodePool;

	/// Array to keep track of which atoms
	boost::dynamic_bitset<> _visitedAtoms;

	/// The maximum length of an atom-to-atom path.
	/// A length of 1 would only return paths between direct neighbor atoms.
	int _maxPathLength;
};

}; // End of namespace

#endif // __CA_CRYSTAL_PATH_FINDER_H
