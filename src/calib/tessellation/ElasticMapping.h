///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#ifndef __CA_ELASTIC_MAPPING_H
#define __CA_ELASTIC_MAPPING_H

#include "../CALib.h"
#include "../context/CAContext.h"
#include "../pattern/superpattern/SuperPatternAnalysis.h"
#include "../cluster_graph/ClusterVector.h"
#include "DelaunayTessellation.h"

namespace CALib {

struct Cluster;

/**
 * Computes the elastic mapping from the current atomistic configuration to a stress-free
 * reference state.
 */
class ElasticMapping : public ContextReference
{
public:

	/// Data structure associated with each edge of the tessellation.
	struct TessellationEdge {

		/// Constructor.
		TessellationEdge(int v1, int v2) : vertex1(v1), vertex2(v2), clusterTransition(NULL), isIncompatible(false) {}

		/// The vertex this edge is originating from.
		int vertex1;

		/// The vertex this edge is pointing to.
		int vertex2;

		/// The vector corresponding to this edge when mapped to the stress-free reference configuration.
		Vector3 clusterVector;

		/// The transition when going from the cluster assigned to vertex 1 to the cluster assigned to vertex 2.
		ClusterTransition* clusterTransition;

		/// The reverse edge, connecting vertex 2 to vertex 1.
		TessellationEdge* reverse;

		/// The next edge in the linked-list of edges leaving vertex 1.
		TessellationEdge* next;

		/// Marks this edge as incompatible, i.e. it has been assigned two different ideal vectors.
		bool isIncompatible;

		/// Returns true if this edge has been assigned a corresponding vector in the ideal configuration of the local cluster.
		bool hasClusterVector() const { return clusterTransition != NULL; }

		/// Assigns a vector to this edge and the negative vector to its reverse edge.
		/// Also stores the cluster transition that connects the two clusters of the two vertices.
		void assignClusterVector(const Vector3& v, ClusterTransition* transition) {
			clusterVector = v;
			clusterTransition = transition;
			reverse->clusterVector = transition->transform(-v);
			reverse->clusterTransition = transition->reverse;
		}

		/// Removes the assigned cluster vector.
		void clearClusterVector() {
			clusterTransition = reverse->clusterTransition = NULL;
		}
	};


public:

	/// Constructor.
	ElasticMapping(const DelaunayTessellation& tessellation, SuperPatternAnalysis& patternAnalysis) : ContextReference(tessellation.context()),
		_tessellation(tessellation), _patternAnalysis(patternAnalysis), _clusterGraph(patternAnalysis.clusterGraph()), _edgeCount(0) {}

	/// Returns the underlying tessellation of the atomistic system.
	const DelaunayTessellation& tessellation() const { return _tessellation; }

	/// Returns the results of the pattern analysis.
	const SuperPatternAnalysis& patternAnalysis() const { return _patternAnalysis; }

	/// Returns a const-reference to the cluster graph.
	const ClusterGraph& clusterGraph() const { return _clusterGraph; }

	/// Returns a reference to the cluster graph.
	ClusterGraph& clusterGraph() { return _clusterGraph; }

	/// Returns a reference to the underlying atomic structure.
	const AtomicStructure& structure() const { return _patternAnalysis.structure(); }

	/// Computes the elastic mapping.
	/// If onlyLocalTetrahedra==true, the elastic deformation field is
	/// only computed for tetrahedra that overlap with the local processor domain.
	/// If onlyLocalTetrahedra==false, it is also computed for ghost tetrahedra.
	void generate(bool onlyLocalTetrahedra, int crystalPathSteps, bool dontReconstructIdealEdgeVectors);

	/// Exchanges the ideal vectors assigned to tessellation edges between processors.
	/// The synchronization is performed only for edges of tetrahedra that overlap with
	/// more than one processor domain.
	void synchronize();

	/// Determines whether the elastic mapping from the actual physical configuration
	/// of the crystal to the imaginary, stress-free configuration is compatible
	/// inside the given tessellation cell. Returns false if the mapping is incompatible
	/// or cannot be determined.
	bool isElasticMappingCompatible(DelaunayTessellation::CellHandle cell) const;

	/// Computes the elastic deformation gradient within the given cell.
	/// Return NULL if the gradient could not be determined. Otherwise returns
	/// the atomic cluster whose frame of reference served as the relaxed configuration to compute F_elastic.
	Cluster* computeElasticMapping(DelaunayTessellation::CellHandle cell, Matrix3& F_elastic) const;

	/// Looks up the tessellation edge connecting two tessellation vertices.
	/// Returns NULL if the vertices are not connected by an edge.
	TessellationEdge* findEdge(int vertexIndex1, int vertexIndex2) const {
		CALIB_ASSERT(vertexIndex1 >= 0 && vertexIndex1 < (int)_vertexEdges.size());
		CALIB_ASSERT(vertexIndex2 >= 0 && vertexIndex2 < (int)_vertexEdges.size());
		for(TessellationEdge* e = _vertexEdges[vertexIndex1]; e != NULL; e = e->next)
			if(e->vertex2 == vertexIndex2) return e;
		return NULL;
	}

	/// Maps tessellation vertex indices to atom indices.
	void setVertexToAtomMapping(const std::vector<int>& map) {
		CALIB_ASSERT((int)map.size() == tessellation().structure().numLocalAtoms() + tessellation().structure().numGhostAtoms());
		_vertexToAtomMapping = map;
	}

	/// Returns whether a map from tessellation vertices to atoms in the input structure has been specified.
	/// If no such mapping has been specified, then we assume that the tessellation vertices are the actual atoms.
	bool hasVertexToAtomMapping() const { return !_vertexToAtomMapping.empty(); }

private:

	/// Returns the number of tessellation edges on the local processor.
	int edgeCount() const { return _edgeCount; }

	/// Builds the list of edges in the tetrahedral tessellation.
	void generateTessellationEdges(bool onlyLocalTetrahedra);

	/// Assigns each tessellation vertex to a cluster.
	void assignVerticesToClusters();

	/// Determines the ideal vector corresponding to each edge of the tessellation.
	void assignIdealVectorsToEdges(int crystalPathSteps, bool dontReconstructIdealEdgeVectors);

	/// Tries to compute the ideal vector of tessellation, which are in the disordered region of the crystal.
	void reconstructIdealEdgeVectors();

	/// Returns the cluster to which a vertex of the tessellation has been assigned (may be NULL).
	Cluster* clusterOfVertex(int vertexIndex) const {
		int atomIndex = vertexToAtom(vertexIndex);
		if(atomIndex == -1) return NULL;
		CALIB_ASSERT(atomIndex < (int)_vertexClusters.size());
		return _vertexClusters[atomIndex];
	}

	/// Looks up the index of the atom that corresponds to the given tessellation vertex index.
	/// Returns -1 if the vertex does not correspond to a physical atom.
	int vertexToAtom(int vertexIndex) const {
		if(hasVertexToAtomMapping()) {
			CALIB_ASSERT(vertexIndex >= 0 && vertexIndex < (int)_vertexToAtomMapping.size());
			return _vertexToAtomMapping[vertexIndex];
		}
		else return vertexIndex;
	}

	/// Looks up the index of the atom that corresponds to the given tessellation vertex.
	/// Returns -1 if the vertex does not correspond to a physical atom.
	int vertexToAtom(DelaunayTessellation::VertexHandle vertex) const {
		return vertexToAtom(vertex->info());
	}

	enum DomainOutcodes {
		X_POSITIVE = (1<<0), X_NEGATIVE = (1<<1),
		Y_POSITIVE = (1<<2), Y_NEGATIVE = (1<<3),
		Z_POSITIVE = (1<<4), Z_NEGATIVE = (1<<5)
	};

	/// Determines the overlap of a tetrahedron with the domains of neighboring processors.
	std::pair<int,int> computeCellOutcodes(DelaunayTessellation::CellHandle cell) const;

	/// After synchronizing the elastic mapping across processors,
	/// this method performs an additional comparison to check whether
	/// the data on all processors is consistent.
	void performConsistencyCheck();

private:

	/// The underlying tessellation of the atomistic system.
	const DelaunayTessellation& _tessellation;

	/// The results of the pattern analysis.
	SuperPatternAnalysis& _patternAnalysis;

	/// The cluster graph.
	ClusterGraph& _clusterGraph;

	/// Stores the head of the linked list of edges of each tessellation vertex.
	std::vector<TessellationEdge*> _vertexEdges;

	/// Memory pool for the creation of TessellationEdge structure instances.
	MemoryPool<TessellationEdge> _edgePool;

	/// Number of tessellation edges on the local processor.
	int _edgeCount;

	/// Stores the cluster assigned to each vertex atom of the tessellation.
	std::vector<Cluster*> _vertexClusters;

	/// Maps tessellation vertex indices to atom indices.
	std::vector<int> _vertexToAtomMapping;

	friend struct EdgeReceiverComm;
};

}; // End of namespace

#endif // __CA_ELASTIC_MAPPING_H
