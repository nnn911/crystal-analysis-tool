///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#include "DiamondCoordinationPatternMatcher.h"
#include "DiamondCoordinationPattern.h"
#include "../../../atomic_structure/AtomicStructure.h"

using namespace std;

namespace CALib {

bool DiamondCoordinationPatternMatcher::initializeNeighborPermutations()
{
	CALIB_ASSERT(atomIndex() >= 0 && atomIndex() < structure().numLocalAtoms());
	CALIB_ASSERT(pattern().numNeighbors() == 16);

	// Check for sufficient number of neighbors.
	int nn = pattern().numNeighbors();
	if(neighborList().neighborCount(atomIndex()) < nn)
		return false;

	// Compute local cutoff from first and second nearest neighbors.
	CAFloat latticeConstant = 0;
	NeighborList::neighbor_iterator neigh_start = neighborList().neighbors(atomIndex()).begin();
	NeighborList::neighbor_iterator neigh = neigh_start;
	for(int ni = 0; ni < 4; ni++, ++neigh) {
		latticeConstant += (1.0/16.0) / (sqrt(3.0)/4) * sqrt(neigh->distsq);
	}
	for(int ni = 4; ni < 16; ni++, ++neigh) {
		latticeConstant += (1.0/16.0) / sqrt(0.5) * sqrt(neigh->distsq);
	}

	const CNACoordinationPattern& cnaPattern = static_cast<const CNACoordinationPattern&>(pattern());

	nneighbors = neighborList().neighborCount(atomIndex());
	if(nneighbors > secondToFirstMap.size()) {
		secondToFirstMap.resize(nneighbors);
		neighborToSecondMap.resize(nneighbors);
	}

	// Reset permutation.
	_previousIndices.assign(nn, -1);
	_atomNeighborIndices.resize(nn);
	for(int n = 0; n < 4; n++) {
		_atomNeighborIndices[n] = n;
	}

	// Find real set of 12 second nearest neighbors. They must all be neighbors of nearest neighbors.
	CAFloat cutoff1 = latticeConstant * (0.5 * (sqrt(0.5) + sqrt(3.0)/4.0));
	CAFloat cutoff1Squared = cutoff1 * cutoff1;
	NeighborList::neighbor_iterator n1 = neigh_start + 4;
	int n = 4;
	for(int i = 4; i < nneighbors; i++, ++n1) {
		secondToFirstMap[i] = -1;
		NeighborList::neighbor_iterator n2 = neigh_start;
		for(int j = 0; j < 4; j++, ++n2) {
			if((n1->delta - n2->delta).squaredNorm() < cutoff1Squared) {
				if(secondToFirstMap[i] != -1 || n == 16) return false;
				secondToFirstMap[i] = j;
				CALIB_ASSERT(n < 16);
				neighborToSecondMap[i] = n;
				_atomNeighborIndices[n] = i;
				n++;
			}
		}
	}

	if(n != 16)
		return false;

	// Compute bond bit-flag array.
	CAFloat cutoff2 = latticeConstant * (0.5 * (sqrt(0.5) + 1.0));
	CAFloat cutoff2Squared = cutoff2 * cutoff2;
	for(int ni1 = 0; ni1 < 12; ni1++) {
		_neighborArray.setNeighborBond(ni1, ni1, false);
		NeighborList::neighbor_iterator n1 = neigh_start + _atomNeighborIndices[ni1+4];
		for(int ni2 = ni1 + 1; ni2 < 12; ni2++) {
			NeighborList::neighbor_iterator n2 = neigh_start + _atomNeighborIndices[ni2+4];
			bool bonded = (n2->delta - n1->delta).squaredNorm() < cutoff2Squared;
			_neighborArray.setNeighborBond(ni1, ni2, bonded);
		}
	}

	// Calculate CNA signatures.
	CNABond cnaSignaturesSorted[CA_MAX_PATTERN_NEIGHBORS];
	for(int ni = 4; ni < nn; ni++) {
		CNACoordinationPattern::calculateCNASignature(_neighborArray, ni-4, _cnaSignatures[ni], 12);
		cnaSignaturesSorted[ni] = _cnaSignatures[ni];
	}
	sort(cnaSignaturesSorted + 4, cnaSignaturesSorted + nn);

	// Check if sorted CNA signatures are identical.
	for(int ni = 4; ni < nn; ni++) {
		if(cnaSignaturesSorted[ni] != cnaPattern.cnaNeighborsSorted[ni]) {
			return false;
		}
	}

	// Find first matching permutation.
	for(;;) {
		if(validatePermutation()) {
			return true;
		}
		bitmapSort(_atomNeighborIndices.begin() + _validUpTo + 1, _atomNeighborIndices.end(), nneighbors);
		if(!std::next_permutation(_atomNeighborIndices.begin() + 4, _atomNeighborIndices.end()))
			break;
	}

	return false;
}

bool DiamondCoordinationPatternMatcher::nextNeighborPermutation()
{
	for(;;) {
		if(!std::next_permutation(_atomNeighborIndices.begin() + 4, _atomNeighborIndices.end()))
			break;
		if(validatePermutation())
			return true;
		bitmapSort(_atomNeighborIndices.begin() + _validUpTo + 1, _atomNeighborIndices.end(), nneighbors);
	}
	return false;
}

bool DiamondCoordinationPatternMatcher::validatePermutation()
{
	// Validate interconnectivity.
	bool check = false;
	const DiamondCoordinationPattern& cnaPattern = static_cast<const DiamondCoordinationPattern&>(pattern());
	for(int ni1 = 4; ni1 < _atomNeighborIndices.size(); ni1++) {
		_validUpTo = ni1;
		int atomNeighborIndex1 = _atomNeighborIndices[ni1];
		if(atomNeighborIndex1 != _previousIndices[ni1]) check = true;
		_previousIndices[ni1] = atomNeighborIndex1;
		if(check) {
			if(_cnaSignatures[neighborToSecondMap[atomNeighborIndex1]] != cnaPattern.cnaNeighbors[ni1])
				return false;
			for(int ni2 = 4; ni2 < ni1; ni2++) {
				int atomNeighborIndex2 = _atomNeighborIndices[ni2];
				if(_neighborArray.neighborBond(neighborToSecondMap[atomNeighborIndex1]-4, neighborToSecondMap[atomNeighborIndex2]-4) != cnaPattern.neighborArray.neighborBond(ni1-4, ni2-4))
					return false;
			}
		}
	}

	for(int i = 0; i < 4; i++) {
		for(int j = 0; j < 3; j++) {
			int pn = cnaPattern.secondToFirstMap[i][j];
			int an = _atomNeighborIndices[pn];
			if(j == 0) _atomNeighborIndices[i] = secondToFirstMap[an];
			else if(_atomNeighborIndices[i] != secondToFirstMap[an]) {
				return false;
			}
		}
	}

	_validUpTo = _atomNeighborIndices.size();
	return true;
}

}; // End of namespace
