///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#ifndef __DIAMOND_COORDINATION_PATTERN_MATCHER_H
#define __DIAMOND_COORDINATION_PATTERN_MATCHER_H

#include "../CoordinationPatternMatcher.h"
#include "../cna/CNACoordinationPattern.h"

namespace CALib {

class DiamondCoordinationPatternMatcher : public CoordinationPatternMatcher
{
public:

	/// Constructor.
	DiamondCoordinationPatternMatcher(const AtomicStructure& structure, const NeighborList& neighborList, const CoordinationPattern& pattern) :
		CoordinationPatternMatcher(structure, neighborList, pattern) {}

protected:

	virtual bool initializeNeighborPermutations();
	virtual bool nextNeighborPermutation();

	bool validatePermutation();

protected:

	/// Two-dimensional bit array that stores the bonds between neighbors.
	NeighborBondArray _neighborArray;

	/// The CNA signatures for the neighbors of the central atom.
	CNABond _cnaSignatures[CA_MAX_PATTERN_NEIGHBORS];

	std::vector<int> secondToFirstMap;
	std::vector<int> neighborToSecondMap;
	int nneighbors;
};

}; // End of namespace

#endif // __DIAMOND_COORDINATION_PATTERN_MATCHER_H

