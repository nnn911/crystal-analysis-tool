///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#ifndef __NDA_COORDINATION_PATTERN_MATCHER_H
#define __NDA_COORDINATION_PATTERN_MATCHER_H

#include "../CoordinationPatternMatcher.h"

namespace CALib {

class NDACoordinationPatternMatcher : public CoordinationPatternMatcher
{
public:

	/// Constructor.
	NDACoordinationPatternMatcher(const AtomicStructure& structure, const NeighborList& neighborList, const CoordinationPattern& pattern) :
		CoordinationPatternMatcher(structure, neighborList, pattern) {}

private:

	virtual bool initializeNeighborPermutations();
	virtual bool nextNeighborPermutation();
	bool validatePermutation();

private:

	CAFloat bondLengthsSquared[CA_MAX_PATTERN_NEIGHBORS][CA_MAX_PATTERN_NEIGHBORS];
};

}; // End of namespace

#endif // __NDA_COORDINATION_PATTERN_MATCHER_H

