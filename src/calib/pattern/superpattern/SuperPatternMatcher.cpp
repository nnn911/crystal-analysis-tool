///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#include "SuperPatternMatcher.h"
#include "../catalog/PatternCatalog.h"
#include "../../atomic_structure/AtomicStructure.h"
#include "../../context/CAContext.h"

using namespace std;

namespace CALib {

/******************************************************************************
* Constructor
******************************************************************************/
SuperPatternMatcher::SuperPatternMatcher(const CoordinationPatternAnalysis& coordAnalysis, const PatternCatalog& catalog, const SuperPattern& pattern, AtomMatchFilter* filter) :
	_coordAnalysis(coordAnalysis), _structure(coordAnalysis.structure()), _neighborList(coordAnalysis.neighborList()), _catalog(catalog), _pattern(pattern), _filter(filter), _foundDisclination(false)
{
	_seedAtomIndex = 0;
	_seedNodeIndex = 0;
	_seedPermutationIndex = 0;

	// Determine the seed node of the super pattern.
	// For performance reasons we use a node whose coordination pattern occurs the
	// least in the atomic structure.
	if(pattern.numCoreNodes > 1) {
		vector<int> coordPatternCounts(catalog.coordinationPatterns().size() + 1, 0);
		for(int atomIndex = 0; atomIndex < structure().numLocalAtoms(); atomIndex++) {
			BOOST_FOREACH(const CoordinationPatternAnalysis::coordmatch& match, coordAnalysis.coordinationPatternMatches(atomIndex)) {
				CALIB_ASSERT(match.pattern->index >= 1 && match.pattern->index < coordPatternCounts.size());
				coordPatternCounts[match.pattern->index]++;
			}
		}
		int minimumCount = structure().numLocalAtoms();
		for(int patternNodeIndex = 0; patternNodeIndex < pattern.numCoreNodes; patternNodeIndex++) {
			if(coordPatternCounts[pattern.nodes[patternNodeIndex].coordinationPattern->index] <= minimumCount) {
				minimumCount = coordPatternCounts[pattern.nodes[patternNodeIndex].coordinationPattern->index];
				_seedNodeIndex = patternNodeIndex;
			}
		}
	}

	_seedNode = &pattern.nodes[_seedNodeIndex];

	// Initialize map.
	_matchedAtoms.resize(structure().numLocalAtoms() + structure().numGhostAtoms(), NULL);
}

/******************************************************************************
* Finds the next occurrence of the super pattern in the input structure.
*
* Returns false if no more matches were found.
******************************************************************************/
bool SuperPatternMatcher::findNextCluster()
{
	int atomCount = structure().numLocalAtoms() + structure().numGhostAtoms();
	for(; _seedAtomIndex < atomCount; _seedAtomIndex++) {

		// Is this an acceptable atom for a super-pattern match?
		if(acceptAtom(_seedNodeIndex, _seedAtomIndex)) {

			// Check atomic species of seed atom. It must match the pattern if type matching is enabled for this pattern.
			if(_seedNode->species == 0 || structure().atomSpecies(_seedAtomIndex) == _seedNode->species) {

				boost::optional<CoordinationPatternAnalysis::coordmatch_iterator> match = coordAnalysis().findCoordinationPatternMatch(_seedAtomIndex, *_seedNode->coordinationPattern);
				if(match) {
					// Test all symmetry permutations of the neighbors.
					for(; _seedPermutationIndex < _seedNode->coordinationPattern->permutations.size(); ++_seedPermutationIndex) {
						if(checkPermutation(_seedAtomIndex, _seedNodeIndex, _seedPermutationIndex)) {
							// A match has been found. Next time, continue with the next symmetry permutation.
							_seedPermutationIndex++;
							return true;
						}
					}
				}
			}
		}

		// Reset counter for next atom.
		_seedPermutationIndex = 0;
	}
	return false; // End of search
}

/******************************************************************************
* Tries to match the pattern starting at the given atom.
******************************************************************************/
bool SuperPatternMatcher::checkPermutation(int seedAtomIndex, int seedNodeIndex, int seedPermutationIndex)
{
	const SuperPatternNode& seedNode = pattern().nodes[seedNodeIndex];
	boost::optional<CoordinationPatternAnalysis::coordmatch_iterator> seedCoordinationMapping =
			coordAnalysis().findCoordinationPatternMatch(seedAtomIndex, *seedNode.coordinationPattern);
	if(!seedCoordinationMapping) return false;

	// Skip ghost atoms without a neighbor list (which are at the outermost border of the ghost region).
	if(_neighborList.neighborCount(seedAtomIndex) == 0)
		return false;

	CALIB_ASSERT(seedNode.species == 0 || structure().atomSpecies(seedAtomIndex) == seedNode.species);
	CALIB_ASSERT(acceptAtom(seedNodeIndex, seedAtomIndex));
	CALIB_ASSERT(seedNodeIndex < pattern().numCoreNodes);

	// First, cleanup records from last search pass.
	BOOST_FOREACH(const MatchedAtom& atom, _visitedAtoms) {
		CALIB_ASSERT(_matchedAtoms[atom.atomIndex] == &atom);
		_matchedAtoms[atom.atomIndex] = NULL;
	}
	_visitedAtoms.clear();
	_toBeChecked.clear();
	_atomPool.clear(true);

	MatchedAtom* atom = _atomPool.construct();
	atom->atomIndex = seedAtomIndex;
	atom->nodeIndex = seedNodeIndex;
	atom->coordination = &**seedCoordinationMapping;
	atom->permutationIndex = seedPermutationIndex;
	atom->flags = 0;
	atom->predecessor = NULL;
	_matchedAtoms[seedAtomIndex] = atom;
	_visitedAtoms.push_front(*atom);

	// Reset disclination flag.
	_foundDisclination = false;

	// Check if all nodes of the super pattern appear in the vicinity of the seed atom.
	if(!coversPatternNodes(*atom)) {
		return false;
	}

	// Mark the seed atom as a matched atom.
	atom->flags.set(MatchedAtom::MATCHES);

	// Process all remaining atoms on the search stack until it becomes empty.
	while(_toBeChecked.empty() == false) {
		atom = &_toBeChecked.front();
		_toBeChecked.pop_front();
		if(atom->flags.test(MatchedAtom::DISCLINATION_BARRIER)) continue;
		if(coversPatternNodes(*atom)) {
			CALIB_ASSERT(atom->nodeIndex >= 0 && atom->nodeIndex < pattern().numNodes);
			if(acceptAtom(atom->nodeIndex, atom->atomIndex)) {
				atom->flags.set(MatchedAtom::MATCHES);
			}
		}
	}

	// Mark all non-core atoms as matches too, which have a matched core atom as a neighbor.
	if(pattern().isLattice() == false) {
		BOOST_FOREACH(MatchedAtom& atom, _visitedAtoms) {
			if(atom.nodeIndex == -1) continue;
			if(!isCoreNodeAtom(atom)) {
				for(int i = 0; i < atom.numNeighbors(); i++) {
					MatchedAtom* neighbor = lookupNeighbor(atom, i);
					if(neighbor != NULL && neighbor->isValidMatch() && isCoreNodeAtom(*neighbor)) {
						atom.flags.set(MatchedAtom::MATCHES);
						break;
					}
				}
			}
			else if(atom.isValidMatch()) {
				for(int i = 0; i < atom.numNeighbors(); i++) {
					MatchedAtom* neighbor = lookupNeighbor(atom, i);
					if(neighbor != NULL && neighbor->nodeIndex != -1 && isCoreNodeAtom(*neighbor) == false) {
						neighbor->flags.set(MatchedAtom::MATCHES);
					}
				}
			}
		}
	}

#if 0
#ifdef DEBUG_CRYSTAL_ANALYSIS
	// Make sure that the number of matching atoms is not less than the number of pattern nodes.
	int matchCount = 0;
	for(iterator iter = begin(); iter != end(); ++iter)
		matchCount++;
	if(matchCount < pattern.numCoreNodes) {
		for(MatchedAtom* current = visitedAtoms; current != NULL; current = current->nextVisited) {
			structure.getContext().MsgLogger() << "  Atom " << current->inputAtom->tag << " matches=" << current->testFlag(MatchedAtom::MATCHES) << "  discl=" << current->testFlag(MatchedAtom::DISCLINATION_BARRIER) << endl;
		}
		structure.getContext().MsgLogger() << "Pattern " << pattern.name << "  Match count=" << matchCount << endl;
		CALIB_ASSERT(false);
	}
#endif
#endif

	return true;	// We discovered a cluster that matches to the super pattern.
}

/******************************************************************************
* Checks if all nodes of the super pattern appear in the vicinity of the
* given base atom.
******************************************************************************/
bool SuperPatternMatcher::coversPatternNodesMultiple(MatchedAtom& baseAtom)
{
	// Initialize the array that stores which nodes appear in the vicinity of the first atom.
	_coveredNodes.assign(pattern().numNodes, false);

	CALIB_ASSERT(isCoreNodeAtom(baseAtom));
	CALIB_ASSERT(baseAtom.flags.test(MatchedAtom::DISCLINATION_BARRIER) == false);

	// The node mapped to the base atom can already be marked as found.
	_coveredNodes[baseAtom.nodeIndex] = true;
	const SuperPatternNode& baseNode = pattern().nodes[baseAtom.nodeIndex];
	int numNodesCovered = 1;
	int numCoreNodesCovered = 1;
	bool isValidCoverage = true;

	baseAtom.nextInQueue = NULL;
	baseAtom.flags.set(MatchedAtom::IS_IN_QUEUE);
	MatchedAtom* queue_head = &baseAtom;
	MatchedAtom* queue_end = NULL;

	for(int round = 0; round < pattern().numNodes; round++) {

		MatchedAtom* old_queue_head = queue_head;
		for(MatchedAtom* currentAtom = queue_head; currentAtom != queue_end; currentAtom = currentAtom->nextInQueue) {
			if(isCoreNodeAtom(*currentAtom) == false)
				continue;
			if(currentAtom->flags.test(MatchedAtom::DISCLINATION_BARRIER))
				continue;

			const SuperPatternNode& currentNode = pattern().nodes[currentAtom->nodeIndex];
			const CoordinationPattern& currentCoordPattern = *currentNode.coordinationPattern;
			int currentAtomIndex = currentAtom->atomIndex;

			CALIB_ASSERT(currentNode.coordinationPattern == currentAtom->coordination->pattern);
			CALIB_ASSERT(currentNode.species == 0 || structure().atomSpecies(currentAtomIndex) == currentNode.species);

			for(int superPatternNeighborIndex = 0; superPatternNeighborIndex < currentCoordPattern.numNeighbors(); superPatternNeighborIndex++) {
				int neighborNodeIndex = currentNode.neighbors[superPatternNeighborIndex].neighborNodeIndex;
				const SuperPatternNode& neighborNode = pattern().nodes[neighborNodeIndex];
				int atomNeighborIndex = currentAtom->nodeNeighborToAtomNeighbor(superPatternNeighborIndex);
				int neighborInputAtomIndex = _neighborList.neighbor(currentAtom->atomIndex, atomNeighborIndex);

				// Skip ghost atoms without a neighbor list (which are at the outermost border of the ghost region).
				if(_neighborList.neighborCount(neighborInputAtomIndex) == 0) continue;

				MatchedAtom* neighborAtom = _matchedAtoms[neighborInputAtomIndex];

				if(neighborAtom == NULL) {

					// Is this an acceptable atom for a super pattern match?
					if(!acceptAtom(neighborNodeIndex, neighborInputAtomIndex)) {
						continue;
					}

					// Check atomic species of the atom. It must match the pattern if species checking is enabled for this pattern.
					if(neighborNode.species != 0 && structure().atomSpecies(neighborInputAtomIndex) != neighborNode.species) {
						continue;
					}

					neighborAtom = _atomPool.construct();
					neighborAtom->atomIndex = neighborInputAtomIndex;
					neighborAtom->nodeIndex = -1;
					neighborAtom->permutationIndex = -1;
					neighborAtom->predecessor = currentAtom;
					neighborAtom->coordination = NULL;
					_matchedAtoms[neighborInputAtomIndex] = neighborAtom;
					_visitedAtoms.push_front(*neighborAtom);
				}
				else {
					if(neighborAtom->flags.test(MatchedAtom::IS_IN_QUEUE) || neighborAtom->flags.test(MatchedAtom::DISCLINATION_BARRIER))
						continue;
					if(neighborAtom->nodeIndex != -1 && neighborAtom->nodeIndex != neighborNodeIndex)
						continue;
					CALIB_ASSERT(neighborAtom->atomIndex == neighborInputAtomIndex);
				}

				const CoordinationPattern& neighborCoordPattern = *neighborNode.coordinationPattern;

				boost::optional<CoordinationPatternAnalysis::coordmatch_iterator> neighborCoordinationMapping =
						coordAnalysis().findCoordinationPatternMatch(neighborInputAtomIndex, neighborCoordPattern);
				if(!neighborCoordinationMapping) {
					continue;
				}
				CALIB_ASSERT(&neighborCoordPattern == neighborCoordinationMapping.get()->pattern);
				CALIB_ASSERT(neighborCoordPattern.numNeighbors() <= _neighborList.neighborCount(neighborInputAtomIndex));
				CALIB_ASSERT(currentAtom->flags.test(MatchedAtom::DISCLINATION_BARRIER) == false);

				if(neighborAtom->coordination == NULL) {
					bool foundValidPermutation = false;
					neighborAtom->nodeIndex = neighborNodeIndex;
					CAFloat minDeviation = CAFLOAT_MAX;
					for(vector<CoordinationPermutation>::const_iterator permutation = neighborCoordPattern.permutations.begin(); permutation != neighborCoordPattern.permutations.end(); ++permutation) {
						CAFloat deviation;
						if(isValidTransitionPermutation(currentAtom, neighborAtom, superPatternNeighborIndex, atomNeighborIndex, **neighborCoordinationMapping, *permutation, deviation)) {
							foundValidPermutation = true;
							//if(deviation < minDeviation) {
								neighborAtom->coordination = &**neighborCoordinationMapping;
								neighborAtom->permutationIndex = (permutation - neighborCoordPattern.permutations.begin());
								minDeviation = deviation;
							//}
							//if(deviation == 0)
								break;
						}
					}
					if(!foundValidPermutation) {
						neighborAtom->nodeIndex = -1;
						neighborAtom->coordination = NULL;
						continue;
					}
					if(isCoreNodeAtom(*neighborAtom)) {
						_toBeChecked.push_front(*neighborAtom);
					}
				}
				else {
					// If this neighbor has been visited before, then
					// check if its old orientation is in agreement with the new orientation.
					// If not, we have detected a disclination.
					CALIB_ASSERT(neighborAtom->coordination == &**neighborCoordinationMapping);
					CAFloat deviation;
					if(!isValidTransitionPermutation(currentAtom, neighborAtom, superPatternNeighborIndex, atomNeighborIndex, **neighborCoordinationMapping, neighborCoordPattern.permutations[neighborAtom->permutationIndex], deviation)) {
						currentAtom->flags.set(MatchedAtom::DISCLINATION_BARRIER);
						_foundDisclination = true;
						break;
					}
				}

				// The neighbor atom is valid and can be added to the local queue.
				if(round < pattern().numNodes - 1) {
					neighborAtom->flags.set(MatchedAtom::IS_IN_QUEUE);
					neighborAtom->nextInQueue = queue_head;
					queue_head = neighborAtom;
				}

				// Mark the corresponding node in the super pattern as found.
				if(_coveredNodes[neighborNodeIndex] == false) {
					_coveredNodes[neighborNodeIndex] = true;

					numNodesCovered++;
					if(isCoreNodeAtom(*neighborAtom))
						numCoreNodesCovered++;
				}
			}
		}

		if(numNodesCovered == pattern().numNodes)
			break;

		switch(pattern().vicinityCriterion) {
		case SuperPattern::VICINITY_CRITERION_LATTICE:
			if(numNodesCovered <= round)
				isValidCoverage = false;
			break;
		case SuperPattern::VICINITY_CRITERION_INTERFACE:
			if(numNodesCovered <= round || (numCoreNodesCovered < pattern().numCoreNodes && round >= pattern().numCoreNodes)) {
				isValidCoverage = false;
			}
			break;
		case SuperPattern::VICINITY_CRITERION_POINTDEFECT:
			if(numCoreNodesCovered <= round && round < pattern().numCoreNodes)
				isValidCoverage = false;
			break;
		case SuperPattern::VICINITY_CRITERION_SHORTESTPATH:
			for(int n = 0; n < pattern().numNodes; n++) {
				if(baseNode.nodeNodeDistances[n] <= round+1 && _coveredNodes[n] == false) {
					isValidCoverage = false;
					break;
				}
			}
			break;
		default:
			CALIB_ASSERT(false);
			break;
		}

		if(isValidCoverage == false)
			break;

		queue_end = old_queue_head;
	}

	// Clear flags.
	for(MatchedAtom* atom = queue_head; atom != NULL; atom = atom->nextInQueue)
		atom->flags.reset(MatchedAtom::IS_IN_QUEUE);

	return isValidCoverage;
}

/******************************************************************************
* Optimized version of coversPatternNodesMultiple() for super patterns with a single node.
******************************************************************************/
bool SuperPatternMatcher::coversPatternNodesSingle(MatchedAtom& baseAtom)
{
	CALIB_ASSERT(isCoreNodeAtom(baseAtom));
	CALIB_ASSERT(baseAtom.flags.test(MatchedAtom::DISCLINATION_BARRIER) == false);
	CALIB_ASSERT(baseAtom.coordination != NULL);

	const SuperPatternNode& node = pattern().nodes[baseAtom.nodeIndex];
	const CoordinationPattern& coordPattern = *node.coordinationPattern;
	MatchedAtom* currentAtom = &baseAtom;

	CALIB_ASSERT(node.coordinationPattern == currentAtom->coordination->pattern);
	CALIB_ASSERT(node.species == 0 || structure().atomSpecies(currentAtom->atomIndex) == node.species);

	for(int superPatternNeighborIndex = 0; superPatternNeighborIndex < coordPattern.numNeighbors(); superPatternNeighborIndex++) {

		int neighborNodeIndex = node.neighbors[superPatternNeighborIndex].neighborNodeIndex;
		const SuperPatternNode& neighborNode = pattern().nodes[neighborNodeIndex];

		int atomNeighborIndex = currentAtom->nodeNeighborToAtomNeighbor(superPatternNeighborIndex);
		int neighborInputAtomIndex = _neighborList.neighbor(currentAtom->atomIndex, atomNeighborIndex);

		// Skip ghost atoms without a neighbor list (which are at the outermost border of the ghost region).
		if(_neighborList.neighborCount(neighborInputAtomIndex) == 0) continue;

		MatchedAtom* neighborAtom = _matchedAtoms[neighborInputAtomIndex];

		if(neighborAtom == NULL) {

			// Is this an acceptable atom for a super-pattern match?
			if(!acceptAtom(neighborNodeIndex, neighborInputAtomIndex))
				continue;

			// Check atomic species of the atom. It must match the pattern if species checking is enabled for this pattern.
			if(node.species != 0 && structure().atomSpecies(neighborInputAtomIndex) != node.species)
				continue;

			neighborAtom = _atomPool.construct();
			neighborAtom->atomIndex = neighborInputAtomIndex;
			neighborAtom->nodeIndex = -1;
			neighborAtom->coordination = NULL;
			neighborAtom->permutationIndex = -1;
			neighborAtom->predecessor = currentAtom;
			_matchedAtoms[neighborInputAtomIndex] = neighborAtom;
			_visitedAtoms.push_front(*neighborAtom);
		}
		else {
			if(neighborAtom->flags.test(MatchedAtom::IS_IN_QUEUE) || neighborAtom->flags.test(MatchedAtom::DISCLINATION_BARRIER))
				continue;
			if(neighborAtom->nodeIndex != -1 && neighborAtom->nodeIndex != neighborNodeIndex)
				continue;
		}
		CALIB_ASSERT(neighborAtom->atomIndex == neighborInputAtomIndex);
		CALIB_ASSERT(currentAtom->flags.test(MatchedAtom::DISCLINATION_BARRIER) == false);

		if(neighborAtom->coordination == NULL) {

			boost::optional<CoordinationPatternAnalysis::coordmatch_iterator> neighborCoordinationMapping =
					coordAnalysis().findCoordinationPatternMatch(neighborInputAtomIndex, coordPattern);
			if(!neighborCoordinationMapping) continue;
			CALIB_ASSERT(&coordPattern == neighborCoordinationMapping.get()->pattern);
			CALIB_ASSERT(_neighborList.neighborCount(neighborInputAtomIndex) >= coordPattern.numNeighbors());

			neighborAtom->nodeIndex = neighborNodeIndex;
			neighborAtom->permutationIndex = 0;
			for(vector<CoordinationPermutation>::const_iterator permutation = coordPattern.permutations.begin(); permutation != coordPattern.permutations.end(); ++permutation, neighborAtom->permutationIndex++) {
				CAFloat deviation;
				if(isValidTransitionPermutation(currentAtom, neighborAtom, superPatternNeighborIndex, atomNeighborIndex, **neighborCoordinationMapping, *permutation, deviation)) {
					neighborAtom->coordination = &**neighborCoordinationMapping;
					break;
				}
			}
			if(!neighborAtom->coordination) {
				neighborAtom->nodeIndex = -1;
				neighborAtom->permutationIndex = -1;
				continue;
			}

			_toBeChecked.push_front(*neighborAtom);
		}
		else {
			// If this neighbor has been visited before, then
			// check if its old orientation is in agreement with the new orientation.
			// If not, we have detected a disclination.
			CALIB_ASSERT(neighborAtom->coordination == &**coordAnalysis().findCoordinationPatternMatch(neighborInputAtomIndex, coordPattern));
			bool isClose = (neighborAtom->predecessor == currentAtom) || (neighborAtom->predecessor == currentAtom->predecessor) || (neighborAtom == currentAtom->predecessor);
			if(!isClose) {
				CAFloat deviation;
				if(!isValidTransitionPermutation(currentAtom, neighborAtom, superPatternNeighborIndex, atomNeighborIndex, *neighborAtom->coordination, coordPattern.permutations[neighborAtom->permutationIndex], deviation)) {
					currentAtom->flags.set(MatchedAtom::DISCLINATION_BARRIER);
					_foundDisclination = true;
					break;
				}
			}
		}
	}

	return true;
}


bool SuperPatternMatcher::isValidTransitionPermutation(
		MatchedAtom* atomA,
		MatchedAtom* atomB,
		int superPatternNeighborIndexA,
		int atomNeighborIndexA,
		const CoordinationPatternAnalysis::coordmatch& coordMatchB,
		const CoordinationPermutation& permutationB,
		CAFloat& deviation)
{
	CALIB_ASSERT(isCoreNodeAtom(*atomA));
	CALIB_ASSERT(atomB->nodeIndex >= 0 && atomB->nodeIndex < pattern().numNodes);
	const SuperPatternNode& nodeA = pattern().nodes[atomA->nodeIndex];
	const SuperPatternNode& nodeB = pattern().nodes[atomB->nodeIndex];
	const CoordinationPattern& coordPatternA = *atomA->coordination->pattern;
	const CoordinationPattern& coordPatternB = *coordMatchB.pattern;
	CALIB_ASSERT(atomA->flags.test(MatchedAtom::DISCLINATION_BARRIER) == false);
	CALIB_ASSERT(atomB->flags.test(MatchedAtom::DISCLINATION_BARRIER) == false);
	CALIB_ASSERT(coordPatternA.numNeighbors() <= _neighborList.neighborCount(atomA->atomIndex));
	CALIB_ASSERT(coordPatternB.numNeighbors() <= _neighborList.neighborCount(atomB->atomIndex));
	CALIB_ASSERT(permutationB.size() == coordPatternB.numNeighbors());
	deviation = 0;

	// Check if bond A->B is consistent with bond B->A.
	{
		int superPatternNeighborIndexB = nodeA.neighbors[superPatternNeighborIndexA].reverseNodeNeighborIndex;
		CALIB_ASSERT(superPatternNeighborIndexB >= 0 && superPatternNeighborIndexB < coordPatternB.numNeighbors());
		int coordPatternNeighborIndexB = permutationB[superPatternNeighborIndexB];
		CALIB_ASSERT(coordPatternNeighborIndexB >= 0 && coordPatternNeighborIndexB < coordPatternB.numNeighbors());
		int atomNeighborIndexB = coordMatchB.mapping[coordPatternNeighborIndexB];
		CALIB_ASSERT(atomNeighborIndexB >= 0 && atomNeighborIndexB < _neighborList.neighborCount(atomB->atomIndex));
		int neighborAtomB = _neighborList.neighbor(atomB->atomIndex, atomNeighborIndexB);

		if(neighborAtomB != atomA->atomIndex)
			return false;

		// This additional check needs to be performed only when using minimum image convention (and not ghost atoms).
		if(structure().neighborMode() != AtomicStructure::GHOST_ATOMS) {
			if(!(_neighborList.neighborVector(atomA->atomIndex, atomNeighborIndexA) +
					_neighborList.neighborVector(atomB->atomIndex, atomNeighborIndexB)).isZero(CA_ATOM_VECTOR_EPSILON)) {
				return false;
			}
		}

		CALIB_ASSERT((nodeA.neighbors[superPatternNeighborIndexA].referenceVector + nodeB.neighbors[superPatternNeighborIndexB].referenceVector).isZero(CA_LATTICE_VECTOR_EPSILON));
		CALIB_ASSERT(nodeB.neighbors[superPatternNeighborIndexB].neighborNodeIndex == atomA->nodeIndex);
		CALIB_ASSERT(nodeA.neighbors[superPatternNeighborIndexA].neighborNodeIndex == atomB->nodeIndex);
	}

	// Check for correct orientation around bond axis by looking at the common neighbors
	// of the bond.
	int numberOfCommonNeighbors = nodeA.neighbors[superPatternNeighborIndexA].numCommonNeighbors;
	CALIB_ASSERT(numberOfCommonNeighbors >= 1);
	for(int commonNeighbor = 0; commonNeighbor < numberOfCommonNeighbors; commonNeighbor++) {

		int superPatternNeighborIndexC = nodeA.neighbors[superPatternNeighborIndexA].commonNeighborNodeIndices[commonNeighbor][0];
		CALIB_ASSERT(superPatternNeighborIndexC >= 0 && superPatternNeighborIndexC < coordPatternA.numNeighbors());
		int atomNeighborIndexC = atomA->nodeNeighborToAtomNeighbor(superPatternNeighborIndexC);
		CALIB_ASSERT(atomNeighborIndexC >= 0 && atomNeighborIndexC < _neighborList.neighborCount(atomA->atomIndex));
		int neighborAtomC = _neighborList.neighbor(atomA->atomIndex, atomNeighborIndexC);

		int superPatternNeighborIndexB = nodeA.neighbors[superPatternNeighborIndexA].commonNeighborNodeIndices[commonNeighbor][1];
		CALIB_ASSERT(superPatternNeighborIndexB >= 0 && superPatternNeighborIndexB < coordPatternB.numNeighbors());
		int coordPatternNeighborIndexB = permutationB[superPatternNeighborIndexB];
		CALIB_ASSERT(coordPatternNeighborIndexB >= 0 && coordPatternNeighborIndexB < coordPatternB.numNeighbors());
		int atomNeighborIndexB = coordMatchB.mapping[coordPatternNeighborIndexB];
		CALIB_ASSERT(atomNeighborIndexB >= 0 && atomNeighborIndexB < _neighborList.neighborCount(atomB->atomIndex));
		int neighborAtomB = _neighborList.neighbor(atomB->atomIndex, atomNeighborIndexB);

		if(neighborAtomB != neighborAtomC)
			return false;

		// This additional needs to be performed only when using minimum image convention (and not ghost atoms).
		if(structure().neighborMode() != AtomicStructure::GHOST_ATOMS) {
			if(!(_neighborList.neighborVector(atomA->atomIndex, atomNeighborIndexC) - _neighborList.neighborVector(atomA->atomIndex, atomNeighborIndexA) - _neighborList.neighborVector(atomB->atomIndex, atomNeighborIndexB)).isZero(CA_ATOM_VECTOR_EPSILON))
				return false;
		}

		CALIB_ASSERT((nodeA.neighbors[superPatternNeighborIndexC].referenceVector - nodeA.neighbors[superPatternNeighborIndexA].referenceVector - nodeB.neighbors[superPatternNeighborIndexB].referenceVector).isZero(CA_LATTICE_VECTOR_EPSILON));
	}

	return true;
}

/******************************************************************************
* After a cluster of atoms matching the pattern has been found,
* this methods calculates an average orientation matrix that transforms
* vectors from the ideal reference frame of the cluster to the global simulation frame.
******************************************************************************/
Matrix3 SuperPatternMatcher::calculateOrientation() const
{
	FrameTransformation tm;
	size_t count = 0;
	BOOST_FOREACH(const MatchedAtom& atom, matchingAtoms()) {
		CALIB_ASSERT(atom.nodeIndex >= 0 && atom.nodeIndex < pattern().numNodes);
		const SuperPatternNode& node = pattern().nodes[atom.nodeIndex];

		for(int nodeNeighborIndex = 0; nodeNeighborIndex < atom.numNeighbors(); nodeNeighborIndex++) {
			int atomNeighborIndex = atom.nodeNeighborToAtomNeighbor(nodeNeighborIndex);

			tm.addVector(
					node.neighbors[nodeNeighborIndex].referenceVector,
					_neighborList.neighborVector(atom.atomIndex, atomNeighborIndex));
			count++;
		}
	}
	if(count < 3)
		return Matrix3::Identity();
	return tm.computeAverageTransformation();
}

}; // End of namespace
