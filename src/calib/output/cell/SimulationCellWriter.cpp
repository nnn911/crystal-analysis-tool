///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#include "SimulationCellWriter.h"
#include "../../atomic_structure/SimulationCell.h"
#include "../../context/CAContext.h"

using namespace std;

namespace CALib {

/******************************************************************************
* Writes the cell geometry to the given file stream in VTK format.
******************************************************************************/
void SimulationCellWriter::writeVTKFile(std::ostream& stream, const SimulationCell& cell)
{
	if(!parallel().isMaster())
		return;

	Vector3 corners[8];
	corners[0].noalias() = cell.simulationCellMatrix() * Vector3(0,0,0);
	corners[1].noalias() = cell.simulationCellMatrix() * Vector3(1,0,0);
	corners[2].noalias() = cell.simulationCellMatrix() * Vector3(1,1,0);
	corners[3].noalias() = cell.simulationCellMatrix() * Vector3(0,1,0);
	corners[4].noalias() = cell.simulationCellMatrix() * Vector3(0,0,1);
	corners[5].noalias() = cell.simulationCellMatrix() * Vector3(1,0,1);
	corners[6].noalias() = cell.simulationCellMatrix() * Vector3(1,1,1);
	corners[7].noalias() = cell.simulationCellMatrix() * Vector3(0,1,1);

	stream << "# vtk DataFile Version 3.0" << endl;
	stream << "# Simulation cell (CALib version " << CA_LIB_VERSION_STRING << ")" << endl;
	stream << "ASCII" << endl;
	stream << "DATASET UNSTRUCTURED_GRID" << endl;
	stream << "POINTS 8 float" << endl;
	for(int i = 0; i < 8; i++)
		stream << corners[i].x() << " " << corners[i].y() << " " << corners[i].z() << endl;

	stream << endl << "CELLS 1 9" << endl;
	stream << "8 0 1 2 3 4 5 6 7" << endl;

	stream << endl << "CELL_TYPES 1" << endl;
	stream << "12" << endl;  // Hexahedron
}

}; // End of namespace
