///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#include "ClusterGraphWriter.h"
#include "../../cluster_graph/Cluster.h"
#include "../../cluster_graph/ClusterGraph.h"

using namespace std;

namespace CALib {

/******************************************************************************
* Outputs the cluster graph to a VTK file.
******************************************************************************/
void ClusterGraphWriter::writeVTKFile(std::ostream& stream, const ClusterGraph& graph)
{
	if(!parallel().isMaster())
		return;

	stream << "# vtk DataFile Version 3.0" << endl;
	stream << "# Clusters (CALib version " << CA_LIB_VERSION_STRING << ")" << endl;
	stream << "ASCII" << endl;
	stream << "DATASET UNSTRUCTURED_GRID" << endl;
	stream << "POINTS " << graph.clusters().size() << " double" << endl;

	BOOST_FOREACH(Cluster* cluster, graph.clusters())
		stream << cluster->centerOfMass.x() << " " << cluster->centerOfMass.y() << " " << cluster->centerOfMass.z() << endl;

	stream << endl << "CELLS " << graph.clusters().size() << " " << (graph.clusters().size()*2) << endl;
	for(int i = 0; i < graph.clusters().size(); i++)
		stream << "1 " << i << endl;
	stream << endl << "CELL_TYPES " << graph.clusters().size() << endl;
	for(int i = 0; i < graph.clusters().size(); i++)
		stream << "1" << endl;	// Vertex

	stream << endl << "CELL_DATA " << graph.clusters().size() << endl;

	stream << endl << "SCALARS localid int" << endl;
	stream << endl << "LOOKUP_TABLE default" << endl;
	BOOST_FOREACH(Cluster* cluster, graph.clusters())
		stream << cluster->id.id << endl;

	stream << endl << "SCALARS processor int" << endl;
	stream << endl << "LOOKUP_TABLE default" << endl;
	BOOST_FOREACH(Cluster* cluster, graph.clusters())
		stream << cluster->id.processor << endl;

	stream << endl << "SCALARS numatoms int" << endl;
	stream << endl << "LOOKUP_TABLE default" << endl;
	BOOST_FOREACH(Cluster* cluster, graph.clusters())
		stream << cluster->atomCount << endl;

	stream << endl << "SCALARS superpattern int" << endl;
	stream << endl << "LOOKUP_TABLE default" << endl;
	BOOST_FOREACH(Cluster* cluster, graph.clusters())
		stream << cluster->pattern->index << endl;

	stream << endl << "VECTORS orientation_x double" << endl;
	BOOST_FOREACH(Cluster* cluster, graph.clusters())
		stream << cluster->orientation.col(0).x() << " " << cluster->orientation.col(0).y() << " " << cluster->orientation.col(0).z() << endl;

	stream << endl << "VECTORS orientation_y double" << endl;
	BOOST_FOREACH(Cluster* cluster, graph.clusters())
		stream << cluster->orientation.col(1).x() << " " << cluster->orientation.col(1).y() << " " << cluster->orientation.col(1).z() << endl;

	stream << endl << "VECTORS orientation_z double" << endl;
	BOOST_FOREACH(Cluster* cluster, graph.clusters())
		stream << cluster->orientation.col(2).x() << " " << cluster->orientation.col(2).y() << " " << cluster->orientation.col(2).z() << endl;

	stream << endl << "VECTORS normalized_orientation_x double" << endl;
	BOOST_FOREACH(Cluster* cluster, graph.clusters()) {
		Vector3 v = cluster->orientation.col(0).normalized();
		stream << v.x() << " " << v.y() << " " << v.z() << endl;
	}

	stream << endl << "VECTORS normalized_orientation_y double" << endl;
	BOOST_FOREACH(Cluster* cluster, graph.clusters()) {
		Vector3 v = cluster->orientation.col(1).normalized();
		stream << v.x() << " " << v.y() << " " << v.z() << endl;
	}

	stream << endl << "VECTORS normalized_orientation_z double" << endl;
	BOOST_FOREACH(Cluster* cluster, graph.clusters()) {
		Vector3 v = cluster->orientation.col(2).normalized();
		stream << v.x() << " " << v.y() << " " << v.z() << endl;
	}
}

}; // End of namespace
