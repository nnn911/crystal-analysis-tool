///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#ifndef __CA_PATTERN_CATALOG_WRITER_H
#define __CA_PATTERN_CATALOG_WRITER_H

#include "../../CALib.h"
#include "../../context/CAContext.h"
#include "../../pattern/catalog/PatternCatalog.h"

namespace CALib {

/**
 * Writes a PatternCatalog object to a file.
 */
class PatternCatalogWriter : public ContextReference
{
public:

	/// Constructor.
	PatternCatalogWriter(CAContext& context) : ContextReference(context) {}

	/// Serializes a PatternCatalog object to a file.
	void write(std::ostream& stream, const PatternCatalog& catalog);

private:

	void writeCoordinationPattern(std::ostream& stream, const CoordinationPattern& pattern);
	void writeSuperPattern(std::ostream& stream, const SuperPattern& pattern);
	void writeVector(std::ostream& stream, const Vector3& v) { stream << v.x() << ' ' << v.y() << ' ' << v.z() << std::endl; }
	void writeMatrix(std::ostream& stream, const Matrix3& m) {
		for(int i = 0; i < 3; i++)
			stream << m(i,0) << ' ' << m(i,1) << ' ' << m(i,2) << std::endl;
	}
};

}; // End of namespace

#endif // __CA_PATTERN_CATALOG_WRITER_H
