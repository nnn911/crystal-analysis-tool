///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#include "DislocationsWriter.h"

using namespace std;

namespace CALib {

/******************************************************************************
* Outputs the extracted dislocation segments to a VTK file.
******************************************************************************/
void DislocationsWriter::writeVTKFile(std::ostream& stream, const DislocationNetwork& network)
{
	if(!parallel().isMaster())
		return;

	// Gather dislocation line points.
	size_t numSegmentPoints = 0;
	BOOST_FOREACH(const DislocationSegment* segment, network.segments())
		numSegmentPoints += segment->line.size();

	stream << "# vtk DataFile Version 3.0\n";
	stream << "# Dislocations\n";
	stream << "ASCII\n";
	stream << "DATASET UNSTRUCTURED_GRID\n";
	stream << "POINTS " << numSegmentPoints << " double\n";
	BOOST_FOREACH(DislocationSegment* segment, network.segments()) {
		BOOST_FOREACH(const Vector3& p, segment->line) {
			stream << p.x() << " " << p.y() << " " << p.z() << "\n";
		}
	}
	size_t numCells = network.segments().size();
	stream << "\nCELLS " << numCells << " " << (numSegmentPoints + network.segments().size()) << "\n";
	size_t startIndex = 0;
	BOOST_FOREACH(DislocationSegment* segment, network.segments()) {
		CALIB_ASSERT(segment->line.empty() == false);
		stream << segment->line.size();
		for(size_t i = 0; i < segment->line.size(); i++)
			stream << " " << (i+startIndex);
		stream << "\n";
		startIndex += segment->line.size();
	}

	stream << endl << "CELL_TYPES " << numCells << "\n";
	for(size_t i = 0; i < network.segments().size(); i++)
		stream << "4\n";	// Poly line

	stream << "\nCELL_DATA " << numCells << "\n";

	stream << "\nVECTORS burgers_vector_local double\n";
	BOOST_FOREACH(DislocationSegment* segment, network.segments())
		stream << segment->burgersVector.localVec().x() << " " << segment->burgersVector.localVec().y() << " " << segment->burgersVector.localVec().z() << "\n";

	stream << "\nVECTORS burgers_vector_world double\n";
	BOOST_FOREACH(DislocationSegment* segment, network.segments()) {
		Vector3 burgersVectorWorld = segment->burgersVector.toSpatialVector();
		stream << burgersVectorWorld.x() << " " << burgersVectorWorld.y() << " " << burgersVectorWorld.z() << "\n";
	}

	stream << "\nSCALARS burgers_vector_family int 1\n";
	stream << "\nLOOKUP_TABLE default\n";
	BOOST_FOREACH(DislocationSegment* segment, network.segments())
		stream << burgersVectorFamilyId(segment->burgersVector) << "\n";

	stream << "\nSCALARS segment_length double\n";
	stream << "\nLOOKUP_TABLE default\n";
	BOOST_FOREACH(DislocationSegment* segment, network.segments())
		stream << segment->calculateLength() << "\n";

	stream << "\nSCALARS segment_id int 1\n";
	stream << "\nLOOKUP_TABLE default\n";
	BOOST_FOREACH(DislocationSegment* segment, network.segments())
		stream << segment->id << "\n";

	stream << "\nSCALARS cluster_structure_type int 1\n";
	stream << "\nLOOKUP_TABLE default\n";
	BOOST_FOREACH(DislocationSegment* segment, network.segments())
		stream << segment->burgersVector.cluster()->pattern->index << "\n";

	stream << "\nSCALARS cluster_id int 1\n";
	stream << "\nLOOKUP_TABLE default\n";
	BOOST_FOREACH(DislocationSegment* segment, network.segments()) {
		CALIB_ASSERT(segment->burgersVector.cluster()->isRoot());
		stream << segment->burgersVector.cluster()->outputId << "\n";
	}
}

/******************************************************************************
* Determines the identifier of the Burgers vector family of a dislocation.
******************************************************************************/
int DislocationsWriter::burgersVectorFamilyId(const ClusterVector& burgersVector)
{
	return burgersVector.cluster()->pattern->burgersVectorFamilyId(burgersVector.localVec());
}

/******************************************************************************
* Determines the color of the Burgers vector family of a dislocation.
******************************************************************************/
Vector3 DislocationsWriter::burgersVectorFamilyColor(const ClusterVector& burgersVector)
{
	const SuperPattern* pattern = burgersVector.cluster()->pattern;
	BOOST_FOREACH(const BurgersVectorFamily& family, pattern->burgersVectorFamilies) {
		if(family.isMember(burgersVector.localVec()))
			return family.color;
	}

	return Vector3(1,1,1);
}

}; // End of namespace
