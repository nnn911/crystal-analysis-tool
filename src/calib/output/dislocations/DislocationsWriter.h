///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#ifndef __CA_DISLOCATIONS_WRITER_H
#define __CA_DISLOCATIONS_WRITER_H

#include "../../CALib.h"
#include "../../context/CAContext.h"
#include "../../dislocations/DislocationNetwork.h"

namespace CALib {

/**
 * Writes the extracted dislocation lines to an output file.
 */
class DislocationsWriter : public ContextReference
{
public:

	/// Constructor.
	DislocationsWriter(CAContext& context) : ContextReference(context) {}

	/// Outputs the extracted dislocation segments to a VTK file.
	void writeVTKFile(std::ostream& stream, const DislocationNetwork& network);

	/// Determines the identifier of the Burgers vector family of a dislocation segment.
	int burgersVectorFamilyId(const ClusterVector& burgersVector);

	/// Determines the color of the Burgers vector family of a dislocation segment.
	Vector3 burgersVectorFamilyColor(const ClusterVector& burgersVector);
};

}; // End of namespace

#endif // __CA_DISLOCATIONS_WRITER_H
