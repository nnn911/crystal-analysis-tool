///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#ifndef __CA_FIELD_WRITER_H
#define __CA_FIELD_WRITER_H

#include "../../CALib.h"
#include "../../context/CAContext.h"
#include "../../deformation/Field.h"
#include "../../util/MPIGatherHelper.h"

namespace CALib {

/**
 * Base class for outputting a field computed on the Delaunay tessellation to a file for visualization.
 */
class FieldWriter : public ContextReference
{
public:

	/// Constructor.
	FieldWriter(const DelaunayTessellation& tessellation) : ContextReference(tessellation.context()), _tessellation(tessellation) {}

	/// Returns the underlying tessellation on which the field is defined.
	const DelaunayTessellation& tessellation() const { return _tessellation; }

	/// Returns the atomic structure for which a field is being output.
	const AtomicStructure& structure() const { return tessellation().structure(); }

	/// Maps the index of a local tessellation vertex to the full list of vertices written to the output file.
	AtomInteger mapLocalVertexToGlobal(int localVertexIndex) const {
		CALIB_ASSERT(_pointIndexBase.size() == parallel().processorCount());
		std::map<int,int>::const_iterator entry = _vertexToIndexMap.find(localVertexIndex);
		CALIB_ASSERT(entry != _vertexToIndexMap.end());
		return _pointIndexBase[parallel().processor()] + entry->second;
	}

protected:

	/// Build the local list of vertices that need to be output.
	AtomInteger prepareVertices(const Field& field);

	/// Writes vertices and cell information to a VTK file.
	void writeVTKFile(std::ostream& stream, const Field& field, const char* commentString);

	/// Sends all tessellation vertices to the master processor and outputs them.
	template<typename VertexOutput>
	void writeVertices(VertexOutput& vertexOutput) {
		MPIGatherHelper<Vector3> pointGather(context());
		pointGather.initialize(_vertexToIndexMap.size());

		// Pack local atoms into send buffer.
		std::vector<Vector3>::iterator sendItem = pointGather.beginSend();
		int outputIndex = 0;
		for(std::map<int,int>::iterator iter = _vertexToIndexMap.begin(); iter != _vertexToIndexMap.end(); ++iter) {
			*sendItem++ = structure().atomPosition(iter->first);
			iter->second = outputIndex++;
		}

		_pointIndexBase.clear();
		AtomInteger indexBase = 0;
		while(pointGather.communicate()) {
			_pointIndexBase.push_back(indexBase);
			for(std::vector<Vector3>::const_iterator receivedPoint = pointGather.beginReceive(); receivedPoint != pointGather.endReceive(); ++receivedPoint, indexBase++)
				vertexOutput(indexBase, *receivedPoint);
		}

		parallel().broadcastWithSize(_pointIndexBase);
	}

	/// Sends a data record for each tessellation cell to the master processor and outputs it.
	template<typename FieldType, typename CellInformationOutput, typename CellInformationPacker>
	void writeCellInformation(const FieldType& field, CellInformationOutput& cellOutput, const CellInformationPacker& packer) const {
		typedef typename CellInformationOutput::data_type DataType;
		MPIGatherHelper<DataType> cellGather(context());
		cellGather.initialize(field.localCells().size());

		// Pack local data records into send buffer.
		typename std::vector<DataType>::iterator sendItem = cellGather.beginSend();
		for(int cellIndex = 0; cellIndex < field.localCells().size(); cellIndex++) {
			packer(*sendItem++, field, cellIndex, *this);
		}

		// Transfer to master processor.
		while(cellGather.communicate()) {
			// Write to output stream.
			for(typename std::vector<DataType>::const_iterator receivedItem = cellGather.beginReceive(); receivedItem != cellGather.endReceive(); ++receivedItem)
				cellOutput(*receivedItem);
		}
	}

	/// Writes the one float value for each tessellation cell.
	struct VTKFloatWriter {
		std::ostream& _stream;
		VTKFloatWriter(std::ostream& stream) : _stream(stream) {}
		typedef CAFloat data_type;
		void operator()(const data_type& c) const {
			_stream << c << std::endl;
		}
	};

	/// Writes the one inetger value for each tessellation cell.
	struct VTKIntWriter {
		std::ostream& _stream;
		VTKIntWriter(std::ostream& stream) : _stream(stream) {}
		typedef int data_type;
		void operator()(const data_type& c) const {
			_stream << c << std::endl;
		}
	};

	/// Writes a scalar field value to the output stream.
	template<typename FieldType, typename CellInformationPacker>
	void writeVTKCellFloat(std::ostream& stream, const std::string& outputFieldName, const FieldType& field, const CellInformationPacker& packer) const {
		using namespace std;
		if(parallel().isMaster()) {
			stream << endl << "SCALARS " << outputFieldName << " double" << endl;
			stream << endl << "LOOKUP_TABLE default" << endl;
		}
		VTKFloatWriter writer(stream);
		writeCellInformation(field, writer, packer);
	}

	/// Writes a scalar field value to the output stream.
	template<typename FieldType, typename CellInformationPacker>
	void writeVTKCellInt(std::ostream& stream, const std::string& outputFieldName, const FieldType& field, const CellInformationPacker& packer) const {
		using namespace std;
		if(parallel().isMaster()) {
			stream << endl << "SCALARS " << outputFieldName << " int" << endl;
			stream << endl << "LOOKUP_TABLE default" << endl;
		}
		VTKIntWriter writer(stream);
		writeCellInformation(field, writer, packer);
	}

private:

	/// The underlying tessellation on which the field is defined.
	const DelaunayTessellation& _tessellation;

	/// Maps tessellation vertex indices of the tessellation to indices in the local output vertex list.
	std::map<int,int> _vertexToIndexMap;

	/// Starting index of each processor into the list of output vertices.
	std::vector<AtomInteger> _pointIndexBase;
};

}; // End of namespace

#endif // __CA_FIELD_WRITER_H
