///////////////////////////////////////////////////////////////////////////////
//
//  Copyright 2011-2013, Alexander Stukowski
//  All rights reserved. See README.txt for more information.
//
///////////////////////////////////////////////////////////////////////////////

#include "FieldWriter.h"
#include "../../context/CAContext.h"

using namespace std;

namespace CALib {

/// Writes tessellation vertices to the output stream.
struct VTKVertexWriter {
	ostream& _stream;
	VTKVertexWriter(ostream& stream) : _stream(stream) {}
	void operator()(AtomInteger index, const Vector3& p) const {
		_stream << p.x() << " " << p.y() << " " << p.z() << "\n";
	}
};

/// Writes the four vertices of a cell to the output stream.
struct VTKCellWriter {
	ostream& _stream;
	VTKCellWriter(ostream& stream) : _stream(stream) {}

	struct data_type {
		AtomInteger vertices[4];
	};

	void operator()(const data_type& c) {
		_stream << "4 " << c.vertices[0] << " " << c.vertices[1] << " " << c.vertices[2] << " " << c.vertices[3] << "\n";
	}

	static void pack(data_type& r, const Field& field, int cellIndex, const FieldWriter& fieldWriter) {
		DelaunayTessellation::CellHandle cell = field.localCells()[cellIndex];
		for(int v = 0; v < 4; v++)
			r.vertices[v] = fieldWriter.mapLocalVertexToGlobal(cell->vertex(v)->info());
	}
};

/******************************************************************************
* Build the local list of vertices that need to be output.
******************************************************************************/
AtomInteger FieldWriter::prepareVertices(const Field& field)
{
	// Build list of points which need to be written to the output file.
	BOOST_FOREACH(DelaunayTessellation::CellHandle cell, field.localCells()) {
		for(int v = 0; v < 4; v++) {
			int vertexIndex = cell->vertex(v)->info();
			CALIB_ASSERT(vertexIndex >= 0);
			_vertexToIndexMap.insert(make_pair(vertexIndex, -1));
		}
	}

	return parallel().reduce_sum((AtomInteger)_vertexToIndexMap.size());
}

/******************************************************************************
* Writes vertices and cell information to a VTK file.
******************************************************************************/
void FieldWriter::writeVTKFile(ostream& stream, const Field& field, const char* commentString)
{
	CALIB_ASSERT(&field.tessellation() == &this->tessellation());

	// Generate list of points which need to be written to the output file.
	AtomInteger totalNumPoints = prepareVertices(field);

	// Writer header.
	if(parallel().isMaster()) {
		stream << "# vtk DataFile Version 3.0\n";
		stream << "# " << commentString << "\n";
		stream << "ASCII\n";
		stream << "DATASET UNSTRUCTURED_GRID\n";
		stream << "POINTS " << totalNumPoints << " double\n";
	}

	// Write vertices.
	VTKVertexWriter vertexWriter(stream);
	writeVertices(vertexWriter);

	// Write cell count.
	AtomInteger numCells = parallel().reduce_sum(field.localCells().size());
	if(parallel().isMaster()) {
		stream << "\nCELLS " << numCells << " " << (numCells * 5) << "\n";
	}

	// Write cell vertices.
	VTKCellWriter cellWriter(stream);
	writeCellInformation(field, cellWriter, &VTKCellWriter::pack);

	// Write VTK cell types.
	if(parallel().isMaster()) {
		stream << endl << "CELL_TYPES " << numCells << endl;
		for(AtomInteger i = 0; i < numCells; i++)
			stream << "10\n";

		stream << "\nCELL_DATA " << numCells << "\n";
	}
}


}; // End of namespace
