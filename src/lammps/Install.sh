# Install/unInstall package files in LAMMPS
# edit two Makefile.package files to include/exclude package info

if (test $1 = 1) then

  if (test -e ../Makefile.package) then
    sed -i -e 's/[^ \t]*crystalanalysis[^ \t]* //' ../Makefile.package
    sed -i -e 's|^PKG_SYSINC =[ \t]*|&$(crystalanalysis_SYSINC) |' ../Makefile.package
    sed -i -e 's|^PKG_SYSLIB =[ \t]*|&$(crystalanalysis_SYSLIB) |' ../Makefile.package
    sed -i -e 's|^PKG_SYSPATH =[ \t]*|&$(crystalanalysis_SYSPATH) |' ../Makefile.package
  fi

  if (test -e ../Makefile.package.settings) then
    sed -i -e '/^include.*USER-CRYSTALANALYSIS.*$/d' ../Makefile.package.settings
    # multiline form needed for BSD sed on Macs
    sed -i -e '4 i \
include ..\/USER-CRYSTALANALYSIS\/Makefile.lammps
' ../Makefile.package.settings
  fi

  ln -s USER-CRYSTALANALYSIS/fix_crystalanalysis.cpp ..
  ln -s USER-CRYSTALANALYSIS/fix_crystalanalysis.h ..

elif (test $1 = 0) then

  if (test -e ../Makefile.package) then
    sed -i -e 's/[^ \t]*crystalanalysis[^ \t]* //' ../Makefile.package
  fi

  if (test -e ../Makefile.package.settings) then
    sed -i -e '/^include.*USER-CRYSTALANALYSIS.*$/d' ../Makefile.package.settings
  fi

  rm -f ../fix_crystalanalysis.cpp
  rm -f ../fix_crystalanalysis.h

fi
