#!/bin/bash

# Path to the cmake program. Edit this if CMake is not installed in the system search path.
CMAKE_EXECUTABLE=cmake

# List of 3rd-party libraries to download and install:
ZLIB_PACKAGE=(https://github.com/madler/zlib/archive/v1.2.8.tar.gz zlib-1.2.8)
BOOST_PACKAGE=(https://sf.net/projects/boost/files/boost/1.60.0/boost_1_60_0.tar.bz2 boost_1_60_0)
GMP_PACKAGE=(https://gmplib.org/download/gmp/gmp-5.1.3.tar.bz2 gmp-5.1.3)
MPFR_PACKAGE=(https://ftp.gnu.org/gnu/mpfr/mpfr-3.1.3.tar.gz mpfr-3.1.3)
CGAL_PACKAGE=(https://github.com/CGAL/cgal/archive/releases/CGAL-4.7.tar.gz CGAL-4.7)
EIGEN_PACKAGE=(https://gitlab.com/libeigen/eigen/-/archive/3.2.8/eigen-3.2.8.tar.bz2 eigen3.2.8)

# Make sure CMake is installed.
which ${CMAKE_EXECUTABLE} >> /dev/null
if (( $? != 0 )) ; then
	echo "ERROR:"
	echo "Could not locate the program '${CMAKE_EXECUTABLE}', which is required to compile the source code. Please install CMake first or specify its location by setting the CMAKE_EXECUTABLE variable in this shell script."
	exit 1
fi

# Executes command with status testing.
# Usage  : runcmd <cmd>
function runcmd {
    local cmd=($*)
    ${cmd[@]}
    local st=$?
    if (( $st != 0 )) ; then
    	echo "The following command failed with error status code $st."
    	echo "Current directory: $PWD"
    	echo "Command: ${cmd[@]}"
		exit $st;
    fi
}

# Downloads a file from the server.
# Usage  : download <URL>
function download {
	local ar=$1
	local fn=$(basename $ar)
    if [  -f "$fn" ] ; then
		echo "Skipping download of $fn because it's already there."
    else
	    echo '==================================================================='
	    echo "| Downloading library source archive from server"
	    echo "| Source URL: ${ar}"
	    echo '==================================================================='    
		runcmd wget $ar -O "$fn"
    fi
}

# Extracts a source code archive.
# Usage  : extract <file>
function extract {
	local fn=$1
	local ext=$(echo $fn | awk -F. '{print $NF}')
    echo '==================================================================='
    echo "| Extracting archive"
    echo "| File: ${fn}"
    echo '==================================================================='    
	case "$ext" in
	    "bz2")
		runcmd tar jxf $fn
		;;
	    "gz")
		runcmd tar zxf $fn
		;;
	    "tar")
		runcmd tar xf $fn
		;;
	    *)
		echo "ERROR: Unknown file extension: $ext. Can't continue."
		exit 1		
		;;
	esac
}

runcmd mkdir -p 3rdparty
runcmd cd 3rdparty

# Download archives
for ar in ${ZLIB_PACKAGE[0]} ${BOOST_PACKAGE[0]} ${GMP_PACKAGE[0]} ${MPFR_PACKAGE[0]} ${CGAL_PACKAGE[0]} ${EIGEN_PACKAGE[0]} ; do
    download $ar
done

# Extract archives
for ar in ${ZLIB_PACKAGE[0]} ${BOOST_PACKAGE[0]} ${GMP_PACKAGE[0]} ${MPFR_PACKAGE[0]} ${CGAL_PACKAGE[0]} ${EIGEN_PACKAGE[0]} ; do
	fn=$(basename $ar)
	extract $fn
done

# Eigen3 library is extracted to a directory with a non-standard name.
# Give directory a well-defined name.
if [ ! -d "${EIGEN_PACKAGE[1]}" ] ; then
	runcmd mv eigen-eigen* ${EIGEN_PACKAGE[1]}
fi

# The same for the CGAL library
if [ ! -d "${CGAL_PACKAGE[1]}" ] ; then
        runcmd mv cgal-releases-${CGAL_PACKAGE[1]} ${CGAL_PACKAGE[1]}
fi

echo '==================================================================='
echo "| Compiling Boost library"
echo '==================================================================='
cd ${BOOST_PACKAGE[1]}
runcmd ./bootstrap.sh
runcmd ./b2 --with-iostreams \
			--with-thread --with-system --with-program_options \
			-sNO_BZIP2=1 -sZLIB_SOURCE=$PWD/../${ZLIB_PACKAGE[1]} -q \
			link=static stage
cd ..

echo '==================================================================='
echo "| Configuring GMP library"
echo '==================================================================='
cd ${GMP_PACKAGE[1]}
runcmd ./configure --prefix=$PWD/../gmp
echo '==================================================================='
echo "| Compiling GMP library"
echo '==================================================================='
runcmd make
runcmd make check
runcmd make install
cd ..

echo '==================================================================='
echo "| Configuring MPFR library"
echo '==================================================================='
cd ${MPFR_PACKAGE[1]}
runcmd ./configure --with-gmp-build=$PWD/../${GMP_PACKAGE[1]}  --prefix=$PWD/../mpfr
echo '==================================================================='
echo "| Compiling MPFR library"
echo '==================================================================='
runcmd make
runcmd make check
runcmd make install
cd ..

echo '==================================================================='
echo "| Configuring CGAL library"
echo '==================================================================='
cd ${CGAL_PACKAGE[1]}
GMP_DIR=$PWD/../gmp
MPFR_DIR=$PWD/../mpfr
GMP_INC_DIR=$PWD/../gmp/include
runcmd ${CMAKE_EXECUTABLE} -DWITH_CGAL_Core=OFF -DWITH_CGAL_ImageIO=OFF -DWITH_CGAL_Qt3=OFF \
			 -DWITH_CGAL_Qt5=OFF \
			 -DGMP_INCLUDE_DIR=$PWD/../gmp/include -DGMP_LIBRARIES=$PWD/../gmp/lib/libgmp.a \
			 -DMPFR_INCLUDE_DIR=$PWD/../mpfr/include -DMPFR_LIBRARIES=$PWD/../mpfr/lib/libmpfr.a \
			 -DBUILD_SHARED_LIBS=FALSE -DBOOST_ROOT=$PWD/../${BOOST_PACKAGE[1]} \
			 -DBoost_NO_SYSTEM_PATHS=TRUE -DCGAL_Boost_USE_STATIC_LIBS=ON .
echo '==================================================================='
echo "| Compiling CGAL library"
echo '==================================================================='
runcmd make
cd ..

echo '==================================================================='
echo "| All required libraries have been built."
echo '==================================================================='

cd ..

echo '==================================================================='
echo "| Creating Makefile for Crystal Analysis Tool"
echo '==================================================================='
runcmd mkdir -p build
runcmd cd build
runcmd ${CMAKE_EXECUTABLE} -DBOOST_ROOT=$PWD/../3rdparty/${BOOST_PACKAGE[1]} -DBoost_NO_BOOST_CMAKE=TRUE \
      -DBoost_NO_SYSTEM_PATHS=TRUE -DCGAL_DIR=$PWD/../3rdparty/${CGAL_PACKAGE[1]} \
      -DEIGEN3_INCLUDE_DIR=$PWD/../3rdparty/${EIGEN_PACKAGE[1]} \
      ../src/
cd ..

echo '==================================================================='
echo
echo "All required third-party libraries have been prepared."
echo
echo "To compile the Crystal Analysis Tool, type"
echo
echo "  cd build"
echo "  make"
echo '==================================================================='
