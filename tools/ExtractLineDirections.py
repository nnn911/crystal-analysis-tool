#!/usr/bin/env python

# Creates a VTK file for visualizing the line directions.

# Author: Alexander Stukowski

from sys import *
from optparse import OptionParser
from DislocationNetwork import *
from LinAlg import *

# Parse command line options
parser = OptionParser(usage="Usage: %prog inputfile outputfile")
options, args = parser.parse_args()
if len(args) < 2:
    parser.error("Incorrect number of command line arguments. Please specify an input dislocations file and an output VTK file.")

# Read the input file.
instream = stdin
if args[0] != '-': instream = open(args[0], "r")
network = loadDislocationNetwork(instream)
 
# Write the output file.
stream = stdout
if args[1] != '-': stream = open(args[1], "w")

stream.write("# vtk DataFile Version 3.0\n")
stream.write("Line directions\n")
stream.write("ASCII\n")
stream.write("DATASET UNSTRUCTURED_GRID\n")

# Write point coordinates
stream.write("POINTS %i float\n" % len(network.segments))

for segment in network.segments:
	stream.write("%g %g %g\n" % (segment.points[0].x(), segment.points[0].y(), segment.points[0].z()))

stream.write("\nCELLS %i %i\n" % (len(network.segments), len(network.segments)*2))

for i in range(len(network.segments)):
	stream.write("1 %i\n" % i)

stream.write("\nCELL_TYPES %i\n" % len(network.segments))
for segment in network.segments:
	stream.write("1\n")

stream.write("\nCELL_DATA %i\n" % len(network.segments))

stream.write("\nVECTORS line_direction float\n")
for segment in network.segments:
	direction = Vector(0,0,0)
	for p in segment.points[1:]:
		try:
			direction = (segment.points[0] - p).normal()
			break
		except ZeroDivisionError:
			pass
	stream.write("%g %g %g\n" % (direction.x(), direction.y(), direction.z()))

stream.write("\nSCALARS segment_id int\n")
stream.write("LOOKUP_TABLE default\n")
for segment in network.segments:
	if segment.id != None:
		stream.write("%i\n" % segment.id)
	else:
		stream.write("-1\n")

