""" 
Linear algebra classes.

Author: Alexander Stukowski (stukowski@mm.tu-darmstadt.de)
"""

import math

class Vector:
	""" A 3d vector """

	def __init__(self):	
		self._x = 0.0
		self._y = 0.0
		self._z = 0.0

	def __init__(self, x, y, z):	
		self._x = float(x)
		self._y = float(y)
		self._z = float(z)

	def x(self):
		return self._x

	def y(self):
		return self._y

	def z(self):
		return self._z

	def length(self):
		return math.sqrt(self._x*self._x + self._y*self._y + self._z*self._z)

	def lengthSquared(self):
		return self._x*self._x + self._y*self._y + self._z*self._z

	def normal(self):
		ls = self.length()
		if ls == 0.0: raise ZeroDivisionError()
		return Vector(self._x / ls, self._y / ls, self._z / ls)

	def __add__(self, other):
		return Vector(self._x + other._x, self._y + other._y, self._z + other._z)

	def __sub__(self, other):
		return Vector(self._x - other._x, self._y - other._y, self._z - other._z)

	def __div__(self, other):
		return Vector(self._x / other, self._y / other, self._z / other)

	def __neg__(self):
		return Vector(-self._x, -self._y, -self._z)

	def __str__(self):
		return "(%f %f %f)" % (self._x, self._y, self._z)

	def dot(self, b):
		return self._x*b._x + self._y*b._y + self._z*b._z

	def cross(self, b):
		return Vector(self._y * b._z - self._z * b._y, self._z * b._x - self._x * b._z, self._x * b._y - self._y * b._x)

nullVector = Vector(0,0,0)

